<?php
/**
 * This file is part of OXID eSales Flow theme.
 *
 * OXID eSales Flow theme is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OXID eSales Flow theme is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OXID eSales Flow theme.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 */

$sLangName = "Deutsch";

$aLang = array(
    'charset' => 'UTF-8',

    // Global
    'DD_SORT_DESC' => 'absteigend',
    'DD_SORT_ASC' => 'aufsteigend',
    'DD_CLOSE_MODAL' => 'schließen',
    'DD_DEMO_ADMIN_TOOL' => 'Admin-Tool starten',

    // Form-Validation
    'DD_FORM_VALIDATION_VALIDEMAIL' => 'Bitte gib eine gültige E-Mail-Adresse ein.',
    'DD_FORM_VALIDATION_PASSWORDAGAIN' => 'Die Passwörter stimmen scheinbar nicht überein.',
    'DD_FORM_VALIDATION_NUMBER' => 'Bitte gib eine Zahl ein.',
    'DD_FORM_VALIDATION_INTEGER' => 'Nachkommastellen sind leider nicht erlaubt.',
    'DD_FORM_VALIDATION_POSITIVENUMBER' => 'Bitte gib eine positive Zahl ein.',
    'DD_FORM_VALIDATION_NEGATIVENUMBER' => 'Bitte gib eine negative Zahl ein.',
    'DD_FORM_VALIDATION_REQUIRED' => 'Bitte gib einen Wert an.',
    'DD_FORM_VALIDATION_CHECKONE' => 'Bitte wähle mindestens eine Option aus.',

    // Header
    'SEARCH_TITLE' => 'Gib einen Suchbegriff ein',
    'SEARCH_SUBMIT' => 'Suchen',

    // Sidebar
    'DD_SIDEBAR_CATEGORYTREE' => 'Kategorien',

    // Footer
    'FOOTER_NEWSLETTER_INFO' => 'Newsletter abonnieren und 5€ Rabatt erhalten',
    'ZAHLUNGSMOEGLICHKEITEN' => 'ZAHLUNGSMÖGLICHKEITEN',
    'VERSANDMIT' => 'VERSAND MIT',
    'FAQ' => 'FAQ - Häufig gestellte Fragen',
    'UnserYogastudio' => 'Unser Yoga Studio in Köln',
    'KATALOGEDOWNLOADS' => 'Kataloge & Downloads',

    // Startseite
    'MANUFACTURERSLIDER_SUBHEAD' => 'Unsere Marken',
    'START_BARGAIN_HEADER' => 'Angebote der Woche',
    'START_NEWEST_HEADER' => 'Neu im Shop',
    'START_TOP_PRODUCTS_HEADER' => 'Unsere Topseller',
    'START_BARGAIN_SUBHEADER' => 'Entdecke die neuesten Angebote und Schnäppchen im Shop',
    'START_NEWEST_SUBHEADER' => 'Unsere neuesten Produkte aus den Bereichen Yoga, Meditation, Wellness und Lifestyle',
    'START_TOP_PRODUCTS_SUBHEADER' => 'Die 5 beliebtesten Produkte im Shop',

    // Kontaktformular
    'DD_CONTACT_PAGE_HEADING' => 'Kontaktiere uns',
    'DD_CONTACT_FORM_HEADING' => 'Kontaktformular',
    'DD_CONTACT_ADDRESS_HEADING' => 'Adresse',
    'DD_CONTACT_THANKYOU1' => "Vielen Dank für deine Nachricht an",
    'DD_CONTACT_THANKYOU2' => ".",

    // Link-Seite
    'DD_LINKS_NO_ENTRIES' => 'Es sind leider noch keine Links vorhanden.',

    // 404-Seite
    'DD_ERR_404_START_TEXT' => 'Vielleicht findest du die gewünschten Informationen auf unserer Startseite:',
    'DD_ERR_404_START_BUTTON' => 'Startseite aufrufen',
    'DD_ERR_404_CONTACT_TEXT' => 'Benötigst du Hilfe oder hast Fragen zu unseren Produkten?<br>Gerne kannst du uns anrufen oder uns eine E-Mail schreiben:',
    'DD_ERR_404_CONTACT_BUTTON' => 'Zum Kontaktformular',

    // Login
    'DD_LOGIN_ACCOUNT_PANEL_CREATE_TITLE' => 'Konto eröffnen',
    'DD_LOGIN_ACCOUNT_PANEL_CREATE_BODY' => 'Durch deine Anmeldung in unserem Shop wird deine Bestellung noch einfacher und schneller. In deinem Konto kannst du mehrere Versandadressen speichern und den Status deiner Bestellungen verfolgen.',
    'DD_LOGIN_ACCOUNT_PANEL_LOGIN_TITLE' => 'Anmelden',

    // Rechnungs- und Lieferadresse
    'DD_USER_BILLING_LABEL_STATE' => 'Bundesland:',
    'DD_USER_SHIPPING_LABEL_STATE' => 'Bundesland:',
    'DD_USER_SHIPPING_SELECT_ADDRESS' => 'Auswählen',
    'DD_USER_SHIPPING_ADD_DELIVERY_ADDRESS' => 'Neue Adresse hinzufügen',

    // Bestellhistorie
    'DD_ORDER_ORDERDATE' => 'Datum:',

    // Listen
    'DD_LISTLOCATOR_FILTER_ATTRIBUTES' => 'Filter:',
    'DD_LIST_SHOW_MORE' => 'Produkte ansehen...',

    // Lieblingslisten
    'DD_RECOMMENDATION_EDIT_BACK_TO_LIST' => 'Zurück zur Übersicht',

    // Downloads
    'DD_DOWNLOADS_DOWNLOAD_TOOLTIP' => 'Herunterladen',
    'DD_FILE_ATTRIBUTES_FILESIZE' => 'Dateigröße:',
    'DD_FILE_ATTRIBUTES_OCLOCK' => 'Uhr',
    'DD_FILE_ATTRIBUTES_FILENAME' => 'Dateiname:',

    // Detailseite
    'BACK_TO_OVERVIEW' => 'Zur Übersicht',
    'OF' => 'von',
    'DD_PRODUCTMAIN_STOCKSTATUS' => 'Lagerbestand',
    'DD_RATING_CUSTOMERRATINGS' => 'Kundenmeinungen',
    'PAGE_DETAILS_CUSTOMERS_ALSO_BOUGHT_SUBHEADER' => 'Kunden, die diesen Artikel gekauft haben, kauften auch folgende Artikel.',
    'WIDGET_PRODUCT_RELATED_PRODUCTS_ACCESSORIES_SUBHEADER' => 'Folgende Artikel passen gut zu diesem Artikel:',
    'WIDGET_PRODUCT_RELATED_PRODUCTS_SIMILAR_SUBHEADER' => 'Diese Artikel könnten dir auch gefallen:',
    'WIDGET_PRODUCT_RELATED_PRODUCTS_CROSSSELING_SUBHEADER' => 'Kunden, die sich diesen Artikel angesehen haben, haben sich auch folgende Artikel angesehen:',
    'DETAILS_VPE_MESSAGE_1' => 'Dieser Artikel kann nur in Verpackungseinheiten zu je',
    'DETAILS_VPE_MESSAGE_2' => 'erworben werden.',

    // Modal-Warenkorb
    'DD_MINIBASKET_MODAL_TABLE_TITLE' => 'Artikel',
    'DD_MINIBASKET_MODAL_TABLE_PRICE' => 'Gesamtsumme',
    'DD_MINIBASKET_CONTINUE_SHOPPING' => 'weiter einkaufen',

    // Checkout
    'DD_BASKET_BACK_TO_SHOP' => 'zurück zum Shop',

    // E-Mails
    'DD_ORDER_CUST_HEADING' => 'Bestellung',
    'DD_FOOTER_FOLLOW_US' => 'Folge uns:',
    'DD_FOOTER_CONTACT_INFO' => 'Kontakt:',
    'DD_FORGOT_PASSWORD_HEADING' => 'Passwort vergessen',
    'DD_INVITE_HEADING' => 'Artikel-Empfehlung',
    'DD_INVITE_LINK' => 'Link',
    'DD_NEWSLETTER_OPTIN_HEADING' => 'Deine Newsletter-Anmeldung',
    'DD_ORDERSHIPPED_HEADING' => 'Versandbestätigung - Bestellung',
    'DD_OWNER_REMINDER_HEADING' => 'Lagerbestand niedrig',
    'DD_PRICEALARM_HEADING' => 'Preisalarm',
    'DD_REGISTER_HEADING' => 'Deine Registrierung',
    'DD_DOWNLOADLINKS_HEADING' => 'Deine Downloadlinks - Bestellung',
    'DD_SUGGEST_HEADING' => 'Artikel-Empfehlung',
    'DD_WISHLIST_HEADING' => 'Wunschzettel',

    'DD_ROLES_BEMAIN_UIROOTHEADER' => 'Menü',
    'WIDGET_SERVICES_CONTACTS' => 'Kontakt',
    'SUCHBEGRIFF' => 'Suchbegriff hinzufügen...',
    'PREIS' => 'Preis',
    'FREE' => 'Kostenlos',
    'MORE_INFO' => 'Details',
    'BLOCK_PRICE' => 'Mengenrabatt',
    'Newslettercode' => 'Gutscheincode per Mail senden',
    'Newslettercodebetreff' => 'Ihr Bodynova Gutschein Code',
    'Newslettercodebody'    => 'code: NWSLTBN Mindestbestellwert 30 EUR, nicht mit anderen Rabatten kombinierbar',
    'MESSAGE_NEWSLETTER_CONGRATULATIONS' => 'Herzlich Willkommen',
    'STAY_INFORMED' => 'Erhalte News aus der Bodynova-Welt!',
    'Retouren' => 'Retouren',
    'RetourenLabel' => 'RetourenLabel',
    'VOUCHERMELDUNG' => 'Um einen Gutschein einzul&ouml;sen, m&uuml;ssen Sie sich zuerst oben "Registrieren", oder anmelden ("Login") wenn Sie schon registriert sind.',
    'TELEFONNUMMER' => ' + 49 - 221 - 3566350',
    'OEFFNUNGSZEITEN' => 'Mo-Fr: 11 - 15 Uhr',
    'DELIVERYTIME_DELIVERYTIME' => 'Produktionszeit',

    'DD_DELETE_MY_ACCOUNT_WARNING' => 'Dieser Vorgang kann nicht rückgängig gemacht werden. Alle persönlichen Daten werden dauerhaft gelöscht.',
    'DD_DELETE_MY_ACCOUNT' => 'Konto löschen',
    'DD_DELETE_MY_ACCOUNT_CONFIRMATION_QUESTION' => 'Sind Sie sicher, dass Sie Ihr Konto löschen wollen?',
    'DD_DELETE_MY_ACCOUNT_CANCEL' => 'Abbrechen',
    'DD_DELETE_MY_ACCOUNT_SUCCESS' => 'Ihr Konto wurde gelöscht',
    'DD_DELETE_MY_ACCOUNT_ERROR' => 'Das Konto konnte nicht gelöscht werden',

    // Account -> My product reviews
    'DD_DELETE_REVIEW_AND_RATING' => 'Bewertung und Sterne-Rating löschen',
    'DD_REVIEWS_NOT_AVAILABLE' => 'Es liegen keine Bewertungen vor',
    'DD_DELETE_REVIEW_CONFIRMATION_QUESTION' => 'Sind Sie sicher, dass Sie die Bewertung löschen wollen?',

    // Contact page
    'DD_SELECT_SALUTATION' => 'Bitte auswählen',

    'DD_CATEGORY_RESET_BUTTON' => 'Zurücksetzen',
    'PAGE_WISHLIST_PRODUCTS_PRODUCTS1' => '',
    'PAGE_WISHLIST_PRODUCTS_PRODUCTS2' => '',

	// Ampel Texte
	'READY_FOR_SHIPPING' => 'Sofort lieferbar',
	'STOCK_LOW' => 'Lagerbestand niedrig',

	'LOW_STOCK' => 'Nur noch wenige Exemplare - schnell zugreifen!',
	'AVAILABLE_ON' => 'Nachschub kommt! Lieferbar ab %s',
	'AVAILABLE_ON_NOT_BUYABLE' => 'Wir warten auf Nachschub. Voraussichtlich lieferbar ab %s',
	'MESSAGE_NOT_ON_STOCK' => 'Momentan nicht verfügbar',

    // Duzen:
    'THANK_YOU_FOR_ORDER' => 'Vielen Dank für Deine Bestellung im',
    'THANK_YOU_MESSAGE' => 'Vielen Dank für Deine Nachricht an %s.',
    'REGISTERED_YOUR_ORDER' => 'Deine Bestellung ist unter der Nummer %s bei uns registriert.',
    'ADD_YOUR_COMMENTS' => 'Deine Nachricht',
    'CARD_SECURITY_CODE_DESCRIPTION' => 'Diese befindet sich auf der Rückseite Deiner Kreditkarte. Die Prüfziffer sind die letzten drei Ziffern im Unterschriftsfeld.',
    'CHECK_YOUR_ORDER_HISTORY' => 'Deine Bestellhistorie aufrufen',
    'ERROR_MESSAGE_VERSION_EXPIRED1' => 'Deine Version ist leider abgelaufen. Bitte kontaktiere',
    'ERROR_MESSAGE_VOUCHER_NOTVALIDUSERGROUP' => 'Deine Benutzergruppe erlaubt diesen Gutschein nicht!',
    'LOGIN_ALREADY_CUSTOMER' => 'Falls Du schon Kunde bei uns bist, melde dich bitte hier mit Deiner E-Mail-Adresse und Deinem Passwort an.',
    'MESSAGE_BASKET_EXCLUDE_INFO' => 'Du kannst nun zur Kasse gehen und Deine Bestellung abschließen. Du kannst auch weiter einkaufen, jedoch wird dann der Warenkorbinhalt geleert.',
    'MESSAGE_CONFIRMING_REGISTRATION' => 'Du hast eine E-Mail von uns erhalten, die Deine Registrierung bestätigt.',
    'MESSAGE_ENTER_YOUR_ADDRESS_AND_MESSAGE' => 'Gib die Adressdaten und Deine persönliche Nachricht ein!',
    'MESSAGE_GET_BONUS_POINTS' => 'Hol Dir jetzt für Deinen Einkauf Bonuspunkte!',
    'MESSAGE_INVITE_YOUR_FRIENDS' => 'Lade Deine Freunde ein und erhalte Bonuspunkte für jede neue Registrierung!',
    'MESSAGE_INVITE_YOUR_FRIENDS_EMAIL' => 'Trage die E-Mail-Adressen Deiner Freunde unten ein, um ihnen eine Einladung zu schicken.',
    'MESSAGE_PAYMENT_AUTHORIZATION_FAILED' => 'Autorisierung der Zahlung fehlgeschlagen. Bitte prüfe Deine Eingabe!',
    'MESSAGE_PRICE_ALARM_PRICE_CHANGE' => 'Wir informieren Dich gern darüber, falls der Preis dieses Artikels Deinem Wunschpreis entspricht.',
    'MESSAGE_SEND_GIFT_REGISTRY' => 'Klicke hier, um Deinen Wunschzettel an Deine Freunde zu versenden!',
    'MESSAGE_SUBMIT_BOTTOM' => 'Bitte prüfe alle Daten, bevor Du Deine Bestellung abschließt!',
    'MESSAGE_VERIFY_YOUR_EMAIL' => 'Bitte kontrolliere Deine E-Mail-Adresse!',
    'MESSAGE_DOWNLOADABLE_PRODUCT' => 'Hinweis: Du hast Download-Artikel im Warenkorb. Wenn Du ohne Registrierung einkaufst, findest Du Downloadlinks ausschließlich in Deiner E-Mail zur Bestellbestätigung. Bist Du registriert, werden die Downloadlinks unter KONTO -> MEINE DOWNLOADS angezeigt.',
    'SELECT_SHIPPING_METHOD' => 'Bitte wähle Deine Versandart',
    'VAT_MESSAGE_COMPANY_MISSING' => 'Bitte gib zur USt-ID auch Deinen Firmennamen an!',
    'WHAT_I_WANTED_TO_SAY' => 'Deine Mitteilung an uns',
    'WRAPPING_DESCRIPTION' => 'Wir verpacken gern Dein Geschenk oder legen eine Karte mit Deiner persönlichen Nachricht bei.',
    'YOUR_EMAIL_ADDRESS' => 'Deine E-Mail-Adresse',
    'YOUR_GREETING_CARD' => 'Deine Grußkarte',
    'YOUR_PREVIOUS_ORDER' => 'Deine bisherigen Bestellungen',
    'YOUR_REVIEW' => 'Deine Bewertung',
    'YOUR_PRICE' => 'Dein Preis',
    'YOU_ARE_HERE' => 'Du bist hier',
    'YOU_CAN_GO' => 'Du kannst nun',
    'MY_DOWNLOADS_DESC' => 'Laden Deine bestellten Dateien hier herunter.',
    'COOKIE_NOTE' => 'Dieser Online-Shop verwendet Cookies für ein optimales Einkaufserlebnis. Dabei werden beispielsweise die Session-Informationen oder die Spracheinstellung auf Deinem Rechner gespeichert. Ohne Cookies ist der Funktionsumfang des Online-Shops eingeschränkt.',
    'GIFT_REGISTRY_SENT_SUCCESSFULLY' => 'Dein Wunschzettel wurde erfolgreich an %s verschickt.',
    'SHIPMENT_TRACKING' => 'Dein Link zur Sendungsverfolgung',
    'ERROR_MESSAGE_PASSWORD_TOO_SHORT' => 'Fehler: Dein Passwort ist zu kurz.',
    'ERROR_MESSAGE_CURRENT_PASSWORD_INVALID' => 'Fehler: Dein aktuelles Passwort ist falsch.',
    'ERROR_MESSAGE_UNLICENSED1' => 'Dein Shop ist nicht lizenziert. Trage bitte im Shop Admin einen gültigen Lizenzschlüssel ein oder kontaktiere',
    'HAVE_YOU_FORGOTTEN_PASSWORD' => 'Du hast Dein Passwort vergessen?',
    'MESSAGE_NEWSLETTER_FOR_SUBSCRIPTION_BONUS' => 'Hol Dir jetzt für Dein Newsletter-Abonnement Bonuspunkte!',
    'MESSAGE_PASSWORD_CHANGED' => 'Dein Passwort wurde geändert.',
    'PASSWORD_CHANGED' => 'Dein Passwort wurde erfolgreich geändert.',
    'WISHLIST_PRODUCTS' => 'Diese Artikel hat sich %s gewünscht. Wenn Du ihr/ihm eine Freude machen willst, dann kaufe einen oder mehrere von diesen Artikeln.',
    // Sie
    'CHECK' => 'Sieh',
    'ENTER_NEW_PASSWORD' => 'Bitte gib ein neues Passwort ein.',
    'ERROR_MESSAGE_COOKIE_NOCOOKIE' => 'Für diese Aktion werden Cookies benötigt. Bitte aktiviere Cookies oder nutze einen anderen Browser.',
    'ERROR_MESSAGE_INPUT_EMPTYPASS' => 'Bitte gib ein Passwort ein.',
    'ERROR_MESSAGE_INPUT_INVALIDAMOUNT' => 'Bitte gib eine gültige Menge für den Artikel ein!',
    'ERROR_MESSAGE_INPUT_NOVALIDEMAIL' => 'Bitte gib eine gültige E-Mail-Adresse ein',
    'ERROR_MESSAGE_INVITE_INCORRECTEMAILADDRESS' => 'Ungültige E-Mail-Adresse. Bitte überprüfe die E-Mail-Adressen.',
    'ERROR_MESSAGE_MANDATES_EXCEEDED' => 'Die Anzahl der lizenzierten Mandanten ist überschritten. Trage bitte im Shop Admin einen gültigen Lizenzschlüssel ein oder kontaktiere',
    'ERROR_MESSAGE_PASSWORD_EMAIL_INVALID' => 'Bitte gib eine gültige E-Mail-Adresse ein!',
    'ERROR_MESSAGE_PASSWORD_LINK_EXPIRED' => 'Diese Seite ist nicht mehr gültig. Bitte benutze die Funktion "Passwort vergessen?" erneut.',
    'ERROR_MESSAGE_USER_USEREXISTS' => '%s konnte nicht registriert werden. Hast Du bereits ein Kundenkonto bei uns?',
    'HERE_YOU_CAN_ENETER_MESSAGE' => 'Hier kannst Du uns noch etwas mitteilen.',
    'HERE_YOU_SET_UP_NEW_PASSWORD' => 'Kein Problem! Hier kannst Du ein neues Passwort einrichten.',
    'MESSAGE_ALREADY_RATED' => 'Du hast schon bewertet!',
    'MESSAGE_LOGIN_TO_WRITE_REVIEW' => 'Du musst angemeldet sein, um eine Bewertung schreiben zu können.',
    'MESSAGE_NEWSLETTER_SUBSCRIPTION_ACTIVATED' => 'Du bist nun für den Empfang unseres Newsletters freigeschaltet.',
    'MESSAGE_NEWSLETTER_SUBSCRIPTION_CANCELED' => 'Du hast den Newsletter erfolgreich abbestellt.',
    'MESSAGE_NO_SHIPPING_METHOD_FOUND' => 'Keine Versandarten gefunden. Bitte kontaktiere uns telefonisch oder per E-Mail!',
    'MESSAGE_PAYMENT_SELECT_ANOTHER_PAYMENT' => 'Bitte wähle eine andere Zahlungsart!',
    'MESSAGE_PAYMENT_BANK_CODE_INVALID' => 'Bitte gib eine gültige Bankleitzahl an!',
    'MESSAGE_PAYMENT_ACCOUNT_NUMBER_INVALID' => 'Bitte gib eine gültige Kontonummer an!',
    'MESSAGE_PLEASE_CONTACT_SUPPORT' => 'Bitte wende Dich an den technischen Support!',
    'MESSAGE_PLEASE_DELETE_FOLLOWING_DIRECTORY' => 'Bitte lösche folgendes Verzeichnis',
    'MESSAGE_RATE_THIS_ARTICLE' => 'Bewerte diesen Artikel!',
    'MESSAGE_READ_DETAILS' => 'Lies Details zum',
    'MESSAGE_RECOMMEND_CLICK_ON_SEND' => 'Klicke auf "Empfehlung abschicken", um die E-Mail zu senden!',
    'MESSAGE_SELECT_AT_LEAST_ONE_PRODUCT' => 'Bitte wähle mindestens einen Artikel aus!',
    'MESSAGE_SELECT_MORE_PRODUCTS' => 'Bitte wähle Artikel zum Vergleichen aus!',
    'MESSAGE_SENT_CONFIRMATION_EMAIL' => 'Du hast soeben eine E-Mail erhalten, mit der Du unseren Newsletter freischalten kannst.',
    'MESSAGE_UNAVAILABLE_SHIPPING_METHOD' => 'Die von Dir gewählte Versandart ist nicht mehr verfügbar. Bitte wähle eine andere Versandart aus!',
    'MESSAGE_WE_WILL_INFORM_YOU' => 'Sollte etwas nicht lieferbar sein, werden wir Dich sofort informieren.',
    'MESSAGE_WRONG_VERIFICATION_CODE' => 'Der Prüfcode, den Du eingegeben hast, ist nicht korrekt. Bitte versuche es erneut!',
    'MESSAGE_YOU_RECEIVED_ORDER_CONFIRM' => 'Du hast bereits eine Bestellbestätigung per E-Mail erhalten.',
    'PLEASE_SELECT_STATE' => 'Bitte wähle ein Bundesland aus',
    'QUESTIONS_ABOUT_THIS_PRODUCT_2' => '[?] Du hast Fragen zu diesem Artikel?',
    'READ_AND_CONFIRM_TERMS' => 'Bitte bestätige unsere Allg. Geschäftsbedingungen!',
    'REQUEST_PASSWORD_AFTERCLICK' => 'Du erhälst eine E-Mail mit einem Link, um ein neues Passwort zu vergeben.',
    'SELECT_LISTMANIA_LIST' => 'Wähle die Lieblingsliste',
    'RATE_OUR_SHOP' => 'Bitte nimm Dir eine Minute, um unseren Shop zu bewerten.',
    'WRITE_REVIEW_2' => 'Bewerte unseren Shop!',
    'DOWNLOADS_EMPTY' => 'Du hast bisher noch keine Dateien bestellt.',
    'COOKIE_NOTE_DISAGREE' => 'Bist Du damit nicht einverstanden, klicke bitte hier.',
    'PAGE_DETAILS_THANKYOUMESSAGE3' => 'Du bekommst eine Nachricht von uns sobald der Preis unter',
    'DETAILS_CHOOSEVARIANT' => 'Bitte wähle eine Variante',
    'PRICE_ALERT_THANK_YOU_MESSAGE' => 'Vielen Dank für Deine Nachricht an %s. Du bekommst eine Nachricht von uns, sobald der Preis unter %s %s fällt.',
    'ABOUT_BODYNOVA' => 'ÜBER BODYNOVA',
    'NEWSLETTERFOOTER' => 'BLEIB AUF DEM LAUFENDEN',
    'ORDERGBINFO' => 'Für eine Lieferung nach Großbritannien haben Sie leider den Mindestbestellwert von €160,- noch nicht erreicht.',
	'PAGE_TITLE_START'                                            => 'Bodynova',
    'PHONE_FRANCE' => '',
    'STARTSEITEH1' => 'Willkommen bei bodynova.de - Deinem Shop für Yoga Zubehör, Yogamatten, Meditationskissen & Massagebedarf',
);

/*
[{ oxmultilang ident="GENERAL_YOUWANTTODELETE"}]
*/
