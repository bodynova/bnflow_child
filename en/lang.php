<?php
/**
 * This file is part of OXID eSales Flow theme.
 *
 * OXID eSales Flow theme is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OXID eSales Flow theme is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OXID eSales Flow theme.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 */

$sLangName = "English";

$aLang = array(
    'charset' => 'UTF-8',

    // Global
    'DD_SORT_DESC' => 'descending',
    'DD_SORT_ASC' => 'ascending',
    'DD_CLOSE_MODAL' => 'close',
    'DD_DEMO_ADMIN_TOOL' => 'Open admin interface',

    // Form validation
    'DD_FORM_VALIDATION_VALIDEMAIL' => 'Please enter a valid email address.',
    'DD_FORM_VALIDATION_PASSWORDAGAIN' => 'Passwords do not match.',
    'DD_FORM_VALIDATION_NUMBER' => 'Please enter a number.',
    'DD_FORM_VALIDATION_INTEGER' => 'No decimal places allowed.',
    'DD_FORM_VALIDATION_POSITIVENUMBER' => 'Please enter a positive number.',
    'DD_FORM_VALIDATION_NEGATIVENUMBER' => 'Please enter a negative number.',
    'DD_FORM_VALIDATION_REQUIRED' => 'Please specify a value for this required field.',
    'DD_FORM_VALIDATION_CHECKONE' => 'Please select at least one option.',

    // Header
    'SEARCH_TITLE' => 'Enter a search term...',
    'SEARCH_SUBMIT' => 'Search',

    // Sidebar
    'DD_SIDEBAR_CATEGORYTREE' => 'Categories',

    // Footer
    'FOOTER_NEWSLETTER_INFO' => 'Subscribe to our newsletter and get 5&euro; discount',
    'FAQ' => 'FAQ - Frequently asked questions',
    'UnserYogastudio' => 'Our Yoga Studio in Cologne',
    'KATALOGEDOWNLOADS' => 'Catalogues & Downloads',

    // Home page
    'MANUFACTURERSLIDER_SUBHEAD' => 'Our brands',
    'START_BARGAIN_HEADER' => 'Sale of the week',
    'START_NEWEST_HEADER' => 'New in shop',
    'START_TOP_PRODUCTS_HEADER' => 'Our best sellers',
    'START_BARGAIN_SUBHEADER' => 'Discover the latest sales and offers!',
    'START_NEWEST_SUBHEADER' => 'Our latest products from the categories Yoga, Meditation, Wellness and Lifestyle',
    'START_TOP_PRODUCTS_SUBHEADER' => 'The 5 most popular products in our shop.',

    // Contact form
    'DD_CONTACT_PAGE_HEADING' => 'Contact us',
    'DD_CONTACT_FORM_HEADING' => 'Contact form',
    'DD_CONTACT_ADDRESS_HEADING' => 'Address',
    'DD_CONTACT_THANKYOU1' => "Thank you for your message to",
    'DD_CONTACT_THANKYOU2' => " .",

    // Link list
    'DD_LINKS_NO_ENTRIES' => 'Unfortunately, there are no links available.',

    // 404 page
    'DD_ERR_404_START_TEXT' => 'Perhaps you can find what you are looking for on our home page:',
    'DD_ERR_404_START_BUTTON' => 'Go to home page',
    'DD_ERR_404_CONTACT_TEXT' => 'How can we help?<br>Feel free to call or write us an email:',
    'DD_ERR_404_CONTACT_BUTTON' => 'to the contact form',

    // Login
    'DD_LOGIN_ACCOUNT_PANEL_CREATE_TITLE' => 'Open account',
    'DD_LOGIN_ACCOUNT_PANEL_CREATE_BODY' => 'By creating an account you can order more quckly and easily. In your account you can also save different delivery addresses and track the status of your orders.',
    'DD_LOGIN_ACCOUNT_PANEL_LOGIN_TITLE' => 'Login',

    // Billing address
    'DD_USER_BILLING_LABEL_STATE' => 'State:',
    'DD_USER_SHIPPING_LABEL_STATE' => 'State:',
    'DD_USER_SHIPPING_SELECT_ADDRESS' => 'Select',
    'DD_USER_SHIPPING_ADD_DELIVERY_ADDRESS' => 'Add new delivery address',

    // Order history
    'DD_ORDER_ORDERDATE' => 'Date:',

    // List views
    'DD_LISTLOCATOR_FILTER_ATTRIBUTES' => 'Filter:',
    'DD_LIST_SHOW_MORE' => 'View products...',

    // Recommendation list
    'DD_RECOMMENDATION_EDIT_BACK_TO_LIST' => 'Back to overview',

    // Downloads
    'DD_DOWNLOADS_DOWNLOAD_TOOLTIP' => 'Download',
    'DD_FILE_ATTRIBUTES_FILESIZE' => 'File size:',
    'DD_FILE_ATTRIBUTES_OCLOCK' => '',
    'DD_FILE_ATTRIBUTES_FILENAME' => 'File name:',

    // Details page
    'BACK_TO_OVERVIEW' => 'To overview',
    'OF' => 'of',
    'DD_PRODUCTMAIN_STOCKSTATUS' => 'Stock level',
    'DD_RATING_CUSTOMERRATINGS' => 'Customer reviews',
    'PAGE_DETAILS_CUSTOMERS_ALSO_BOUGHT_SUBHEADER' => 'Customers who bought this item also bought one of the following products.',
    'WIDGET_PRODUCT_RELATED_PRODUCTS_ACCESSORIES_SUBHEADER' => 'The following products fit well with this product.',
    'WIDGET_PRODUCT_RELATED_PRODUCTS_SIMILAR_SUBHEADER' => 'You may also like this product:',
    'WIDGET_PRODUCT_RELATED_PRODUCTS_CROSSSELING_SUBHEADER' => 'Customers who viewed this product also viewed the following products.',
    'DETAILS_VPE_MESSAGE_1' => "This product can only be ordered in packaging units of ",
    'DETAILS_VPE_MESSAGE_2' => "",

    // Modal basket
    'DD_MINIBASKET_MODAL_TABLE_TITLE' => 'Article',
    'DD_MINIBASKET_MODAL_TABLE_PRICE' => 'Total',
    'DD_MINIBASKET_CONTINUE_SHOPPING' => 'Continue shopping',

    // Checkout
    'DD_BASKET_BACK_TO_SHOP' => 'Back to shop',

    // E-Mails
    'DD_ORDER_CUST_HEADING' => 'Order',
    'DD_FOOTER_FOLLOW_US' => 'Follow us:',
    'DD_FOOTER_CONTACT_INFO' => 'Contact:',
    'DD_FORGOT_PASSWORD_HEADING' => 'Forgot password',
    'DD_INVITE_HEADING' => 'Product recommendation',
    'DD_INVITE_LINK' => 'Link',
    'DD_NEWSLETTER_OPTIN_HEADING' => 'Your newsletter subscription',
    'DD_ORDERSHIPPED_HEADING' => 'Shipping confirmation - Order',
    'DD_OWNER_REMINDER_HEADING' => 'Low stock',
    'DD_PRICEALARM_HEADING' => 'Price alarm',
    'DD_REGISTER_HEADING' => 'Your registration',
    'DD_DOWNLOADLINKS_HEADING' => 'Your download links - Order',
    'DD_SUGGEST_HEADING' => 'Product recommendation',
    'DD_WISHLIST_HEADING' => 'Wish list',

    'DD_ROLES_BEMAIN_UIROOTHEADER' => 'Menu',
    'WIDGET_SERVICES_CONTACTS' => 'Contact',
    'PREIS' => 'Price',
    'FREE' => 'Free',
    'SUCHBEGRIFF' => 'Add search keyword...',
    'Newslettercode' => 'Send coupon code by mail',
    'Newslettercodebetreff' => 'Your Bodynova voucher code',
    'Newslettercodebody' => 'code: NWSLTBN Minimum order value 30 EUR, cannot be combined with other discounts',
    'MESSAGE_NEWSLETTER_CONGRATULATIONS' => 'Welcome',
    'STAY_INFORMED' => 'Receive news from the Bodynova world!',
    'Retouren' => 'Returns',
    'BLOCK_PRICE' => 'Quantity discounts',
    'RetourenLabel' => 'Return shipping label',
    'VOUCHERMELDUNG' => 'Before you can use your voucher code, you need to first register (if you do not already have an account) or login if you have an account.',
    'TELEFONNUMMER' => ' +49-221-3566350',
    'OEFFNUNGSZEITEN' => 'Mon-Fri: 11 am - 3 pm',
    'DELIVERYTIME_DELIVERYTIME' => 'production time',

    'DD_DELETE_MY_ACCOUNT_WARNING' => 'This action cannot be undone. This will permanently delete your personal data.',
    'DD_DELETE_MY_ACCOUNT' => 'Delete account',
    'DD_DELETE_MY_ACCOUNT_CONFIRMATION_QUESTION' => 'Are you sure you want to delete your account?',
    'DD_DELETE_MY_ACCOUNT_CANCEL' => 'Cancel',
    'DD_DELETE_MY_ACCOUNT_SUCCESS' => 'The account has been deleted',
    'DD_DELETE_MY_ACCOUNT_ERROR' => 'The account could not have been deleted',

    // Account -> My product reviews
    'DD_DELETE_REVIEW_AND_RATING' => 'Delete review and star rating',
    'DD_REVIEWS_NOT_AVAILABLE' => 'No reviews available',
    'DD_DELETE_REVIEW_CONFIRMATION_QUESTION' => 'Are you sure you want to delete the review?',

    'DD_CATEGORY_RESET_BUTTON' => 'Reset',
    'ZAHLUNGSMOEGLICHKEITEN' => 'METHODS OF PAYMENT',
    'VERSANDMIT' => 'SHIP WITH',
    'NEWSLETTERFOOTER' => 'STAY UP TO DATE',
    'ABOUT_BODYNOVA' => 'ABOUT BODYNOVA',
    'ORDERGBINFO' => 'For delivery to the UK, you have not yet reached the minimum order amount of €160.',
	'PAGE_TITLE_START'                                            => 'Bodynova',
	// Ampel Texte
	'READY_FOR_SHIPPING' => 'In stock.',
	'LOW_STOCK' => 'Limited stock - order quickly!',
	'AVAILABLE_ON' => 'Available from %s',
	'AVAILABLE_ON_NOT_BUYABLE' => 'Expected for %s',
	'MESSAGE_NOT_ON_STOCK' => 'Currently not available.',
    'PHONE_FRANCE' => '',
    'STARTSEITEH1' => 'Welcome to bodynova.de - your shop for yoga accessories, yoga mats, meditation cushions & massage supplies',
);

/*
[{ oxmultilang ident="GENERAL_YOUWANTTODELETE"}]
*/
