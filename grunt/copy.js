module.exports = {

    fonts: {
        files: [
            {
                expand: true,
                src: '*',
                cwd: 'build/vendor/font-awesome/fonts/',
                dest: 'out/bnflow_child/src/fonts/'
            },
            {
                expand: true,
                src: '*',
                cwd: 'build/vendor/jquery-flexslider2/fonts/',
                dest: 'out/bnflow_child/src/fonts/'
            },
            {
                expand: true,
                src: '*',
                cwd: 'build/vendor/photoswipe/img/',
                dest: 'out/bnflow_child/src/bg/'
            },
            {
                expand: true,
                src: 'bndre.js',
                cwd: 'build/js/',
                dest: 'out/bnflow_child/src/js/'
            }
        ]
    }

};
