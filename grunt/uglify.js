module.exports = {

    theme: {
        files: {
            "out/bnflow_child/src/js/scripts.min.js": [
                "build/vendor/bootstrap/js/bootstrap.js",
                "build/vendor/bootstrap-select/js/bootstrap-select.js",
                "build/vendor/jquery-bootstrap-validation/js/jqBootstrapValidation.js",
                "build/vendor/jquery-unveil/js/jquery.unveil.js",
	            "build/js/jquery.hoverIntent.js",
                "build/js/main.js"
            ],
            "out/bnflow_child/src/js/pages/compare.min.js":     "build/js/pages/compare.js",
            "out/bnflow_child/src/js/pages/details.min.js":     "build/js/pages/details.js",
            "out/bnflow_child/src/js/pages/review.min.js":      "build/js/pages/review.js",
	        "out/bnflow_child/src/js/pages/background.min.js":  "build/js/jquery.adaptive-backgrounds.js",
	        "out/bnflow_child/src/js/pages/coljquery.min.js":   "build/js/jquery.js",
	        "out/bnflow_child/src/js/pages/color-thief.min.js": "build/js/color-thief.js",
            "out/bnflow_child/src/js/pages/start.min.js":       "build/js/pages/start.js"
        }
    },

    vendor: {
        files: {
            "out/bnflow_child/src/js/libs/jquery.min.js":                "build/vendor/jquery/js/jquery.js",
            "out/bnflow_child/src/js/libs/jquery.cookie.min.js":         "build/vendor/jquery-cookie/js/jquery.cookie.js",
            "out/bnflow_child/src/js/libs/jquery.flexslider.min.js":     "build/vendor/jquery-flexslider2/js/jquery.flexslider.js",
            "out/bnflow_child/src/js/libs/jquery-ui.min.js":             "build/vendor/jquery-ui/js/jquery-ui.js",
            "out/bnflow_child/src/js/libs/jqBootstrapValidation.min.js": "build/vendor/jquery-bootstrap-validation/js/jqBootstrapValidation.js",
            "out/bnflow_child/src/js/libs/photoswipe.min.js":            "build/vendor/photoswipe/js/photoswipe.js",
            "out/bnflow_child/src/js/libs/photoswipe-ui-default.min.js": "build/vendor/photoswipe/js/photoswipe-ui-default.js",
	        "out/bnflow_child/src/js/libs/jquery.chameleon.min.js":      "build/js/jquery.chameleon.js",
	        "out/bnflow_child/src/js/libs/chameleonDebug.min.js":        "build/js/chameleonDebug.js",
	        "out/bnflow_child/src/js/libs/jquery.elevatezoom.min.js":    "build/js/jquery.elevatezoom.js",
	        //"out/bnflow_child/src/js/hoverintend.min.js":                "build/js/jquery.hoverIntend.js",
            /*"out/bnflow_child/src/js/modernizr.min.js":                  "build/js/modernizr.js",
            "out/bnflow_child/src/js/jquery.menu-aim.min.js":            "build/js/jquery.menu-aim.js",
            "out/bnflow_child/src/js/menu-main.min.js":                  "build/js/menu-main.js",*/
            "out/bnflow_child/src/js/jquery.sticky.min.js":              "build/js/jquery.sticky.js",
            "out/bnflow_child/src/js/stellarnav.min.js":                 "build/js/stellarnav.js",
            "out/bnflow_child/src/js/jquery.ui.touch-punch.min.js":      "build/js/jquery.ui.touch-punch.js",
            //"out/bnflow_child/src/js/jquery.svg.min.js":                 "build/js/jquery.svg.js",
            "out/bnflow_child/src/js/owl.carousel.min.js":               "build/js/owl.carousel.js",
            "out/bnflow_child/src/js/sidebar-responsive.min.js":         "build/js/sidebar-responsive.js"
	        //"out/bnflow_child/src/js/bndre.min.js":                      "build/js/bndre.js"
        }
    },

    widgets: {
        files: [
            {
                expand: true,
                src:    "*.js",
                cwd:    "build/vendor/oxid-esales/js/",
                dest:   "out/bnflow_child/src/js/widgets/",
                ext:    ".min.js",
                extDot: "last"
            }
        ]
    }

};
