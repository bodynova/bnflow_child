/**
 * Created by andre on 15.03.18.
 */

function nextTab(elem) {
    "use strict";
    $(elem).next().find('a[data-toggle="tab"]').click();
}
function prevTab(elem) {
    "use strict";
    $(elem).prev().find('a[data-toggle="tab"]').click();
}

function sidebar() {
    "use strict";
    if ($('#debugbarLeft').hasClass('active')) {

        $('#debugPlatzhalter').css({
            'display':'block'
        }).animate({
            'width': '250px'
        }, {
            complete: function () {
                if (!$('body').hasClass('fixed-header')) {
                    var toppos = 236 - $(window).scrollTop();
                    $('#debugbarLeft').css({
                        'position': 'relative',
                        'top': toppos
                    }).removeClass('active');
                }else {
                    $('#debugbarLeft').css({
                        'position': 'fixed',
                        'top': '55px'
                    }).removeClass('active');
                }
            }
        });
    } else {
        $('#debugbarLeft').addClass('active');

        $('#debugPlatzhalter').animate({
            'width': '0'
        }, {
            complete: function () {
                $(this).css({
                    'display':'none'
                });
            }
        });
    }
    return false;
}

/* end Checkout Steps  */

// -----------
// Debugger that shows view port size. Helps when making responsive designs.
// -----------
/*
function showViewPortSize(display) {
    "use strict";
    if(display) {
        var height = window.innerHeight;
        var width  = window.innerWidth;
        var bnsize = 'col-xs';
        jQuery('body').prepend('<div id="viewportsize" style="z-index:9999;position:fixed;/*bottom:0px;left:0px;color:#fff;background:#000;padding:10px">Height: '+height+'<br>Width: '+width+'<br>Grid: '+bnsize+'</div>');
        jQuery(window).resize(function() {
            height = window.innerHeight;
            width  = window.innerWidth;
            if(window.innerWidth < 768){
                bnsize = 'col-xs';
            }
            if(window.innerWidth > 768 && window.innerWidth < 992){
                bnsize = 'col-sm';
            }
            if(window.innerWidth > 992 && window.innerWidth < 1200){
                bnsize = 'col-md';
            }
            if(window.innerWidth > 1200){
                bnsize = 'col-lg';
            }
            jQuery('#viewportsize').html('Height: '+height+'<br>Width: '+width+'<br>Grid: '+bnsize);

            /*jQuery('#viewportsize').draggable();*/

            /*$('#viewportsize').draggable();

        });
    }

}
*/


/*
Promo Slider
*/
//(function ($) {
//    'use strict';
    /*jQuery(function ($) {*/
    // Instantiate the Bootstrap carousel
    /*$('.multi-item-carousel').carousel({
        interval: false
    });*/

    // for every slide in carousel, copy the next slide's item in the slide.
    // Do the same for the next, next item.
    /*function initMultiItemCarousels($) {

        $('.multi-item-carousel').each(function () {

            var parent = $(this),
                actives = 1,
                helper = "xs-items-1",
                bodyWidth = $("body").width(),
                screenSizes = ["xs"],
                finalHelper = {};

            if (bodyWidth >= 768) {
                screenSizes.push("sm");
            }
            if (bodyWidth >= 992) {
                screenSizes.push("md");
            }
            if (bodyWidth >= 1200) {
                screenSizes.push("lg");
            }
            screenSizes.reverse();

            if (parent.attr("class").length) {
                helper = parent.attr("class");
            }

            var patt = /(xs|sm|md|lg)\-items\-(\d+)/ig, item;

            while (item = patt.exec(helper)) {
                finalHelper[item[1].toLocaleLowerCase()] = item[2];
            }

            for (var x in screenSizes) {
                if (finalHelper.hasOwnProperty(screenSizes[x])) {
                    actives = finalHelper[screenSizes[x]];
                    break;
                }
            }

            if (actives > 0) {
                $('.item', parent).each(function () {
                    var obj = $(this), next = obj, num = actives;

                    // remove old cloned items if exists
                    obj.children().filter(":gt(0)").remove();


                    while (num > 1) {
                        num--;
                        next = next.next();
                        if (!next.length) {
                            next = $('.item', parent).eq(0).removeClass('item-left item-middle item-right');
                        }
                        next.children(':first-child').clone().appendTo(obj).removeClass('item-left item-middle item-right');     //.addClass("item-" + num);//.removeClass('item-right');
                    }

                    if (bodyWidth >= 1200) {
                        obj.children().removeClass('item-left item-middle item-right');
                        //console.log('3 Elemente');
                        obj.children(':first-child').addClass("item-left");
                        obj.children().first().next().addClass('item-middle');
                        obj.children().first().next().next().addClass('item-right');
                    }
                    if (bodyWidth >= 992 && bodyWidth <= 1200) {
                        obj.children().removeClass('item-left item-middle item-right');
                        //console.log('2 Elemente');
                    }
                    if (bodyWidth <= 992) {
                        obj.children().removeClass('item-left item-middle item-right');
                        //console.log('1 Element');
                    }

                });
            }
        });
    }

    // initial setup
    initMultiItemCarousels($);

    // re setup in screen resizes
    $.timeoutMultiItemCarousels = null;

    $(window).resize(function () {
        if ($.timeoutMultiItemCarousels) {
            clearTimeout($.timeoutMultiItemCarousels);
        }
        $.timeoutMultiItemCarousels = setTimeout(function () {
            initMultiItemCarousels($);
        }, 200);
    });
    /*});*/
//})(jQuery);

/*function startSlider(){
    "use strict";
    $('#multi-item-example').carousel({
        interval: false, //5000,
        pause: 'hover',
        wrap : true,
        keyboard : true
    });
}*/

$(document).ready(function () {
    "use strict";

/*
    Checkout Steps:
*/
	//Initialize tooltips
	$('.nav-tabs > li a[title]').tooltip();

	//Wizard
	$('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
		var $target = $(e.target);
		if ($target.parent().hasClass('disabled')) {
			return false;
		}
	});
	$(".next-step").click(function (e) {
		var $active = $('.wizard .nav-tabs li.active');
		$active.next().removeClass('disabled');
		nextTab($active);
	});
	$(".prev-step").click(function (e) {
		var $active = $('.wizard .nav-tabs li.active');
		prevTab($active);
	});

	$('.bntooltip').tooltip({ trigger: "hover" });


	/*
    $('.innerProductBox').hover(function(){
       $(this).css('border','1px solid #E7E7E7');
    });

    $('.innerProductBox').mouseleave(function () {
        $(this).css('border', '1px dashed #E7E7E7');
    });
	 */

    $(function () {
        $('[data-toggle="popover"]').popover({
            html:true ,
            trigger: 'hover',
            delay:{
                'show': 200,
                'hide': 800
            }
        });

    });

    /*
    $('[data-toggle="popover"]').on('hover',function () {
        $(this).popover();
    });
    */

/*
    Sidebar Button:
*/
/*
    $('#debugbarLeftCollapse').on('click', function () {
        sidebar();
    });

    $('nav.sidebar li.dropdown>a').on('hover', function (e) {
        console.log($(this));
        console.log(e);
        $(this).children('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
    }, function () {
        console.log($(this));
        console.log(e);
        $(this).children('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
    });



    htmlbodyHeightUpdate();
    $(window).resize(function () {
        htmlbodyHeightUpdate()
    });
    $(window).scroll(function () {
        height2 = $('.main').height();
        htmlbodyHeightUpdate();
    });
*/

/*
    Navbar dran hindern sich zu schließen, wenn interaktiver Content bedient wird
*/
    $(document).on('click', '.yamm .dropdown-menu', function(e) {
        e.stopPropagation();

        if($('#searchformhead').hidden === 'false'){
            console.log('sichtbar');
        } else {
            console.log('unsichtbar');
        }
        console.log(e);
    });

/*
    PromoSlider
*/
    //startSlider();

    // callback sobald slide aufgerufen wird
    /*$('#multi-item-example').on('slide.bs.carousel', function () {
        console.log('beginn des Slides');
        console.log($('#multi-item-example > .item.active'));
    });

    // callback sobald eine Bewegung zu ende geführt wurde
    $('#multi-item-example').on('slid.bs.carousel', function () {
        console.log('nach dem Slide');
        console.log($('#multi-item-example > .item.active'));
    });*/


/*
    Debugger Show Viewport
*/
    //showViewPortSize(true);


    /*
    Mega Navi Responsive Menue
     */


    $('.stellarnav').stellarNav({
        theme: 'plain', // plain, light, dark
        breakpoint: 990, // 768
        menuLabel: '', // String
        sticky: true,   // Makes Nav sticky on Scroll
        position: 'left', // static, top, left, right
        openingSpeed: 250, // Integer
        closingDelay: 250, // Integer
        showArrows: true, // Bool
        phoneBtn:   '', // Adds Click To Call Link to top Menue
        locationBtn:   '', // Adds a location link i.e: "/location/","http://site.com/contact-us/"
        closeBtn:   true, // Adds a close Btn to the End
        mobileMode: false, // Turns mobile Friendly by default
        scrollbarFix:  false // Fixes horizontal scrollbar issue on very logn menus
    });

    $('#viewportsize').draggable();


    /* Blureffekt bei betreten der Navigationsleiste ganze seite ausser Children */
    $('#mainnav > ul > li').mouseenter( function () {

        $('#mainnav > ul').addClass("hasFocus");

        if (!$('#mainnav').hasClass('mobile')) {
            setTimeout(function () {
                if ($('#mainnav > ul').hasClass("hasFocus")) {
                    $(".top-head").addClass('blurry');
                    $(".logo-col").addClass('blurry');
                    $(".symbol-col").addClass('blurry');
                    $('#wrapper').addClass('blurry');
                    $('.owl-carousel').addClass('blurry');
                }
            }, 500);

            $('#mainnav > ul > li').mouseleave(function () {
                $('#mainnav > ul').removeClass("hasFocus");
                $(".top-head").removeClass('blurry');
                $(".logo-col").removeClass('blurry');
                $(".symbol-col").removeClass('blurry');
                $('#wrapper').removeClass('blurry');
                $('.owl-carousel').removeClass('blurry');
            });
        }else{
            $('#mainnav > ul > li').mouseleave(function () {
                $('#mainnav > ul').removeClass("hasFocus");
            });
        }
    });



    /* Tooltips */

    $('[data-toggle="tooltip"]').tooltip();


    $(".owl-carousel").owlCarousel({
        //items:1,
        center:true,
        //stagePadding:200,
        //nav:true,
        //autoplay:true,
        loop:true,
        responsiveClass: true,
        responsive:{
            0:{
              items:1,
              nav:true,
              dots:false
            },
            770:{
                items:1,
                //stagePadding: 50,
                nav:true
            },
            990:{
                items:1,
                stagePadding: 100,
                nav: true
            },
            1200:{
                items:1,
                stagePadding: 200,
                nav: true
            }
        }
    });




});

/*
        Fixed Header
    */
$(document).on('scroll', function () {
    "use strict";
    if ($('body').hasClass('fixed-header') && !$('#debugbarLeft').hasClass('active')) {
        $('#debugPlatzhalter').css({
            'display': 'block',
            'width': '250px'
        });
        $('#debugbarLeft').css({
            'position': 'fixed',
            'top': '55px'
        });
    } else {
        $('#debugPlatzhalter').css({
            'display': 'none',
            'width': '0'
        });
        $('#debugbarLeft').css({
            'position': 'relative',
            'top': '0px'
        });
    }
});

/*
function htmlbodyHeightUpdate() {
    var height3 = $(window).height();
    var height1 = $('.nav').height() + 50;
    height2 = $('.main').height();
    if (height2 > height3) {
        $('html').height(Math.max(height1, height3, height2) + 10);
        $('body').height(Math.max(height1, height3, height2) + 10);
    } else {
        $('html').height(Math.max(height1, height3, height2));
        $('body').height(Math.max(height1, height3, height2));
    }

}
*/


$(document).ready(function(){
    $(" .owlgap").parent().addClass("bannerAbstand");
});

$(document).ready(function () {
    $('.productData').mouseover(function (e) {
        e.preventDefault();

        $(this).find('.text-shortdesc > .short').addClass('hide');
        $(this).find('.text-shortdesc > .long').removeClass('hide');

    }).mouseout(function (e) {

        $(this).find('.text-shortdesc > .short').removeClass('hide');
        $(this).find('.text-shortdesc > .long').addClass('hide');
    });
});
