/**
 * Created by andre on 03.09.15.
 */

/*<![CDATA[*/
function generatePlayList(playlistlocation,description,artist,picture,playerlocation)
{

    //console.info(playlistlocation,description,artist,picture,playerlocation )
    /*
    req = new XMLHttpRequest();
    req.overrideMimeType("text/plain");
    req.addEventListener("load", myCallBack, false);
    req.open("get", playlistlocation);
    req.send();
    */
    var myPlaylist = new Array();

    $.ajax({
        type: 'GET',
        url: playlistlocation,
        /*dataType: 'xml',
        mimeType: 'text/plain; charset=UTF-8',*/
        contentType: "text/xml",
        beforeSend: function (xhr) {
            xhr.overrideMimeType("text/xml");
        },
        success: function (response) {
            var tracks = $(response).children().children();
            //console.log('tracks');
            //console.log(tracks);

            var description = description;

            $(tracks).each(function (i) {
                var track = $(this);
                var path = track.children('path')[0].firstChild.data;
                var title = track.children('title')[0].firstChild.data;
                //console.log('path: ', path);
                //console.log('title: ', title)
                myPlaylist[i] = {
                    mp3: path,
                    oga: '',
                    title: title,
                    artist: artist,
                    //rating:3,
                    //buy:'',
                    price: '',
                    duration: '',
                    cover: picture
                };
            });
            //console.dir(myPlaylist);





            /*
            $(response).find('track').each(function (i) {
                var track = $(this);
                var path = track.find('path').text();
                var title = track.find('title').text();
                //var dauer = track.find('test').text();
                myPlaylist[i] = {
                    mp3: path,
                    oga: '',
                    title: title,
                    artist: artist,
                    //rating:3,
                    //buy:'',
                    price: '',
                    duration: '',
                    cover: picture
                };
                //console.log(dauer);
                console.log('path: ' + path);
                console.log('myPlaylist: ' + myPlaylist);
            });
            console.dir(myPlaylist);
            $('#playerbox').ttwMusicPlayer(myPlaylist, {
                autoPlay: true,
                description: description,
                jPlayer: {
                    swfPath: playerlocation //You need to override the default swf path any time the directory structure changes
                }
            });
             */
        },
        error: function (error) {
            console.log(error);
        }
    }).done(function () {
        console.dir(myPlaylist);
        $("#playerbox").ttwMusicPlayer(myPlaylist, {
            autoPlay: false,
            description: description,
            jPlayer: {
                swfPath: playerlocation //You need to override the default swf path any time the directory structure changes
            }
        });
       /* //data = JSON.parse(data);
        var xmlDoc = $.parseXML(data);
        //response = JSON.parse(response);
        console.log('xmlDoc:');
        console.log(xmlDoc);
        /*
        var test = $(xmlDoc).children('xml').children('track');
        console.log('test');
        console.log(test);
        */
        /*
        var myPlaylist = new Array();
        var description = description;
        $(xmlDoc).find('track').each(function (i) {
            var track = $(this);
            var path = track.find('path').text();
            console.log('path: ' + path);
        });

         */



        /*
        $(xmlDoc).find('track').each(function (i) {
            var track = $(this);
            var path = track.find('path').text();
            var title = track.find('title').text();
            //var dauer = track.find('test').text();
            myPlaylist[i] = {
                mp3: path,
                oga: '',
                title: title,
                artist: artist,
                //rating:3,
                //buy:'',
                price: '',
                duration: '',
                cover: picture
            };
            //console.log(dauer);
            console.log('path: ' + path);
            console.log('myPlaylist: ' + myPlaylist);
        });
         */

        /*
        $(xmlDoc).find('track').each(function (i) {
            var track = $(this);
            var path = track.find('path').text();
            var title = track.find('title').text();
            //var dauer = track.find('test').text();
            myPlaylist[i] = {
                mp3: path,
                oga: '',
                title: title,
                artist: artist,
                //rating:3,
                //buy:'',
                price: '',
                duration: '',
                cover: picture
            };
            //console.log(dauer);
            console.log('path: ' + path);
            console.log('myPlaylist: ' + myPlaylist);
        });
        console.dir(myPlaylist);
        $('#playerbox').ttwMusicPlayer(myPlaylist, {
            autoPlay: true,
            description: description,
            jPlayer: {
                swfPath: playerlocation //You need to override the default swf path any time the directory structure changes
            }
        });

         */
    });
    //console.log(reg);
    /*
    var xml = $.get(playlistlocation).done(function (xml) {
        var myPlaylist = new Array();
        var description = description;
        $(xml).find('track').each(function (i) {

            var track = $(this);
            var path = track.find('path').text();
            var title = track.find('title').text();
            //var dauer = track.find('test').text();

            myPlaylist[i] = {
                mp3: path,
                oga: '',
                title: title,
                artist: artist,
                //rating:3,
                //buy:'',
                price: '',
                duration: '',
                cover: picture
            }
            //console.log(dauer);
            console.log('path: ' + path);
            console.log('myPlaylist: ' + myPlaylist);
        });
        console.dir(myPlaylist);
        $('#playerbox').ttwMusicPlayer(myPlaylist, {
            autoPlay: true,
            description: description,
            jPlayer: {
                swfPath: playerlocation //You need to override the default swf path any time the directory structure changes
            }
        });
    });


    console.log('xml:');
    console.log(xml);
    */

    /*
    var xmlDoc = $.parseXML(xml);
    console.log('xmlDoc:');
    console.log(xmlDoc);
    */
/*
    $.get(playlistlocation, function(d){
        var myPlaylist = new Array();
        var description = description;
        $(d).find('track').each(function(i){

            var track = $(this);
            var path  = track.find('path').text();
            var title = track.find('title').text();
            var dauer = track.find('test').text();

            myPlaylist[i] = {
                mp3:path,
                oga:'',
                title:title,
                artist:artist,
                //rating:3,
                //buy:'',
                price:'',
                duration:'',
                cover:picture
            }
            //console.log(dauer);
            //console.log('path: ' + path);
            //console.log('myPlaylist: ' + myPlaylist);
        });
        //console.dir(myPlaylist);
        $('#playerbox').ttwMusicPlayer(myPlaylist, {
            autoPlay:true,
            description:description,
            jPlayer:{
                swfPath:playerlocation //You need to override the default swf path any time the directory structure changes
            }
        });
    });
    //console.log('$playerlocation');
*/
}
