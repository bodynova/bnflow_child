<table class="row footer">
	<tr bgcolor="#ebebeb">
		<td class="wrapper">
			<table class="six columns">
				<tr>
					<td class="left-text-pad">
						<h5>[{oxmultilang ident="DD_FOOTER_CONTACT_INFO"}]</h5>
                        [{oxcontent ident="oxemailfooter"}]
					</td>
					<td class="expander"></td>
				</tr>
			</table>
		</td>
        [{if $oViewConf->getViewThemeParam('sFacebookUrl') || $oViewConf->getViewThemeParam('sGooglePlusUrl') || $oViewConf->getViewThemeParam('sTwitterUrl') || $oViewConf->getViewThemeParam('sYouTubeUrl') || $oViewConf->getViewThemeParam('sBlogUrl')}]
			<td class="wrapper last">
				<table class="six columns">
					<tr>
						<td class="right-text-pad">
                            [{oxifcontent ident="mailsocialfooter" object="_cont"}]
                            [{$_cont->oxcontents__oxcontent->value}]
                            [{/oxifcontent}]
						</td>
						<td class="expander"></td>
					</tr>
				</table>
			</td>
        [{/if}]
	</tr>
</table>


<table class="row">
	<tr>
		<td class="wrapper last">

			<table class="twelve columns">
				<tr>
					<td align="left">
                        [{*ToDo*}]
					</td>
					<td class="expander"></td>
				</tr>
			</table>

		</td>
	</tr>
</table>
</td>
</tr>
</table>


</center>
</td>
</tr>
</table>
</body>
</html>