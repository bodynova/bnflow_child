[{* Important ! render page head and body to collect scripts and styles *}]
[{capture append="oxidBlock_pageHead"}]
    [{strip}]
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" id="Viewport"
		      content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">
		<meta http-equiv="Content-Type" content="text/html; charset=[{$oView->getCharSet()}]">
        [{assign var=sPageTitle value=$oView->getPageTitle()}]
		<title>[{block name="head_title"}][{$sPageTitle}][{/block}]</title>
        [{block name="head_meta_robots"}]
            [{if $oView->noIndex() == 1}]
				<meta name="ROBOTS" content="NOINDEX, NOFOLLOW">
            [{elseif $oView->noIndex() == 2}]
				<meta name="ROBOTS" content="NOINDEX, FOLLOW">
            [{/if}]
        [{/block}]

        [{block name="head_meta_description"}]
            [{if $oView->getMetaDescription()}]
				<meta name="description" content="[{$oView->getMetaDescription()}]">
            [{/if}]
        [{/block}]

        [{block name="head_meta_keywords"}]
            [{if $oView->getMetaKeywords()}]
				<meta name="keywords" content="[{$oView->getMetaKeywords()}]">
            [{/if}]
        [{/block}]

        [{block name="head_meta_open_graph"}]
			<meta property="og:site_name" content="[{$oViewConf->getBaseDir()}]">
			<meta property="og:title" content="[{$sPageTitle}]">
			<meta property="og:description" content="[{$oView->getMetaDescription()}]">
            [{if $oViewConf->getActiveClassName() == 'details'}]
				<meta property="og:type" content="product">
				<meta property="og:image" content="[{$oView->getActPicture()}]">
				<meta property="og:url" content="[{$oView->getCanonicalUrl()}]">
            [{else}]
				<meta property="og:type" content="website">
                [{assign var="sFavicon512File" value=$oViewConf->getViewThemeParam('sFavicon512File')}]
                [{*}]<meta property="og:image" content="[{$oViewConf->getImageUrl('basket.png')}]">[{*}]
				<meta property="og:image" content="[{$oViewConf->getImageUrl("favicons/`$sFavicon512File`")}]">
				<meta property="og:url" content="[{$oViewConf->getCurrentHomeDir()}]">
            [{/if}]
        [{/block}]

        [{assign var="canonical_url" value=$oView->getCanonicalUrl()}]
        [{block name="head_link_canonical"}]
            [{if $canonical_url}]
				<link rel="canonical" href="[{$canonical_url}]">
            [{/if}]
        [{/block}]

        [{block name="head_link_hreflang"}]
            [{if $oView->isLanguageLoaded()}]
                [{assign var="oConfig" value=$oViewConf->getConfig()}]
                [{foreach from=$oxcmp_lang item=_lng}]
                    [{if $_lng->id == $oConfig->getConfigParam('sDefaultLang')}]
						<link rel="alternate" hreflang="x-default" href="[{$_lng->link}]">
                    [{/if}]
					<link rel="alternate" hreflang="[{$_lng->abbr}]"
					      href="[{$_lng->link|oxaddparams:$oView->getDynUrlParams()}]">
                [{/foreach}]
            [{/if}]
        [{/block}]

        [{assign var="oPageNavigation" value=$oView->getPageNavigation()}]
        [{if $oPageNavigation}]
            [{if $oPageNavigation->previousPage}]
				<link rel="prev" href="[{$oPageNavigation->previousPage}]">
            [{/if}]
            [{if $oPageNavigation->nextPage}]
				<link rel="next" href="[{$oPageNavigation->nextPage}]">
            [{/if}]
        [{/if}]

        [{block name="head_link_favicon"}]
            [{assign var="sFavicon512File" value=$oViewConf->getViewThemeParam('sFavicon512File')}]
            [{if $sFavicon512File}]
				<!-- iOS Homescreen Icon (version < 4.2)-->
				<link rel="apple-touch-icon-precomposed" media="screen and (resolution: 163dpi)"
				      href="https://cdn.bodynova.de/out/bnflow_child/img/favicons/[{$sFavicon512File}]" >
				<!-- iOS Homescreen Icon -->
				<link rel="apple-touch-icon-precomposed"
				      href="https://cdn.bodynova.de/out/bnflow_child/img/favicons/[{$sFavicon512File}]" >
				<!-- iPad Homescreen Icon (version < 4.2) -->
				<link rel="apple-touch-icon-precomposed" media="screen and (resolution: 132dpi)"
				      href="https://cdn.bodynova.de/out/bnflow_child/img/favicons/[{$sFavicon512File}]" >
				<!-- iPad Homescreen Icon -->
				<link rel="apple-touch-icon-precomposed" sizes="72x72"
				      href="https://cdn.bodynova.de/out/bnflow_child/img/favicons/[{$sFavicon512File}]" >
				<!-- iPhone 4 Homescreen Icon (version < 4.2) -->
				<link rel="apple-touch-icon-precomposed" media="screen and (resolution: 326dpi)"
				      href="https://cdn.bodynova.de/out/bnflow_child/img/favicons/[{$sFavicon512File}]" >
				<!-- iPhone 4 Homescreen Icon -->
				<link rel="apple-touch-icon-precomposed" sizes="114x114"
				      href="https://cdn.bodynova.de/out/bnflow_child/img/favicons/[{$sFavicon512File}]" >
				<!-- new iPad Homescreen Icon and iOS Version > 4.2 -->
				<link rel="apple-touch-icon-precomposed" sizes="144x144"
				      href="https://cdn.bodynova.de/out/bnflow_child/img/favicons/[{$sFavicon512File}]" >
				<!-- Windows 8 -->
                [{assign var="sFaviconMSTileColor" value=$oViewConf->getViewThemeParam('sFaviconMSTileColor')}]
                [{if $sFaviconMSTileColor}]
					<meta name="msapplication-TileColor" content="[{$sFaviconMSTileColor}]">
					<!-- Kachel-Farbe -->
                [{/if}]
				<meta name="msapplication-TileImage"
				      content="https://cdn.bodynova.de/out/bnflow_child/img/favicons/[{$sFavicon512File}]">
            [{/if}]

			<!-- Shortcut Icons -->
            [{assign var="sFaviconFile"    value=$oViewConf->getViewThemeParam('sFaviconFile')}]
            [{assign var="sFavicon16File"  value=$oViewConf->getViewThemeParam('sFavicon16File')}]
            [{assign var="sFavicon32File"  value=$oViewConf->getViewThemeParam('sFavicon32File')}]
            [{assign var="sFavicon48File"  value=$oViewConf->getViewThemeParam('sFavicon48File')}]
            [{assign var="sFavicon64File"  value=$oViewConf->getViewThemeParam('sFavicon64File')}]
            [{assign var="sFavicon128File" value=$oViewConf->getViewThemeParam('sFavicon128File')}]

            [{if $sFaviconFile}]
				<link rel="shortcut icon" href="https://cdn.bodynova.de/out/bnflow_child/img/favicons/[{$sFaviconFile}]?rand=1"
				      type="image/x-icon" >
            [{/if}]
            [{if $sFavicon16File}]
				<link rel="icon" href="https://cdn.bodynova.de/out/bnflow_child/img/favicons/[{$sFavicon16File}]" sizes="16x16" >
            [{/if}]
            [{if $sFavicon32File}]
				<link rel="icon" href="https://cdn.bodynova.de/out/bnflow_child/img/favicons/[{$sFavicon32File}]" sizes="32x32" >
            [{/if}]
            [{if $sFavicon48File}]
				<link rel="icon" href="https://cdn.bodynova.de/out/bnflow_child/img/favicons/[{$sFavicon48File}]" sizes="48x48" >
            [{/if}]
            [{if $sFavicon64File}]
				<link rel="icon" href=https://cdn.bodynova.de/out/bnflow_child/img/favicons/[{$sFavicon64File}]" sizes="64x64" >
            [{/if}]
            [{if $sFavicon128File}]
				<link rel="icon" href="https://cdn.bodynova.de/out/bnflow_child/img/favicons/[{$sFavicon128File}]" sizes="128x128" >
            [{/if}]
        [{/block}]

        [{block name="base_style"}]
	        <link rel="stylesheet" type="text/css" href="https://cdn.bodynova.de/out/bnflow_child/src/css/styles.min.css" >
	        <link rel="stylesheet" type="text/css" href="https://cdn.bodynova.de/out/bnflow_child/src/js/musicplayer/plugin/css/style.css">
	        <link rel="stylesheet" type="text/css" href="https://cdn.bodynova.de/out/bnflow_child/src/js/musicplayer/plugin/css/demo.css">
            [{*oxstyle include="css/styles.min.css"*}]
        [{/block}]

        [{block name="base_fonts"}]
	        <link href="https://pro.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-6jHF7Z3XI3fF4XZixAuSu0gGKrXwoX/w3uFPxC56OtjChio7wtTGJWRW53Nhx6Ev" crossorigin="anonymous" rel="stylesheet">
	        <link href="https://fonts.googleapis.com/css?family=Hind:300,400,500,600|Kodchasan:600|Raleway:200,300,400,500,600" rel="stylesheet">

            [{*}]<link href="https://fonts.googleapis.com/css?family=Kodchasan:200,200i,300,300i,400,400i,500,500i,600,600i,700,700i|Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
                  rel="stylesheet">[{*}]

            [{* Font Awesome Pro *}]

            [{*}]
            <link href='https://fonts.googleapis.com/css?family=Raleway:200,400,700,600' rel='stylesheet' type='text/css'>
            <link href="https://fonts.googleapis.com/css?family=Kodchasan" rel="stylesheet">
            [{*}]
            [{*}]
            <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/solid.css" integrity="sha384-rdyFrfAIC05c5ph7BKz3l5NG5yEottvO/DQ0dCrwD8gzeQDjYBHNr1ucUpQuljos" crossorigin="anonymous">
            <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/regular.css" integrity="sha384-z3ccjLyn+akM2DtvRQCXJwvT5bGZsspS4uptQKNXNg778nyzvdMqiGcqHVGiAUyY" crossorigin="anonymous">
            <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/fontawesome.css" integrity="sha384-u5J7JghGz0qUrmEsWzBQkfvc8nK3fUT7DCaQzNQ+q4oEXhGSx+P2OqjWsfIRB8QT" crossorigin="anonymous">
            [{*}]
        [{/block}]

        [{assign var='rsslinks' value=$oView->getRssLinks()}]
        [{block name="head_link_rss"}]
            [{*if $rsslinks}]
                [{foreach from=$rsslinks item='rssentry'}]
                    <link rel="alternate" type="application/rss+xml" title="[{$rssentry.title|strip_tags}]" href="[{$rssentry.link}]">
                [{/foreach}]
            [{/if*}]
        [{/block}]

        [{block name="head_css"}]
            [{foreach from=$oxidBlock_head item="_block"}]
                [{$_block}]
            [{/foreach}]
        [{/block}]
    [{/strip}]
[{/capture}]

[{assign var="blIsCheckout"     value=$oView->getIsOrderStep()}]
[{assign var="blFullwidth"      value=$oViewConf->getViewThemeParam('blFullwidthLayout')}]
[{assign var="sBackgroundColor" value=$oViewConf->getViewThemeParam('sBackgroundColor')}]

[{* Fullpage Background *}]
[{if $oViewConf->getViewThemeParam('blUseBackground')}]
    [{assign var="sBackgroundPath"          value=$oViewConf->getViewThemeParam('sBackgroundPath')}]
    [{assign var="sBackgroundUrl"           value=$oViewConf->getImageUrl("backgrounds/`$sBackgroundPath`")}]
    [{assign var="sBackgroundRepeat"        value=$oViewConf->getViewThemeParam('sBackgroundRepeat')}]
    [{assign var="sBackgroundPosHorizontal" value=$oViewConf->getViewThemeParam('sBackgroundPosHorizontal')}]
    [{assign var="sBackgroundPosVertical"   value=$oViewConf->getViewThemeParam('sBackgroundPosVertical')}]
    [{assign var="sBackgroundSize"          value=$oViewConf->getViewThemeParam('sBackgroundSize')}]
    [{assign var="blBackgroundAttachment"   value=$oViewConf->getViewThemeParam('blBackgroundAttachment')}]

    [{if $sBackgroundUrl}]
        [{assign var="sStyle" value="background:`$sBackgroundColor` url(`$sBackgroundUrl`) `$sBackgroundRepeat` `$sBackgroundPosHorizontal` `$sBackgroundPosVertical`;"}]

        [{if $sBackgroundSize}]
            [{assign var="sStyle" value=$sStyle|cat:"background-size:`$sBackgroundSize`;"}]
        [{/if}]

        [{if $blBackgroundAttachment}]
            [{assign var="sStyle" value=$sStyle|cat:"background-attachment:fixed;"}]
        [{/if}]
    [{else}]
        [{assign var="sStyle" value="background:`$sBackgroundColor`;"}]
    [{/if}]
[{elseif $sBackgroundColor}]
    [{assign var="sStyle" value="background:`$sBackgroundColor`;"}]
[{/if}]

<!DOCTYPE html>
<html lang="[{$oView->getActiveLangAbbr()}]" [{block name="head_html_namespace"}][{/block}]>
<head>
    [{foreach from=$oxidBlock_pageHead item="_block"}]
        [{$_block}]
    [{/foreach}]
    [{oxstyle}]

	<meta charset="UTF-8">

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
	<!-- Microsoft Clarity  test 23.01.2022-->
	[{*}]
	<script type="text/javascript">
        (function (c, l, a, r, i, t, y) {
            c[a] = c[a] || function () {
                (c[a].q = c[a].q || []).push(arguments)
            };
            t = l.createElement(r);
            t.async = 1;
            t.src = "https://www.clarity.ms/tag/" + i;
            y = l.getElementsByTagName(r)[0];
            y.parentNode.insertBefore(t, y);
        })(window, document, "clarity", "script", "abe9uanm4h");
	</script>
	[{*}]
</head>
<body class="cl-[{$oView->getClassName()}][{if $smarty.get.plain == '1'}] popup[{/if}][{if $blIsCheckout}] is-checkout[{/if}][{if $oxcmp_user && $oxcmp_user->oxuser__oxpassword->value}] is-logged-in[{/if}]"[{if $sStyle}] style="[{$sStyle}]"[{/if}]>
[{oxid_include_dynamic file="widget/consentmanager.tpl"}]
[{* Theme SVG icons block *}]
[{block name="theme_svg_icons"}][{/block}]



[{*}]<div class="main">[{*}]


<div class="[{if $blFullwidth}]fullwidth-container[{else}]container[{/if}]">


    [{*include file="layout/right-buttons.tpl"*}]

    [{foreach from=$oxidBlock_pageBody item="_block"}]
        [{$_block}]
    [{/foreach}]

</div>

[{foreach from=$oxidBlock_pagePopup item="_block"}]
    [{$_block}]
[{/foreach}]

[{*oxscript include="js/modernizr.min.js" priority=1*}]
<script type="text/javascript" src="https://cdn.bodynova.de/out/bnflow_child/src/js/modernizr.min.js"></script>
[{*}]
Raleway:300,400,500,600
[{*}]
[{*}]
<link href="https://fonts.googleapis.com/css?family=Hind:300,600|Kodchasan:300,600|Raleway:300,600" rel="stylesheet"
      async>
[{*}]
<!-- Fluid -->
<link rel="fluid-icon" href="https://cdn.bodynova.de/out/bnflow_child/img/favicons/[{$sFavicon512File}]" title="[{$sPageTitle}]">


[{if $oViewConf->getTopActiveClassName() == 'details' && $oView->showZoomPics()}]
    [{include file="page/details/inc/photoswipe.tpl"}]
[{/if}]

[{block name="base_js"}]
    [{include file="i18n/dre_js_vars.tpl"}]
    [{*oxscript include="js/libs/jquery.min.js" priority=1*}]
	<script type="text/javascript" src="https://cdn.bodynova.de/out/bnflow_child/src/js/libs/jquery.min.js"></script>
    [{*oxscript include="js/libs/jquery-ui.min.js" priority=1*}]
	<script type="text/javascript" src="https://cdn.bodynova.de/out/bnflow_child/src/js/libs/jquery-ui.min.js"></script>
	<script type="text/javascript" src="https://cdn.bodynova.de/out/bnflow_child/src/js/libs/jquery.chameleon.min.js"></script>
    [{*oxscript include="js/libs/jquery.chameleon.min.js" priority=1*}]
    [{*oxscript include="js/libs/jquery.elevatezoom.min.js" priority=1*}]
	<script type="text/javascript" src="https://cdn.bodynova.de/out/bnflow_child/src/js/libs/jquery.elevatezoom.min.js"></script>
    [{*oxscript include="js/scripts.min.js" priority=1*}]
	<script type="text/javascript" src="https://cdn.bodynova.de/out/bnflow_child/src/js/scripts.min.js"></script>
    [{*oxscript include="js/hoverintend.min.js" priority=1*}]
    [{*oxscript include="js/jquery.sticky.min.js" priority=1*}]
	<script type="text/javascript" src="https://cdn.bodynova.de/out/bnflow_child/src/js/jquery.sticky.min.js"></script>
    [{*oxscript include="js/stellarnav.min.js" priority=1*}]
	<script type="text/javascript" src="https://cdn.bodynova.de/out/bnflow_child/src/js/stellarnav.min.js"></script>
    [{*}]
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/svg.js/3.0.5/polyfills.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/svg.js/3.0.5/svg.js"></script>
	[{*}]
    [{*oxscript include="js/jquery.svg.min.js" priority=1*}]
    [{*oxscript include="js/jquery.menu-aim.min.js" priority=1*}]
    [{*oxscript include="js/owl.carousel.min.js" priority=1*}]
	<script type="text/javascript" src="https://cdn.bodynova.de/out/bnflow_child/src/js/owl.carousel.min.js"></script>
    [{*oxscript include="js/menu-main.min.js" priority=1*}]
    [{*oxscript include="js/bndre.js" priority=1*}]
	<script type="text/javascript" src="https://bodynova.de/out/bnflow_child/src/js/bndre.js"></script>
    [{*oxscript include="js/counter.js" priority=1*}]
[{/block}]

[{if $oViewConf->isTplBlocksDebugMode()}]
    [{*oxscript include="js/widgets/oxblockdebug.min.js"*}]
	<script type="text/javascript" src="https://cdn.bodynova.de/out/bnflow_child/src/js/widgets/oxblockdebug.min.js"></script>
    [{oxscript add="$( 'body' ).oxBlockDebug();"}]
[{/if}]

<!--[if gte IE 9]>
<style type="text/css">.gradient {
	filter: none;
}</style><![endif]-->
[{oxscript}]

[{if !$oView->isDemoShop()}]
    [{oxid_include_dynamic file="widget/dynscript.tpl"}]
[{/if}]

[{foreach from=$oxidBlock_pageScript item="_block"}]
    [{$_block}]
[{/foreach}]

[{*include file="widget/svg/svgs.tpl"*}]
[{*}]</div>
<script type="text/JavaScript">
    [{php}]
    if (!strpos($_SERVER['HTTP_HOST'], '.local')) {
        echo
        "var produktiv = 1;";
        echo
        "var console = {};";
        echo
        "console.log = function () {};";
    } else {
        echo
        "console.log('Lokal und es darf gedebuggt werden.');";
    }
    [{/php}]
</script>
[{*}]

</body>
</html>
