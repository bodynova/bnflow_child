<div id="debugPlatzhalter" class="toggled active"></div>

<nav id="debugbarLeft" class="pull-left toggled active">
	[{*block name="widget_header_search_form"}]
		[{* wird erweitert durch das eigene Modul dre_solrsearch }]
	[{/block*}]
</nav>


[{*}]

Klasse:
[{$oView->getClassName()}]<br>
Template:
[{$oView->getTemplateName()}]<br>
BreadCrumb:
[{*$oView->getBreadCrumb()|var_dump}]
[{*}]

	[{*}]
	<form class="form search" role="form" action="http://bodyshoppe.local/index.php?" method="get" name="search">
		<input name="stoken" value="7A56B676" type="hidden">
		<input name="lang" value="0" type="hidden">
		<input name="cl" value="search" type="hidden">


		<div class="input-group">

			<input class="form-control" id="searchParam" name="searchparam" value="" placeholder="Suche" type="text">
			<span class="input-group-btn">
				<button type="submit" class="btn btn-primary" title="Suchen">
					<i class="fa fa-search"></i>
				</button>
			</span>
		</div>
	</form>

	<!-- Sidebar Links -->
	<ul class="list-unstyled components">


		<li class="active">
			<a href="#">Home</a>
		</li>
		<li>
			<a href="#">About</a>
		</li>

		<li><!-- Link with dropdown items -->
			<a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false">Pages</a>
			<ul class="collapse list-unstyled" id="homeSubmenu">
				<li><a href="#">Page</a></li>
				<li><a href="#">Page</a></li>
				<li><a href="#">Page</a></li>
			</ul>
		</li>

		<li><a href="#">Portfolio</a></li>
		<li><a href="#">Contact</a></li>
	</ul>
	<!-- <ul class="nav nav-pills nav-stacked" data-spy="affix" data-offset-top="205">-->
	<div id="sidebarLeft" class="col-xs-3 col-sm-3 col-md-2 col-lg-2">
		[{include file="layout/dreDebug.tpl"}]
	</div>

	[{*}]

