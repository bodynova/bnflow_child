[{block name="footer_main"}]
    [{assign var="blShowFullFooter" value=$oView->showSearch()}]
    [{assign var="blFullwidth" value=$oViewConf->getViewThemeParam('blFullwidthLayoutFooter')}]
    [{$oView->setShowNewsletter($oViewConf->getViewThemeParam('blFooterShowNewsletterForm'))}]

    [{if $oxcmp_user}]
        [{assign var="force_sid" value=$oView->getSidForWidget()}]
    [{/if}]

    <footer id="footer">
        <div class="[{if $blFullwidth}]container-fluid[{else}]container[{/if}]">
            <div class="row zahlungsrow">
                [{* Payment *}]

                <div class="col-lg-3 col-lg-offset-1 col-sm-6 col-xs-12 zahlungspossible" >
                    <div class="zahlungsklick" >
                        <div style="cursor:pointer;" onclick="location.href='[{oxgetseourl ident='zahlungsmöglichkeiten' type='oxcontent'}]'">


                    <div class="h4 footerhead footer-box-title">[{oxmultilang ident="ZAHLUNGSMOEGLICHKEITEN"}]</div>
                    <img loading="lazy" style="height: 28px" src="https://cdn.bodynova.de/out/bnflow_child/img/icon/paypal_bodynova_payment_footer_info.png" alt="payment-paypal">
                    <img loading="lazy" style="height: 28px; margin-left:15px;" src="https://cdn.bodynova.de/out/bnflow_child/img/icon/visa_bodynova_payment_footer_info.png" alt="payment-visa">
                    <img loading="lazy" style="height: 28px; margin-left:15px;" src="https://cdn.bodynova.de/out/bnflow_child/img/icon/mastercard_bodynova_payment_footer_info.png" alt="payment-mastercard">
                    <img loading="lazy" style="height: 28px; margin-left:15px;" src="https://cdn.bodynova.de/out/bnflow_child/img/icon/maestro_bodynova_payment_footer_info.png" alt="payment-ec-cash">
                    <br>
                    <br>
                    <img loading="lazy" style="height: 28px;" src="https://cdn.bodynova.de/out/bnflow_child/img/icon/sepa_bodynova_payment_footer_info.png" alt="payment-sepa">
                    <img loading="lazy" style="height: 28px; margin-left:15px;" src="https://cdn.bodynova.de/out/bnflow_child/img/icon/rechnungskauf_bodynova_payment_footer_info.png" alt="payment-rechnungskauf">
                    <img loading="lazy" style="height: 28px; margin-left:15px;" src="https://cdn.bodynova.de/out/bnflow_child/img/icon/vorkasse_bodynova_payment_footer_info.png" alt="payment-vorkasse">
                        </div>
                    </div>
                    [{*<img style="height: 30px; margin-left:15px;" src="[{$oViewConf->getCurrentHomeDir()}]out/bnflow_child/img/icon/maestro_bodynova_payment_footer_info.png" alt="payment-ec-cash">*}]
                </div>

                [{* Versand TODO: Übersetzungen *}]
                <div class="col-lg-3 col-sm-6 col-xs-12 abstandfooter">
                    <div class="versandklick">
                        [{*<div style="cursor:pointer;" onclick="location.href='[{oxgetseourl ident='0af41238fdb98bb20f620331caab0a32' type='oxcontent'}]'">*}]
                        <div style="cursor:pointer;" onclick="location.href='[{$oViewConf->getCurrentHomeDir()}]index.php?cl=bnversand'">
                            <div class="h4 footerhead footer-box-title">[{oxmultilang ident="VERSANDMIT"}]</div>
                            <div style="white-space: nowrap">

                                <img loading="lazy" style="height: 32px" src="https://cdn.bodynova.de/out/bnflow_child/img/icon/versand_anbieter_bodynova_dhl.png" alt="delivery-dhl">
                                <img loading="lazy" style="height: 32px; margin-left:15px;" src="https://cdn.bodynova.de/out/bnflow_child/img/icon/go_green_delivery_icon.png" alt="delivery-go-green">
                                <img loading="lazy" style="height: 32px; margin-left:15px;" src="https://cdn.bodynova.de/out/bnflow_child/img/icon/versand_anbieter_bodynova_gls.png" alt="delivery-gls">
                            </div>
                        </div>
                    </div>
                </div>

                [{if $oView->showNewsletter()}]

                    <div class="col-lg-3 col-sm-6 col-xs-12 footer-box footer-box-newsletter abstandfooter">
                        <div class="h4 footerhead footer-box-title">[{oxmultilang ident="NEWSLETTERFOOTER"}]</div>
                        <div class="footer-box-content" style="margin-top:-10px">
                            [{block name="dd_footer_newsletter"}]
                            <p class="small">[{oxmultilang ident="FOOTER_NEWSLETTER_INFO"}]</p>
                            [{include file="widget/footer/newsletter.tpl"}]
                            [{/block}]
                        </div>
                    </div>
                [{/if}]


                <div class="col-lg-2 col-sm-6 col-xs-12 footerSocial abstandfooter">
                    [{oxifcontent ident="socialfoot" object="_cont"}]
                    [{$_cont->oxcontents__oxcontent->value}]
                    [{/oxifcontent}]
                </div>

            </div>
            <div class="row secondrowFooter zahlungsrow">
                [{*<div class="container-fluid">
                    <div class="col-xs-12 col-md-7">
                        <div class="row">*}]
                            <div class="footer-left-part">
                                [{*$blFullwidth*}]
                                [{block name="dd_footer_servicelist"}]
                                    <section class="col-lg-3 col-lg-offset-1 col-sm-6 col-xs-12 zahlungspossible footer-box footer-box-service">
                                        <div class="h4 footerhead footer-box-title">[{oxmultilang ident="SERVICES"}]</div>
                                        <div class="footer-box-content">
                                            [{block name="dd_footer_servicelist_inner"}]
                                                [{oxid_include_widget cl="oxwServiceList" noscript=1 nocookie=1 force_sid=$force_sid}]
                                            [{/block}]
                                        </div>
                                    </section>
                                [{/block}]
                                [{block name="dd_footer_information"}]
                                    <section class="col-lg-3 col-sm-6 col-xs-12 footer-box footer-box-information">
                                        <div class="h4 footerhead footer-box-title">[{oxmultilang ident="ABOUT_BODYNOVA"}]</div>
                                        <div class="footer-box-content">
                                            [{block name="dd_footer_information_inner"}]
                                                [{oxid_include_widget cl="oxwInformation" noscript=1 nocookie=1 force_sid=$force_sid}]
                                            [{/block}]
                                        </div>
                                    </section>
                                [{/block}]
                                [{if $blShowFullFooter}]
                                    [{block name="dd_footer_manufacturerlist"}]
                                        <section class="col-lg-3 col-sm-6 col-xs-12 footer-box footer-box-manufacturers">
                                            <div class="h4 footerhead footer-box-title">[{oxmultilang ident="OUR_BRANDS"}]</div>
                                            <div class="footer-box-content">
                                                [{block name="dd_footer_manufacturerlist_inner"}]
                                                    [{oxid_include_widget cl="oxwManufacturerList" _parent=$oView->getClassName() noscript=1 nocookie=1}]
                                                [{/block}]
                                            </div>
                                        </section>
                                    [{/block}]

                                    [{*block name="dd_footer_categorytree"}]
                                        <section class="col-xs-12 col-sm-3 footer-box footer-box-categories">
                                            <div class="h4 footer-box-title">[{oxmultilang ident="CATEGORIES"}]</div>
                                            <div class="footer-box-content">
                                                [{block name="dd_footer_categorytree_inner"}]
                                                    [{oxid_include_widget cl="oxwCategoryTree" _parent=$oView->getClassName() sWidgetType="footer" noscript=1 nocookie=1}]
                                                [{/block}]
                                            </div>
                                        </section>
                                    [{/block*}]
                                [{/if}]
                                <section class="col-lg-2 col-sm-6 col-xs-12 worldwide">
                                    <div>
                                        [{oxifcontent ident="footerworldwide" object="_cont"}]
                                        [{$_cont->oxcontents__oxcontent->value}]
                                        [{/oxifcontent}]
                                    </div>
                                </section>
                            </div>
                [{* </div>
                    </div>
                    <div class="col-xs-12 col-md-5">
                        <div class="row">
                            <div class="footer-right-part">
                                <div class="col-xs-6 col-xs-offset-3 col-sm-12 col-sm-offset-0">

                                </div>
                            </div>
                        </div>
                    </div>
                </div> *}]
            </div>

            <div class="spacer"></div>

            [{* <<START>> Social Links *}]
            [{block name="dd_footer_social_links"}]
                [{if $oViewConf->getViewThemeParam('sFacebookUrl') || $oViewConf->getViewThemeParam('sGooglePlusUrl') || $oViewConf->getViewThemeParam('sTwitterUrl') || $oViewConf->getViewThemeParam('sYouTubeUrl') || $oViewConf->getViewThemeParam('sBlogUrl')}]
                    <div class="social-links">
                        <div class="row">
                            <section class="col-xs-12">
                                <div class="text-center">
                                    [{block name="dd_footer_social_links_inner"}]
                                        <ul class="list-inline">
                                            [{block name="dd_footer_social_links_list"}]
                                                [{if $oViewConf->getViewThemeParam('sFacebookUrl')}]
                                                    <li>
                                                        <a target="_blank" href="[{$oViewConf->getViewThemeParam('sFacebookUrl')}]">
                                                            <i class="fa fa-facebook"></i> <span>Facebook</span>
                                                        </a>
                                                    </li>
                                                [{/if}]
                                                [{if $oViewConf->getViewThemeParam('sGooglePlusUrl')}]
                                                    <li>
                                                        <a target="_blank" href="[{$oViewConf->getViewThemeParam('sGooglePlusUrl')}]">
                                                            <i class="fa fa-google-plus-square"></i> <span>Google+</span>
                                                        </a>
                                                    </li>
                                                [{/if}]
                                                [{if $oViewConf->getViewThemeParam('sTwitterUrl')}]
                                                    <li>
                                                        <a target="_blank" href="[{$oViewConf->getViewThemeParam('sTwitterUrl')}]">
                                                            <i class="fa fa-twitter"></i> <span>Twitter</span>
                                                        </a>
                                                    </li>
                                                [{/if}]
                                                [{if $oViewConf->getViewThemeParam('sYouTubeUrl')}]
                                                    <li>
                                                        <a target="_blank" href="[{$oViewConf->getViewThemeParam('sYouTubeUrl')}]">
                                                            <i class="fa fa-youtube-square"></i> <span>YouTube</span>
                                                        </a>
                                                    </li>
                                                [{/if}]
                                                [{if $oViewConf->getViewThemeParam('sBlogUrl')}]
                                                    <li>
                                                        <a target="_blank" href="[{$oViewConf->getViewThemeParam('sBlogUrl')}]">
                                                            <i class="fa fa-wordpress"></i> <span>Blog</span>
                                                        </a>
                                                    </li>
                                                [{/if}]
                                            [{/block}]
                                        </ul>
                                    [{/block}]
                                </div>
                            </section>
                        </div>
                    </div>
                [{/if}]
            [{/block}]
            [{* <<ENDE>> Social Links *}]
        </div>

        [{if $oView->isPriceCalculated()}]
            [{block name="layout_page_vatinclude"}]
                [{block name="footer_deliveryinfo"}]
                    [{oxifcontent ident="oxdeliveryinfo" object="oCont"}]
                        <div id="incVatInfo">
                            [{if $oView->isVatIncluded()}]
                                * <span class="deliveryInfo">[{oxmultilang ident="PLUS_SHIPPING"}]<a href="[{$oCont->getLink()}]">[{oxmultilang ident="PLUS_SHIPPING2"}]</a></span>
                            [{else}]
                                * <span class="deliveryInfo">[{oxmultilang ident="PLUS"}]<a href="[{$oCont->getLink()}]">[{oxmultilang ident="PLUS_SHIPPING2"}]</a></span>
                            [{/if}]
                        </div>
                    [{/oxifcontent}]
                [{/block}]
            [{/block}]
        [{/if}]
    </footer>

    <div class="legal">
        <div class="[{*if $blFullwidth}]container[{else}]container-fluid[{/if*}] container-fluid zahlungspossible" >
            <div class="legal-box">
                <div class="row zahlungsrow">
                    <section class="col-sm-11 col-lg-offset-1">
                        [{block name="dd_footer_copyright"}]
                            [{oxifcontent ident="oxstdfooter" object="oCont"}]
                                [{$oCont->oxcontents__oxcontent->value}]
                            [{/oxifcontent}]
                        [{/block}]
                    </section>
                </div>
            </div>
        </div>

    </div>
[{/block}]

[{if $oView->isRootCatChanged()}]
    <div id="scRootCatChanged" class="popupBox corners FXgradGreyLight glowShadow">
        [{include file="form/privatesales/basketexcl.tpl"}]
    </div>
[{/if}]
[{*include file="layout/navbarfooter.tpl"*}]