[{* if $oViewConf->getTopActionClassName() != 'clearcookies' && $oViewConf->getTopActionClassName() != 'mallstart'}]
    [{oxid_include_widget cl="oxwCookieNote" _parent=$oView->getClassName() nocookie=1}]
[{/if*}]

[{block name="header_main"}]
    [{assign var="blFullwidth" value=$oViewConf->getViewThemeParam('blFullwidthLayoutHead')}]

	[{include file="layout/topRow.tpl"}]

    <header id="header" >
        <div class="hidden-xs hidden-sm [{if $blFullwidth}]container-fluid[{else}]container[{/if}]" id="header-logo-bereich">
            <div class="header-box">
                <div class="container-fluid">
                    <div class="row row-flex display-flex-center">
                        <div class="col-md-3 logo-col">
                            <div class="content">
                                [{block name="layout_header_logo"}]
                                    [{assign var="slogoImg" value=$oViewConf->getViewThemeParam('sLogoFile')}]
                                    [{assign var="sLogoWidth" value=$oViewConf->getViewThemeParam('sLogoWidth')}]
                                    [{assign var="sLogoHeight" value=$oViewConf->getViewThemeParam('sLogoHeight')}]
                                    <a href="[{$oViewConf->getHomeLink()}]" title="[{$oxcmp_shop->oxshops__oxtitleprefix->value}]">
                                        <img loading="lazy" src="[{$oViewConf->getImageUrl($slogoImg)}]" alt="[{$oxcmp_shop->oxshops__oxtitleprefix->value}]" style="[{if $sLogoWidth}]width:auto;max-width:[{$sLogoWidth}]px;[{/if}][{if $sLogoHeight}]height:auto;max-height:[{$sLogoHeight}]px;[{/if}]">
                                    </a>
                                [{/block}]
                            </div>
                        </div>
                        <div class="hidden-xs hidden-sm col-md-5 search-col">
                            <div class="content">
                                [{include file="widget/header/search.tpl"}]
                            </div>
                        </div>
                        <div class="hidden-xs hidden-sm col-md-3 symbol-col">
                            [{include file="layout/right-buttons.tpl"}]
                        </div>
                    </div>
                </div>
            </div>
        </div>
        [{block name="layout_header_bottom"}]
        <div class="container-fluid">
            [{include file="widget/header/weltencategorylist.tpl"}]
        </div>
        [{/block}]
    </header>
[{/block}]

[{insert name="oxid_newbasketitem" tpl="widget/minibasket/newbasketitemmsg.tpl" type="message"}]
[{oxid_include_dynamic file="widget/minibasket/minibasketmodal.tpl"}]

