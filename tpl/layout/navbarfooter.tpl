<nav class="navbar navbar-default navbar-fixed-bottom hidden-lg hidden-md">
    <div class="container-fluid">
        <div class="row">

            [{*}]
            <div class="hidden-sm col-sm-4">
                <!-- Titel und Schalter werden für eine bessere mobile Ansicht zusammengefasst -->
                <div class="navbar-header">


                    <button type="button" class="navbar-toggle collapsed hamburger hamburger--collapse-r" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false" onclick="$(this).toggleClass('is-active')">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                    </button>

                        [{if $oxcmp_user->oxuser__oxpassword->value}]
                            [{assign var="fullname" value=$oxcmp_user->oxuser__oxfname->value|cat:" "|cat:$oxcmp_user->oxuser__oxlname->value }]
                            [{*oxmultilang ident="GREETING"}]
                            <a href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=account"}]"
                               class="navbar-brand">
                                [{if $fullname}]
                                    [{$fullname}]
                                [{else}]
                                    [{$oxcmp_user->oxuser__oxusername->value|oxtruncate:25:"...":true}]
                                [{/if}]
                            </a>
                        [{/if}]
                </div>
            </div>
            [{*}]

            <div class="col-xs-12">
                <!-- Alle Navigationslinks, Formulare und anderer Inhalt werden hier zusammengefasst und können dann ein- und ausgeblendet werden -->
                [{*}]<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">[{*}]

                    <div class="container-fluid">
                        <div class="row">
                            <ul class="nav navbar-nav display-flex-center" style="padding-right: 10px">

                                <li class="col-xs-10">
                                    [{* Suche *}]
                                    [{include file="widget/header/search_bottom.tpl"}]
                                </li>

                                <li class="col-xs-2" style="padding-right: 0;padding-left: 0">
                                    [{* Language Dropup *}]
                                    [{*include file="widget/header/languages_bottom.tpl"}]
                                    [{include file="widget/header/telefonnr-button.tpl"*}]

                                    [{* User-Profil Dropup }]
                                        <div class="btn-group dropup">
                                            <button type="button" class="btn dropdown-toggle bodyGrau" data-toggle="dropdown" style="background-color: white;border:none">
                                                [{if $oxcmp_user->oxuser__oxpassword->value}]
                                                    <i class="fal fa-user-check fa-2x"></i>
                                                [{else}]
                                                    <i class="fal fa-user-times fa-2x"></i>
                                                [{/if}]
                                            </button>
                                            <ul class="dropdown-menu" style="margin-left: -100px;margin-top:50px;background-color: white;padding:15px;border:1px solid #E7E7E7;position: absolute; " role="menu">
                                                [{if $oxcmp_user->oxuser__oxpassword->value}]
                                                    <li class="text-align-right">
                                                        [{assign var="fullname" value=$oxcmp_user->oxuser__oxfname->value|cat:" "|cat:$oxcmp_user->oxuser__oxlname->value }]
                                                        [{oxmultilang ident="GREETING"}]
                                                        [{if $fullname}]
                                                            [{$fullname}]
                                                        [{else}]
                                                            [{$oxcmp_user->oxuser__oxusername->value|oxtruncate:25:"...":true}]
                                                        [{/if}]
                                                    </li>
                                                [{/if}]
                                                <li class="">
                                                    [{include file="widget/header/loginbox.tpl"}]
                                                </li>
                                                [{block name="widget_header_servicebox_items"}]
                                                    [{if $oxcmp_user->oxuser__oxpassword->value}]
                                                        <li class="text-align-right">
                                                            <a href="[{oxgetseourl ident=$oViewConf->getSslSelfLink()|cat:"cl=account"}]">[{oxmultilang ident="MY_ACCOUNT"}]</a>
                                                        </li>
                                                        [{if $oViewConf->getShowCompareList()}]
                                                            <li class="text-align-right">
                                                                <a href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=compare"}]">[{oxmultilang ident="MY_PRODUCT_COMPARISON"}]</a> [{if $oView->getCompareItemsCnt()}]
                                                                    <span class="badge">[{$oView->getCompareItemsCnt()}]</span>[{/if}]
                                                            </li>
                                                        [{/if}]
                                                        <li class="text-align-right">
                                                            <a href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=account_noticelist"}]"><span>[{oxmultilang ident="MY_WISH_LIST"}]</span></a>
                                                            [{if $oxcmp_user && $oxcmp_user->getNoticeListArtCnt()}]
                                                                <span class="badge">[{$oxcmp_user->getNoticeListArtCnt()}]</span>[{/if}]
                                                        </li>
                                                        [{if $oViewConf->getShowWishlist()}]
                                                            <li class="text-align-right">
                                                                <a href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=account_wishlist"}]"><span>[{oxmultilang ident="MY_GIFT_REGISTRY"}]</span></a>
                                                                [{if $oxcmp_user && $oxcmp_user->getWishListArtCnt()}]
                                                                    <span class="badge">[{$oxcmp_user->getWishListArtCnt()}]</span>[{/if}]
                                                            </li>
                                                        [{/if}]
                                                        [{if $oViewConf->getShowListmania()}]
                                                            <li class="text-align-right">
                                                                <a href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=account_recommlist"}]"><span>[{oxmultilang ident="MY_LISTMANIA"}]</span></a>
                                                                [{if $oxcmp_user && $oxcmp_user->getRecommListsCount()}]
                                                                    <span class="badge">[{$oxcmp_user->getRecommListsCount()}]</span>[{/if}]
                                                            </li>
                                                        [{/if}]
                                                        [{if $oViewConf->isFunctionalityEnabled( "blEnableDownloads" )}]
                                                            <li class="text-align-right">
                                                                <a href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=account_downloads"}]"><span>[{oxmultilang ident="MY_DOWNLOADS"}]</span></a>
                                                            </li>
                                                        [{/if}]
                                                        [{* Logout }]
                                                        <li class="text-align-right">
                                                            <a href="[{$oViewConf->getLogoutLink()}]"
                                                               class="btn btn-danger" role="getLogoutLink" style="color:white">
                                                                <i class="fal fa-power-off"></i> [{oxmultilang ident="LOGOUT"}]
                                                            </a>
                                                        </li>
                                                    [{/if}]
                                                [{/block}]
                                            </ul>
                                        </div>
                                        *}]




                                    <div class="btn-group dropup clearfix">
                                        [{* Warenkorb *}]
                                        [{block name="dd_layout_page_header_icon_menu_minibasket"}]
                                            [{* Minibasket Dropdown *}]
                                            [{if $oxcmp_basket->getProductsCount()}]
                                                [{assign var="blAnon" value=0}]
                                                [{assign var="force_sid" value=$oViewConf->getSessionId()}]
                                            [{else}]
                                                [{assign var="blAnon" value=1}]
                                            [{/if}]

                                                <button type="button" class="btn bodyGrau dropdown-toggle" data-toggle="dropdown"
                                                        data-href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=basket"}]"
                                                        style="background-color: inherit;border: none">
                                                    [{block name="dd_layout_page_header_icon_menu_minibasket_button"}]
                                                        <i class="fal fa-shopping-cart fa-2x"></i>
                                                        [{if $oxcmp_basket->getItemsCount() > 0}]
                                                            [{$oxcmp_basket->getItemsCount()}]
                                                        [{else}]
                                                            [{*}]
																<i class="fa fa-angle-down"></i>
															[{*}]
                                                        [{/if}]
                                                    [{/block}]
                                                </button>
                                                <ul class="dropdown-menu dropdown-menu-right" role="menu" style="position: absolute;margin-left: -200px;background-color: white;padding:15px;border:1px solid #E7E7E7;">
                                                    [{block name="dd_layout_page_header_icon_menu_minibasket_list"}]

                                                        <li>

                                                            <div id="loginBox" class="loginBox">

                                                                        [{*oxid_include_dynamic file="widget/minibasket/minibasket.tpl"*}]
                                                                        [{if $oxcmp_basket->getProductsCount() gte 5}]
                                                                            [{assign var="blScrollable" value=true}]
                                                                        [{/if}]

                                                                        [{block name="widget_minibasket"}]
                                                                            [{if $oxcmp_basket->getProductsCount()}]
                                                                                [{oxhasrights ident="TOBASKET"}]
                                                                                [{assign var="currency" value=$oView->getActCurrency()}]

                                                                                [{if $_prefix == 'modal'}]
                                                                                    <div class="modal fade basketFlyout"
                                                                                         id="basketModal" tabindex="-1"
                                                                                         role="dialog"
                                                                                         aria-labelledby="basketModalLabel"
                                                                                         aria-hidden="true">
                                                                                        <div class="modal-dialog">
                                                                                            <div class="modal-content">
                                                                                                <div class="modal-header">
                                                                                                    <button type="button"
                                                                                                            class="close"
                                                                                                            data-dismiss="modal">
                                                                                                        <span aria-hidden="true">&times;</span>
                                                                                                        <span class="sr-only">[{oxmultilang ident="CLOSE"}]</span>
                                                                                                    </button>
                                                                                                    <h4 class="modal-title"
                                                                                                        id="basketModalLabel">[{$oxcmp_basket->getItemsCount()}] [{oxmultilang ident="ITEMS_IN_BASKET"}]</h4>
                                                                                                </div>
                                                                                                <div class="modal-body">
                                                                                                    [{if $oxcmp_basket->getProductsCount()}]
                                                                                                        [{oxhasrights ident="TOBASKET"}]
                                                                                                            <div id="[{$_prefix}]basketFlyout"
                                                                                                                 class="basketFlyout">
                                                                                                                <div class="">
                                                                                                                    <table class="table table-striped">
                                                                                                                        <thead>
                                                                                                                        <tr>
                                                                                                                            <th>[{oxmultilang ident="DD_MINIBASKET_MODAL_TABLE_TITLE"}]</th>
                                                                                                                            <th class="text-right">[{oxmultilang ident="DD_MINIBASKET_MODAL_TABLE_PRICE"}]</th>
                                                                                                                        </tr>
                                                                                                                        </thead>
                                                                                                                        <tbody>
                                                                                                                        [{foreach from=$oxcmp_basket->getContents() name=miniBasketList item=_product}]
                                                                                                                            [{block name="widget_minibasket_product"}]
                                                                                                                                [{assign var="minibasketItemTitle" value=$_product->getTitle()}]
                                                                                                                                <tr>
                                                                                                                                    <td>
                                                                                                                                        <a href="[{$_product->getLink()}]"
                                                                                                                                           title="[{$minibasketItemTitle|strip_tags}]">
                                                                                                                                            <span class="item">
                                                                                                                                                [{if $_product->getAmount() > 1}]
                                                                                                                                                    [{$_product->getAmount()}] &times;
                                                                                                                                                [{/if}]
                                                                                                                                                [{$minibasketItemTitle|strip_tags}]
                                                                                                                                            </span>
                                                                                                                                        </a>
                                                                                                                                    </td>
                                                                                                                                    <td class="text-right">
                                                                                                                                        <strong class="price">[{oxprice price=$_product->getPrice() currency=$currency}]
                                                                                                                                            *</strong>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            [{/block}]
                                                                                                                        [{/foreach}]
                                                                                                                        </tbody>
                                                                                                                        <tfoot>
                                                                                                                        <tr>
                                                                                                                            [{block name="widget_minibasket_total"}]
                                                                                                                                <td>
                                                                                                                                    <strong>[{oxmultilang ident="TOTAL"}]</strong>
                                                                                                                                </td>
                                                                                                                                <td class="text-right">
                                                                                                                                    <strong class="price">
                                                                                                                                        [{if $oxcmp_basket->isPriceViewModeNetto()}]
                                                                                                                                            [{$oxcmp_basket->getProductsNetPrice()}]
                                                                                                                                        [{else}]
                                                                                                                                            [{$oxcmp_basket->getFProductsPrice()}]
                                                                                                                                        [{/if}]
                                                                                                                                        [{$currency->sign}]
                                                                                                                                        *
                                                                                                                                    </strong>
                                                                                                                                </td>
                                                                                                                            [{/block}]
                                                                                                                        </tr>
                                                                                                                        </tfoot>
                                                                                                                    </table>
                                                                                                                </div>
                                                                                                                [{include file="widget/minibasket/countdown.tpl"}]
                                                                                                            </div>
                                                                                                        [{/oxhasrights}]
                                                                                                    [{/if}]
                                                                                                </div>
                                                                                                <div class="modal-footer">
                                                                                                    <button type="button"
                                                                                                            class="btn btn-default"
                                                                                                            data-dismiss="modal">[{oxmultilang ident="DD_MINIBASKET_CONTINUE_SHOPPING"}]</button>
                                                                                                    <a href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=basket"}]"
                                                                                                       class="btn btn-primary"
                                                                                                       data-toggle="tooltip"
                                                                                                       data-placement="top"
                                                                                                       title="[{oxmultilang ident="DISPLAY_BASKET"}]">
                                                                                                        <i class="fa fa-shopping-cart"></i>
                                                                                                    </a>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    [{oxscript add="$('#basketModal').modal('show');"}]
                                                                                [{else}]
                                                                                    [{block name="dd_layout_page_header_icon_menu_minibasket_title"}]
                                                                                        <p class="title">
                                                                                            <strong>[{$oxcmp_basket->getItemsCount()}] [{oxmultilang ident="ITEMS_IN_BASKET"}]</strong>
                                                                                        </p>
                                                                                    [{/block}]
                                                                                    <div id="[{$_prefix}]basketFlyout"
                                                                                         class="basketFlyout[{if $blScrollable}] scrollable[{/if}]">
                                                                                        [{block name="dd_layout_page_header_icon_menu_minibasket_table"}]
                                                                                            <table class="table table-bordered table-striped table-mini-basket">
                                                                                                [{foreach from=$oxcmp_basket->getContents() name=miniBasketList item=_product}]
                                                                                                    [{block name="widget_minibasket_product"}]
                                                                                                        [{assign var="minibasketItemTitle" value=$_product->getTitle()}]
                                                                                                        <tr>
                                                                                                            <td class="picture text-center">
                                                                                                                <span class="badge">[{$_product->getAmount()}]</span>
                                                                                                                <a href="[{$_product->getLink()}]"
                                                                                                                   title="[{$minibasketItemTitle|strip_tags}]">
                                                                                                                    <img loading="lazy" src="[{$_product->getIconUrl()}]"
                                                                                                                         alt="[{$minibasketItemTitle|strip_tags}]"/>
                                                                                                                </a>
                                                                                                            </td>
                                                                                                            <td class="title">
                                                                                                                <a href="[{$_product->getLink()}]"
                                                                                                                   title="[{$minibasketItemTitle|strip_tags}]">[{$minibasketItemTitle|strip_tags}]</a>
                                                                                                            </td>
                                                                                                            <td class="price text-right">[{$_product->getFTotalPrice()}]
                                                                                                                &nbsp;[{$currency->sign}]</td>
                                                                                                        </tr>
                                                                                                    [{/block}]
                                                                                                [{/foreach}]
                                                                                                <tr>
                                                                                                    <td class="total_label"
                                                                                                        colspan="2">
                                                                                                        <strong>[{oxmultilang ident="TOTAL"}]</strong>
                                                                                                    </td>
                                                                                                    <td class="total_price text-right">
                                                                                                        <strong>
                                                                                                            [{if $oxcmp_basket->isPriceViewModeNetto()}]
                                                                                                                [{$oxcmp_basket->getProductsNetPrice()}]
                                                                                                            [{else}]
                                                                                                                [{$oxcmp_basket->getFProductsPrice()}]
                                                                                                            [{/if}]
                                                                                                            &nbsp;[{$currency->sign}]
                                                                                                        </strong>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                            <div class="clearfix">
                                                                                                [{block name="widget_minibasket_total"}][{/block}]
                                                                                            </div>
                                                                                        [{/block}]
                                                                                    </div>
                                                                                    [{include file="widget/minibasket/countdown.tpl"}]

                                                                                    [{block name="dd_layout_page_header_icon_menu_minibasket_functions"}]
                                                                                        <p class="functions clear text-right">
                                                                                            [{if $oxcmp_user}]
                                                                                                <a href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=payment"}]"
                                                                                                   class="btn btn-primary">[{oxmultilang ident="CHECKOUT"}]</a>
                                                                                            [{else}]
                                                                                                <a href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=user"}]"
                                                                                                   class="btn btn-primary">[{oxmultilang ident="CHECKOUT"}]</a>
                                                                                            [{/if}]
                                                                                            <a href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=basket"}]"
                                                                                               class="btn btn-default">[{oxmultilang ident="DISPLAY_BASKET"}]</a>
                                                                                        </p>
                                                                                    [{/block}]
                                                                                [{/if}]
                                                                                [{/oxhasrights}]
                                                                            [{else}]
                                                                                [{block name="dd_layout_page_header_icon_menu_minibasket_alert_empty"}]
                                                                                    <div class="alert alert-info">[{oxmultilang ident="BASKET_EMPTY"}]</div>
                                                                                [{/block}]
                                                                            [{/if}]
                                                                        [{/block}]

                                                            </div>

                                                    [{/block}]
                                                </ul>
                                            </div>
                                            [{*oxid_include_widget cl="oxwMiniBasket" nocookie=$blAnon force_sid=$force_sid*}]
                                        [{/block}]
                                    </div>
                                </li>

                            </ul>
                        </div>
                    </div>
                [{*}]</div><!-- /.navbar-collapse -->[{*}]
            </div>
        </div>
    </div><!-- /.container-fluid -->
</nav>