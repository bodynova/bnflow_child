[{capture append="oxidBlock_pageBody"}]
<div class="main-row">
    [{if $oView->showRDFa()}]
        [{include file="rdfa/rdfa.tpl"}]
    [{/if}]

    [{block name="layout_header"}]
        [{include file="layout/header.tpl"}]
    [{/block}]

    [{assign var="blFullwidth" value=$oViewConf->getViewThemeParam('blFullwidthLayout')}]



    [{*if $oView->getClassName()=='start' && $oView->getBanners()|@count > 0}]
        [{include file="widget/promoslider.tpl"}]
    [{/if*}]
    [{* Login Errors *}]
    [{$smarty.capture.loginErrors}]


    [{if $oView->getClassName()=='start' && $oView->getBanners()|@count > 0}]
        [{include file="widget/owlslider.tpl"}]
    [{/if}]

<div id="my-content">

    <div id="left" class="column hidden-xs">
        [{if $sidebar && $sidebar != "Right"}]
            [{include file="layout/sidebar_responsive.tpl"}]
        [{/if}]
    </div>

    <div id="right" class="column" style="flex-grow: 1;width: 100%">
        <div id="wrapper" [{*if $sidebar}]class="sidebar[{$sidebar}]"[{/if*}]>
            [{*}]
            <div id="sidebar-wrapper">
                [{include file="layout/debugbarLeft.tpl"}]
            </div>
            [{*}]
            <div id="page-content-wrapper" class="[{if $blFullwidth}]container-fluid[{else}]container[{/if}]">


                [{*}]<div class="container-fluid">[{*}]

                [{*include file="layout/right-buttons.tpl"*}]

                <div class="underdog">

                    [{*
					<div class="row">
						[{if $oView->getClassName()=='start' && $oView->getBanners()|@count > 0}]
							[{include file="widget/promoslider.tpl"}]
						[{/if}]
						[{*include file="widget/promoslider.tpl"
					</div>
					*}]

                    <div class="content-box">


                        [{if $oView->getClassName() != "start" && !$blHideBreadcrumb}]
                            [{block name="layout_breadcrumb"}]
                                [{include file="widget/breadcrumb.tpl"}]
                            [{/block}]
                        [{/if}]



                        <div class="row">
                            [{*if $sidebar && $sidebar != "Right"}]
								<div class="hidden-xs hidden-sm col-md-4 col-lg-2 [{$oView->getClassName()}]">
									<div id="sidebar">
										[{include file="layout/sidebar.tpl"}]
									</div>
								</div>
							[{/if*}]

                            <div class="col-xs-12 col-sm-12[{*if $sidebar}] col-md-8 col-lg-10[{/if*}]">
                                <div id="content">
                                    [{block name="content_main"}]
                                        [{include file="message/errors.tpl"}]

                                        [{foreach from=$oxidBlock_content item="_block"}]
                                            [{$_block}]
                                        [{/foreach}]
                                    [{/block}]
                                </div>

                            </div>

                            [{*if $sidebar && $sidebar == "Right"}]
								<div class="hidden-xs col-md-4 col-lg-3 [{$oView->getClassName()}]">
									<div id="sidebar">
										[{include file="layout/sidebar.tpl"}]
									</div>
								</div>
							[{/if*}]
                        </div>


                    </div>


                </div>
                [{include file="layout/navbarfooter.tpl"}]
                [{*}]</div>[{*}]

            </div>
        </div>
    </div>
</div>


    [{include file="layout/footer.tpl"}]

    [{block name="layout_init_social"}]
    [{/block}]

    <i class="fa fa-arrow-up fa-4x" id="jumptotop"></i>
</div>
[{/capture}]
[{include file="layout/base.tpl"}]
