<ul class="nav navbar-nav navbar-right">
	<li>
		<div class="my-fixed-right btn-group pull-right" id="right-buttons">

            [{* Languages *}]
            [{block name="dd_layout_page_header_icon_menu_languages"}]
                [{* Language Dropdown*}]
                [{oxid_include_widget cl="oxwLanguageList" lang=$oViewConf->getActLanguageId() _parent=$oView->getClassName() nocookie=1 _navurlparams=$oViewConf->getNavUrlParams() anid=$oViewConf->getActArticleId() nocache=true}]
            [{/block}]

            [{* Profil *}]
            [{block name="dd_layout_page_header_icon_menu_account"}]
                [{if $oxcmp_user || $oView->getCompareItemCount() || $Errors.loginBoxErrors}]
                    [{assign var="blAnon" value=0}]
                    [{assign var="force_sid" value=$oViewConf->getSessionId()}]
                [{else}]
                    [{assign var="blAnon" value=1}]
                [{/if}]
                [{* Account Dropdown *}]
                [{oxid_include_widget cl="oxwServiceMenu" _parent=$oView->getClassName() force_sid=$force_sid nocookie=$blAnon _navurlparams=$oViewConf->getNavUrlParams() anid=$oViewConf->getActArticleId()}]
            [{/block}]

            [{* Telefonnummer *}]
			<div class="btn-group tel-button">
				<button type="button" class="btn dropdown-toggle" data-toggle="dropdown" style="border: none">
					<i class="fal fa-phone fa-2x"></i>
				</button>
				<ul class="dropdown-menu dropdown-menu-right left-auto" role="menu">
					<li>
						<address>
							<a href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=contact"}]"
							   title="[{oxmultilang ident="WIDGET_SERVICES_CONTACTS"}]">
								<strong>[{oxmultilang ident="OEFFNUNGSZEITEN"}]</strong><br>
							</a>
							<abbr title="Telefonnummer">tel:</abbr>[{oxmultilang ident="TELEFONNUMMER"}]
						</address>
					</li>
				</ul>
			</div>

            [{* Warenkorb *}]
            [{block name="dd_layout_page_header_icon_menu_minibasket"}]
                [{* Minibasket Dropdown *}]
                [{if $oxcmp_basket->getProductsCount()}]
                    [{assign var="blAnon" value=0}]
                    [{assign var="force_sid" value=$oViewConf->getSessionId()}]
                [{else}]
                    [{assign var="blAnon" value=1}]
                [{/if}]
                [{oxid_include_widget cl="oxwMiniBasket" nocookie=$blAnon force_sid=$force_sid}]
            [{/block}]
		</div>
	</li>
</ul>



