[{oxifcontent ident="ticker" object="oCont"}]
	<div class="container-fluid">
		<div class="row top-head" style="background-color: #5098aa;">
			<div class="container-fluid">
				<div class="col-xs-12">
					<div class="text-center" style="color:white;">
                        [{$oCont->oxcontents__oxcontent->value}]
					</div>
				</div>
			</div>
		</div>
	</div>
[{/oxifcontent}]
<div class="container-fluid">
	<div class="row top-head">

		[{block name="dre_freeShippingTopRowLeft"}]
		[{/block}]

		<div class="container-fluid">
			<div class="col-xs-12 display-flex-center">
				[{block name="dre_freeShippingTopRowMiddle"}]
				[{/block}]
			</div>
		</div>

		[{block name="dre_freeShippingTopRowRight"}]
		[{/block}]

	</div>
</div>