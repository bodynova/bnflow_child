[{capture append="oxidBlock_content"}]
    [{assign var="template_title" value=""}]

    [{* ordering steps *}]
    [{include file="page/checkout/inc/steps.tpl" active=2}]

    [{block name="checkout_user_main"}]
        [{if !$oxcmp_user && !$oView->getLoginOption()}]
            [{include file="page/checkout/inc/options.tpl"}]
        [{/if}]

        [{block name="checkout_user_noregistration"}]
            [{if !$oxcmp_user && $oView->getLoginOption() == 1}]
                [{include file="form/user_checkout_noregistration.tpl"}]
            [{/if}]
        [{/block}]

        [{block name="checkout_user_registration"}]
            [{if !$oxcmp_user && $oView->getLoginOption() == 3}]
                [{include file="form/user_checkout_registration.tpl"}]
            [{/if}]
        [{/block}]

        [{block name="checkout_user_change"}]
            [{if $oxcmp_user}]
                [{assign var='haltcheckout' value=false}]
                [{if $oxcmp_basket->findDeliveryCountry() eq 'a7c40f632a0804ab5.18804076'}]
                    [{if $oxcmp_basket->getNettoSum() < 160.00}]
                        [{assign var='haltcheckout' value=true}]
						<p class="btn-danger" style="padding: 15px">
                            [{oxmultilang ident="ORDERGBINFO"}]
							<a href="[{oxgetseourl ident=$oViewConf->getBasketLink()}]"
							   class="btn btn-default prevStep submitButton largeButton" id="userBackStepTop"><i
										class="fal fa-shopping-cart"></i></a>
						</p>
                    [{/if}]
                [{/if}]
                [{include file="form/user_checkout_change.tpl"}]
            [{/if}]
        [{/block}]
    [{/block}]
    [{insert name="oxid_tracker" title=$template_title}]
[{/capture}]

[{include file="layout/page.tpl"}]