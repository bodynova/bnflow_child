[{capture append="oxidBlock_content"}]
    <div class="row">
        <div class="col-xs-12">
            [{if $oView->getNewsletterStatus() == 4 || !$oView->getNewsletterStatus()}]
                <h1 class="page-header">[{oxmultilang ident="STAY_INFORMED"}]</h1>
                <div class="row">
                    <div class="col-lg-12">
                        <article>
                            [{oxifcontent ident="oxnewstlerinfo" object="oCont"}]
                                [{$oCont->oxcontents__oxcontent->value}]
                            [{/oxifcontent}]
                        </article>
                    </div>
                </div>

                [{include file="form/newsletter.tpl"}]
            [{elseif $oView->getNewsletterStatus() == 1}]
                <h1 class="page-header">[{oxmultilang ident="MESSAGE_THANKYOU_FOR_SUBSCRIBING_NEWSLETTERS"}]</h1>
                <article>
                    <p>[{oxmultilang ident="MESSAGE_SENT_CONFIRMATION_EMAIL"}]</p>
                </article>

            [{elseif $oView->getNewsletterStatus() == 2}]
                <h1 class="page-header">[{oxmultilang ident="MESSAGE_NEWSLETTER_CONGRATULATIONS"}]</h1>
                [{*}]
                <article>
                    <p>[{oxmultilang ident="MESSAGE_NEWSLETTER_SUBSCRIPTION_ACTIVATED"}]</p>
                </article>
                [{*}]

                [{oxifcontent ident="newslettergs" object="oCont"}]
                    [{$oCont->oxcontents__oxcontent->value}]
                [{/oxifcontent}]
                <div class="col-lg-10 col-lg-offset-1" style="padding-left: 5px">
                    <strong><a href="mailto:[{$oView->getUserEmail()}]?subject=[{oxmultilang ident="Newslettercodebetreff"}]&body=[{oxmultilang ident="Newslettercodebody"}]">[{oxmultilang ident="Newslettercode"}]</a></strong>
                </div>
            [{elseif $oView->getNewsletterStatus() == 3}]
                <h1 class="page-header">[{oxmultilang ident="SUCCESS"}]</h1>
                <article>
                    <p>[{oxmultilang ident="MESSAGE_NEWSLETTER_SUBSCRIPTION_CANCELED"}]</p>
                </article>
            [{/if}]
        </div>
    </div>
    [{insert name="oxid_tracker"}]
[{/capture}]

[{include file="layout/page.tpl"}]