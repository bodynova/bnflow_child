[{capture append="oxidBlock_content"}]
    [{assign var="oConfig" value=$oViewConf->getConfig()}]
    [{assign var='rsslinks' value=$oView->getRssLinks()}]
    [{*oxscript include="js/pages/start.min.js"*}]
	<div class="col-sm-12 col-xs-12 col-md-offset-1 col-md-10 col-lg-offset-1 col-lg-10">
		<h1>[{oxmultilang ident="STARTSEITEH1"}]</h1>
	</div>
    [{* Visual CMS Bereich 1 *}]
    [{block name="start_welcome_text"}]
        [{oxifcontent ident="oxstartwelcome" object="oCont"}]
			<div class="welcome-teaser">
                [{$oCont->oxcontents__oxcontent->value}]
			</div>
        [{/oxifcontent}]
    [{/block}]


    [{* Markenslider deaktiviert dafür beim Footer hinzugefügt *}]
    [{*block name="start_manufacturer_slider"}]
		[{if $oViewConf->getViewThemeParam('bl_showManufacturerSlider')}]
			[{include file="widget/manufacturersslider.tpl"}]
		[{/if}]
	[{/block*}]

    [{* Debug auf Startseite unter Banner *}]
    [{*$oViewConf->getActLanguageAbbr()|var_dump*}]
	<div class="col-sm-12 col-xs-12 col-md-offset-1 col-md-10 col-lg-offset-1 col-lg-10">

        [{block name="start_newest_articles"}]
            [{assign var="oNewestArticles" value=$oView->getNewestArticles()}]
            [{if $oNewestArticles && $oNewestArticles->count()}]
                [{* type=$oViewConf->getViewThemeParam('sStartPageListDisplayType') *}]
                [{include file="widget/product/list.tpl" type="grid"  head="START_NEWEST_HEADER"|oxmultilangassign subhead="START_NEWEST_SUBHEADER"|oxmultilangassign listId="newItems" products=$oNewestArticles rsslink=$rsslinks.newestArticles rssId="rssNewestProducts" showMainLink=true iProductsPerLine=6}]
            [{/if}]
        [{/block}]
	</div>
    [{assign var="oTopArticles" value=$oView->getTop5ArticleList()}]

    [{*
		Aktion Angebote der Woche
	*}]
    [{* oxactions__oxid->value == "oxbargain" *}]
    [{block name="start_bargain_articles"}]
        [{assign var="oBargainArticles" value=$oView->getBargainArticleList()}]
        [{*$oView->getBargainArticleList()|var_dump*}]
        [{if $oBargainArticles && $oBargainArticles->count()}]
            [{* type=$oViewConf->getViewThemeParam('sStartPageListDisplayType') *}]
            [{include file="widget/product/list.tpl" type="grid" head="START_BARGAIN_HEADER"|oxmultilangassign subhead="START_BARGAIN_SUBHEADER"|oxmultilangassign listId="bargainItems" products=$oBargainArticles rsslink=$rsslinks.bargainArticles rssId="rssBargainProducts" showMainLink=true iProductsPerLine=6}]
        [{/if}]
    [{/block}]




    [{*
		Aktion Frisch eingetroffen ( kann auch automatisch über Stammdaten->performance->Liste neueste Artikel
		automatisch/manuell/ausgeschaltet eingestellt werden )
	*}]
    [{* oxactions__oxid->value == "oxnewest" *}]


    [{* trenner zwischen (Angebote der Woche & Frisch Eingetroffen) UND Topseller *}]
    [{if $oNewestArticles && $oNewestArticles->count() && $oTopArticles && $oTopArticles->count()}]
		<div class="row">
			<hr>
		</div>
    [{/if}]

    [{*
		Aktion Topseller ( kann auch automatisch über Stammdaten->performance->Liste meistverkaufte Artikel
		automatisch/manuell/ausgeschaltet eingestellt werden )
	*}]
    [{* oxactions__oxid->value == "oxtop5" *}]

    [{block name="start_top_articles"}]
        [{if $oTopArticles && $oTopArticles->count()}]
            [{include file="widget/product/list.tpl" type="grid" head="START_TOP_PRODUCTS_HEADER"|oxmultilangassign subhead="START_TOP_PRODUCTS_SUBHEADER"|oxmultilangassign:$oTopArticles->count() listId="topBox" products=$oTopArticles rsslink=$rsslinks.topArticles rssId="rssTopProducts" showMainLink=true iProductsPerLine=2}]
        [{/if}]
    [{/block}]




    [{* Visual CMS Bereich *}]
    [{oxifcontent ident="startbottom" object="oCont"}]
		<div class="welcome-teaser">
            [{$oCont->oxcontents__oxcontent->value}]
		</div>
    [{/oxifcontent}]



    [{insert name="oxid_tracker"}]
[{/capture}]
[{include file="layout/page.tpl"}]
