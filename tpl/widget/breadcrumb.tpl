[{block name="dd_widget_breadcrumb"}]
    [{strip}]
        <div class="row">
            <div class="col-xs-12">
                [{block name="dd_widget_breadcrumb_list_inner"}]
                    [{assign var="slogoImg" value=$oViewConf->getViewThemeParam('sLogoFile')}]
                    <ol id="breadcrumb" class="breadcrumb">

                        [{block name="dd_widget_breadcrumb_list"}]
                            [{*}]<li class="text-muted">[{oxmultilang ident="YOU_ARE_HERE"}]:</li>[{*}]

                            <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                                <a href="[{$oViewConf->getHomeLink()}]" title="[{$oxcmp_shop->oxshops__oxtitleprefix->value}]">
                                    <img src="[{$oViewConf->getImageUrl($slogoImg)}]" alt="[{$oxcmp_shop->oxshops__oxtitleprefix->value}]" style="width:auto;max-width:20px;height:auto;max-height:20px;">
                                </a>
                            </li>
                            [{* aktuelle Kategorie zuweisen *}]
                            [{assign var="oCat" value=$oxcmp_categories->getClickCat()}]
                            [{*$oCat|var_dump*}]

                            [{if $oCat !== null}]
                            <li>
                                <a href="[{oxgetseourl ident=$oxcmp_categories->dre_getWeltString($oCat->oxcategories__welt->value) type="oxcontent"}]">
                                [{oxifcontent ident=$oxcmp_categories->dre_getWeltString($oCat->oxcategories__welt->value) object="oCont"}]
                                    [{$oCont->oxcontents__oxtitle->value}]
                                    [{*$oCont->oxcontents__oxcontent->value*}]
                                    [{*$oCont|var_dump*}]
                                [{/oxifcontent}]
                                </a>
                            </li>
                            [{/if}]

                            [{foreach from=$oView->getBreadCrumb() item="sCrum" name="breadcrumb"}]
                                <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"[{if $smarty.foreach.breadcrumb.last}] class="active"[{/if}]>
                                    <a href="[{if $sCrum.link}][{$sCrum.link}][{else}]#[{/if}]" title="[{$sCrum.title|escape:'html'}]" itemprop="url">
                                        <span itemprop="title">[{$sCrum.title}]</span>
                                    </a>
                                </li>
                            [{/foreach}]
                        [{/block}]
                    </ol>

                [{/block}]
            </div>
        </div>
    [{/strip}]
[{/block}]
