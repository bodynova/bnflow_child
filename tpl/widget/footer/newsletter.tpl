[{block name="dd_footer_newsletter_form"}]
    [{*<form class="form-inline" role="form" action="[{$oViewConf->getSslSelfLink()}]" method="post" style="white-space: nowrap; margin-top:-5px;">*}]
    <form class="form-inline" role="form" action="[{oxgetseourl ident='oxnewstlerinfo' type='oxcontent'}]" method="post" style="white-space: nowrap; margin-top:-5px;">
        [{block name="dd_footer_newsletter_form_inner"}]
            <div class="hidden">
                [{$oViewConf->getHiddenSid()}]
                <input type="hidden" name="fnc" value="fill">
                <input type="hidden" name="cl" value="newsletter">
                [{if $oView->getProduct()}]
                    [{assign var="product" value=$oView->getProduct()}]
                    <input type="hidden" name="anid" value="[{$product->oxarticles__oxnid->value}]">
                [{/if}]
            </div>

            [{block name="dd_footer_newsletter_form_inner_group"}]
                <label class="sr-only" for="footer_newsletter_oxusername">[{oxmultilang ident="NEWSLETTER"}]</label>
                <input style="display:unset" class="form-control" type="email" name="editval[oxuser__oxusername]" id="footer_newsletter_oxusername" value="" placeholder="[{oxmultilang ident="EMAIL"}]">
                <button class="btn btn-primary newsletterbutton" type="submit">[{oxmultilang ident="SUBSCRIBE"}]</button>
            [{/block}]
        [{/block}]
    [{*oxifcontent ident="socialfoot" object="_cont"}]
        [{$_cont->oxcontents__oxcontent->value}]
    [{/oxifcontent*}]
    </form>
[{/block}]