[{*block name="dd_widget_header_categorylist"*}]
[{*$welt|var_dump*}]

[{if $oxcmp_categories}]
    [{assign var="homeSelected" value="false"}]
    [{if $oViewConf->getTopActionClassName() == 'start'}]
        [{assign var="homeSelected" value="true"}]
    [{/if}]
    [{assign var="oxcmp_categories" value=$oxcmp_categories}]
    [{assign var="blFullwidth" value=$oViewConf->getViewThemeParam('blFullwidthLayout')}]
    [{*assign var="welt" value=$oxcmp_categories->getWelt()*}]

    [{*if $oxcmp_categories->getWelt() == 1 || $oxcmp_categories->getWelt() == 2}]
		[{assign var="left" value=1}]
	[{else}]
		[{assign var="left" value=0}]
	[{/if*}]


    [{* Dropdown Inhalt *}]
    [{*block name="dd_widget_header_categorylist_navbar"*}]
    [{*block name="dd_widget_header_categorylist_navbar_list"*}]
    [{foreach from=$oxcmp_categories item="ocat" key="catkey" name="root"}]
        [{if $ocat->getIsVisible()}]
            [{foreach from=$ocat->getContentCats() item="oTopCont" name="MoreTopCms"}]
				<li>
					<a href="[{$oTopCont->getLink()}]">
                        [{$oTopCont->oxcontents__oxtitle->value|html_entity_decode}]
					</a>
				</li>
            [{/foreach}]
			<li class="[{if $ocat->getSubCats()}]has-sub[{/if}]"
			    style="[{if !$ocat->getSubCats() && $left}]padding-left:10px;[{/if}]">
				<div class="btn-group btn-block">
                    [{if $left}]
						<a class="btn hidden-xs hidden-sm" style="padding-top: 10px">
                            [{if $ocat->getSubCats()}]
								<span class="fal fa-chevron-left pull-left"></span>
                            [{/if}]
						</a>
                    [{/if}]
					<a class="btn" href="[{$ocat->getLink()}]">
                        [{$ocat->oxcategories__oxtitle->value|html_entity_decode}]
					</a>
                    [{if !$left}]
                        [{if $ocat->getSubCats()}]
							<a class="btn hidden-xs hidden-sm pull-right" style="padding-top: 10px">
								<span class="fal fa-chevron-right pull-right"></span>
							</a>
                        [{/if}]
                    [{/if}]
				</div>
				<ul>
                    [{foreach from=$ocat->getSubCats() item="osubcat" key="subcatkey" name="SubCat"}]
                        [{if $osubcat->getIsVisible()}]
                            [{* CMS - Kategorien Schleife *}]
                            [{foreach from=$osubcat->getContentCats() item=ocont name=MoreCms}]
								<li>
									<a href="[{$ocont->getLink()}]">
                                        [{$ocont->oxcontents__oxtitle->value|html_entity_decode}]
									</a>
								</li>
                            [{/foreach}]
                        [{/if}]
						<li class="[{if $osubcat->getSubCats()}]has-sub[{/if}]">
							<div class="btn-group btn-block">
                                [{if $left}]
									<a class="btn hidden-xs hidden-sm" style="padding-top: 10px">
                                        [{if $osubcat->getSubCats()}]
											<span class="fal fa-chevron-left pull-left"></span>
                                        [{/if}]
									</a>
                                [{/if}]
								<a class="btn" href="[{$osubcat->getLink()}]">
                                    [{$osubcat->oxcategories__oxtitle->value|html_entity_decode}]
								</a>
                                [{if !$left}]
                                    [{if $osubcat->getSubCats()}]
										<a class="btn hidden-xs hidden-sm pull-right" style="padding-top: 10px">
											<span class="fal fa-chevron-right pull-right"></span>
										</a>
                                    [{/if}]
                                [{/if}]
							</div>

                            [{if $osubcat->getSubCats()}]
								<ul>
                                    [{foreach from=$osubcat->getSubCats() item="oosubcat" key="subcatkey" name="SubCat"}]
                                        [{if $oosubcat->getIsVisible()}]
                                            [{* CMS - Kategorien Schleife *}]
                                            [{foreach from=$oosubcat->getContentCats() item=subocont name=MoreCms}]
												<li>
													<a href="[{$subocont->getLink()}]">[{$subocont->oxcontents__oxtitle->value|html_entity_decode}]</a>
												</li>
                                            [{/foreach}]
                                        [{/if}]
                                        [{if $oosubcat->getIsVisible()}]
											<li class="[{if $oosubcat->getSubCats()}]has-sub[{/if}]">
												<div class="btn-group btn-block">
                                                    [{if $left}]
														<a class="btn hidden-xs hidden-sm" style="padding-top: 10px">
                                                            [{if $oosubcat->getSubCats()}]
																<span class="fal fa-chevron-left pull-left"></span>
                                                            [{/if}]
														</a>
                                                    [{/if}]
													<a class="btn" href="[{$oosubcat->getLink()}]">
                                                        [{$oosubcat->oxcategories__oxtitle->value|html_entity_decode}]
													</a>
                                                    [{if !$left}]
                                                        [{if $oosubcat->getSubCats()}]
															<a class="btn hidden-xs hidden-sm pull-right"
															   style="padding-top: 10px">
																<span class="fal fa-chevron-right pull-right"></span>
															</a>
                                                        [{/if}]
                                                    [{/if}]
												</div>
                                                [{if $oosubcat->getSubCats()}]
													<ul>
                                                        [{foreach from=$oosubcat->getSubCats() item="moosubcat" key="subcatkey" name="SubCat"}]
                                                            [{if $moosubcat->getIsVisible()}]
                                                                [{* CMS - Kategorien Schleife *}]
                                                                [{foreach from=$moosubcat->getContentCats() item=subocont name=MoreCms}]
																	<li>
																		<a class="btn btn-block"
																		   href="[{$subocont->getLink()}]">
                                                                            [{$subocont->oxcontents__oxtitle->value|html_entity_decode}]
																		</a>
																	</li>
                                                                [{/foreach}]
																<li>
																	<a class="btn btn-block"
																	   href="[{$moosubcat->getLink()}]">[{*$moosubcat->getSubCats()|@count*}][{$moosubcat->oxcategories__oxtitle->value|html_entity_decode}]</a>
																</li>
                                                            [{/if}]
                                                        [{/foreach}]
													</ul>
                                                [{/if}]
											</li>
                                        [{/if}]
                                    [{/foreach}]
								</ul>
                            [{/if}]
						</li>
                    [{/foreach}]
				</ul>
			</li>
        [{/if}]
    [{/foreach}]
    [{*/block*}]
    [{*/block*}]
[{/if}]
[{*/block*}]

