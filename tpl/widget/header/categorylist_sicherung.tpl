[{block name="dd_widget_header_categorylist"}]
	[{if $oxcmp_categories}]
		[{assign var="homeSelected" value="false"}]
		[{if $oViewConf->getTopActionClassName() == 'start'}]
			[{assign var="homeSelected" value="true"}]
		[{/if}]
		[{assign var="oxcmp_categories" value=$oxcmp_categories}]
		[{assign var="blFullwidth" value=$oViewConf->getViewThemeParam('blFullwidthLayout')}]
		[{*assign var="welt" value=$welt*}]


		[{*$welt|var_dump*}]

		[{*}]
		<hr>
		[{foreach from=$oxcmp_categories item="ocat" key="catkey" name="root"}]
			[{$ocat->oxcategories__welt->value|var_dump}]
		[{/foreach*}]


		[{*oxscript add="
			$(document).ready(function(){
				$.adaptiveBackground.run({
						success: function( data) {
							console.log('Success!', data);
							console.log('fb', data.color);
						}
				});
			});
		"*}]
		<div id="mainnav" class="navbar yamm navbar-default [{*}]navbar-default[{*}]" role="navigation">
			<!--class="navbar navbar-default"-->
			[{*}]<div class="[{if !$blFullwidth}]container[{else}]container-fluid[{/if}]">[{*}]
			[{block name="dd_widget_header_categorylist_navbar"}]
				<div class="navbar-header">
					<!-- Navbar ausgeblendet Button zum einblenden mini-Display -->
					[{block name="dd_widget_header_categorylist_navbar_header"}]
						<button type="button" class="navbar-toggle" data-toggle="collapse"
						        data-target=".navbar-ex1-collapse">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<span class="visible-xs-inline">[{oxmultilang ident="DD_ROLES_BEMAIN_UIROOTHEADER"}]</span>
					[{/block}]
					<!-- /Navbar ausgeblendet Button zum einblenden mini-Display -->
					<!-- Moving Head  # Bodynova Logo Mini # -->
					<!--
					<a class="btn btn-secondary" id="debugbarLeftCollapse">
						<i class="fa fa-search"></i>
					</a>
					-->
					[{block name="layout_header_logo"}]
					[{assign var="slogoImg" value=$oViewConf->getViewThemeParam('sLogoFile')}]
					[{assign var="sLogoWidth" value=$oViewConf->getViewThemeParam('sLogoWidth')}]
					[{assign var="sLogoHeight" value=$oViewConf->getViewThemeParam('sLogoHeight')}]
					<a class="navbar-brand" href="[{$oViewConf->getHomeLink()}]"
					   title="[{$oxcmp_shop->oxshops__oxtitleprefix->value}]" style="display:none;">
						<img src="[{$oViewConf->getImageUrl($slogoImg)}]"
						     alt="[{$oxcmp_shop->oxshops__oxtitleprefix->value}]"
						     style="height:100%;[{*if $sLogoWidth}]width:auto;max-width:[{$sLogoWidth}]px;[{/if}][{if $sLogoHeight}]height:auto;max-height:[{$sLogoHeight}]px;[{/if*}]">
					</a>
					[{/block}]<!-- /Moving Head # Bodynova Logo Mini # -->
				</div>
				<div class="collapse navbar-collapse navbar-ex1-collapse">
					[{* Navigation *}]
					<ul id="navigation" class="nav navbar-nav">
						[{block name="dd_widget_header_categorylist_navbar_list"}]
							[{*
								Home Button
								<li [{if $homeSelected == 'true'}]class="active"[{/if}]>
									<a href="[{$oViewConf->getHomeLink()}]">[{oxmultilang ident="HOME"}]</a>
								</li>
							*}]
							[{* 1. Ebene oxrootid Top-Navigation *}]
							[{foreach from=$oxcmp_categories item="ocat" key="catkey" name="root"}]
								[{*$ocat->oxcategories__welt->value|var_dump*}]
								[{if $ocat->getIsVisible()}]
									[{* CMS - Kategorien Schleife *}]
									[{foreach from=$ocat->getContentCats() item="oTopCont" name="MoreTopCms"}]
										<li>
											<a href="[{$oTopCont->getLink()}]">[{$oTopCont->oxcontents__oxtitle->value}]</a>
										</li>
									[{/foreach}]

									[{* Top - Navigation Buttons *}]
									<li class="topnav [{if $homeSelected == 'false' && $ocat->expanded}]active[{/if}][{if $ocat->getSubCats()}] dropdown yamm-fw[{/if}]">
										<a href="[{$ocat->getLink()}]" [{if $ocat->getSubCats()}] class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"[{/if}]
										   role="button" aria-haspopup="true" aria-expanded="false">
											[{if $oxcmp_user->oxuser__oxrights->value == "malladmin" && $oViewConf->getViewThemeParam('showNavSort') == 1}]
												[{$ocat->oxcategories__oxsort->value}]
											[{/if}]
											[{$ocat->oxcategories__oxtitle->value}][{if $ocat->getSubCats()}]
												<i class="fa fa-angle-down"></i>
											[{/if}]
										</a>

										[{* Wenn Unterkategorien vorhanden sind Hover Ausklappmenue*}]
										[{if $ocat->getSubCats()}]
											<ul class="dropdown-menu">
												<li class="container-fluid">
													<div class="row row-flex">

														[{*}]<div class="container-fluid">[{*}]
														[{assign var="subcatsCount" value=$ocat->getSubCats()|@count}]
														[{assign var="counter" value=1}]
														[{*$subcatsCount*}]
														<ul class="center-block fullwidth">
															[{* 2. Ebene Unterkategorien Schleife *}]
															[{foreach from=$ocat->getSubCats() item="osubcat" key="subcatkey" name="SubCat"}]
															[{if $osubcat->getIsVisible()}]
															[{if $counter <=4}]
															<li class="col-sm-3 list-unstyled">
																[{*$counter*}]
																[{else}]
														</ul>
													</div>
													<div class="divider"></div>
													<div class="row my-row-flex margin-top-10px">
														[{assign var="counter" value=1}]
														[{assign var="ulneu" value=1}]
														<ul class="center-block fullwidth">
															<li class="col-sm-3 list-unstyled">
																[{/if}]
																<ul>
																	[{* CMS - Kategorien Schleife *}]
																	[{foreach from=$osubcat->getContentCats() item=ocont name=MoreCms}]
																		<li>
																			<a href="[{$ocont->getLink()}]">[{$ocont->oxcontents__oxtitle->value}]</a>
																		</li>
																	[{/foreach}]

																	[{if $osubcat->getIsVisible()}]
																		[{assign var="subsubcatsCount" value=$osubcat->getSubCats()|@count}]
																		[{* Ausklappmenue Titel *}]
																		<li class="nav [{if $homeSelected == 'false' && $osubcat->expanded}]active[{/if}] dropdown-header [{*if $osubcat->getHasSubCats()}]  dropdown-header[{/if*}]">
																			[{*}]<a [{if $homeSelected == 'false' && $osubcat->expanded}]class="current"[{/if}] href="[{$osubcat->getLink()}]">[{$osubcat->oxcategories__oxtitle->value}]</a>[{*}]
																			[{* 2. Ebene Unter - Navigation Buttons *}]
																			<a href="[{$osubcat->getLink()}]"[{*if $osubcat->getHasSubCats()}] class="dropdown-toggle" data-toggle="dropdown"[{/if*}] >
																				[{* oxsort->values *}]
																				[{if $oxcmp_user->oxuser__oxrights->value == "malladmin" && $oViewConf->getViewThemeParam('showNavSort') == 1}]
																					[{$osubcat->oxcategories__oxsort->value}]
																				[{/if}]
																				[{$osubcat->oxcategories__oxtitle->value}] [{if $subsubcatsCount > 0}]
																					<i class="fa fa-angle-down"></i>
																				[{/if}]
																			</a>
																		</li>
																		[{* Wenn Unterkategorien vorhanden sind *}]
																		[{if $subsubcatsCount > 0}]
																			[{*$subsubcatsCount*}]
																			[{*}] Untermenue divider <li class="divider"></li>[{*}]
																			[{*}]<ul class="dropdown-menu">[{*}]
																			[{* 3. Ebene Unterkategorien Schleife *}]
																			[{foreach from=$osubcat->getSubCats() item="oosubcat" key="subcatkey" name="SubCat"}]
																				[{if $oosubcat->getIsVisible()}]
																					[{* CMS - Kategorien Schleife *}]
																					[{foreach from=$oosubcat->getContentCats() item=subocont name=MoreCms}]
																						<li>
																							<a href="[{$subocont->getLink()}]">[{$subocont->oxcontents__oxtitle->value}]</a>
																						</li>
																					[{/foreach}]
																					[{if $oosubcat->getIsVisible()}]
																						<li class="[{if $homeSelected == 'false' && $oosubcat->expanded}]active [{/if}]nav">
																							[{* 3. Ebene Unter - Navigation Buttons *}]
																							<a [{*if $homeSelected == 'false' && $oosubcat->expanded}]class="current"[{/if*}]
																									href="[{$oosubcat->getLink()}]">
																								[{$oosubcat->oxcategories__oxtitle->value}]
																							</a>
																						</li>
																					[{/if}]
																				[{/if}]
																			[{/foreach}]
																		[{/if}]
																		[{*}]</ul>[{*}]
																	[{/if}]
																</ul>
															</li>
															[{* Counter bei jedem Schleifendurchlauf eines *}]
															[{math equation="x + y" x=$counter y=1 assign="counter"}]
															[{/if}]
															[{/foreach}]
														</ul>
														[{*}]</div>[{*}]
														[{*}]</div>[{*}]
													</div>
												</li>
											</ul>
										[{/if}]
									</li>
								[{/if}]
							[{/foreach}]
						[{/block}]
					</ul>
				</div>
			[{/block}]
		</div>
		[{* Suchformular *}]
		[{*}]
							<form id="mini-search-box" class="navbar-form navbar-left" role="search" style="display:none;">
								<div class="form-group">
									<input id="suchfeld" type="text" class="form-control" placeholder="Suchen">
								</div>
								<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
							</form><!-- /Moving Head  # Suche # -->

							<ul class="nav navbar-nav navbar-left">
								[{block name="categorylist_navbar_minibasket"}]
									[{include file="widget/header/menubasket.tpl"}]
								[{/block}]
							</ul>
							[{*}]
		[{*}]
							<ul class="nav navbar-nav navbar-right fixed-header-actions">

								[{block name="categorylist_navbar_minibasket"}]
									[{include file="widget/header/menubasket.tpl"}]
								[{/block}]

								<li>
									<a href="javascript:void(null)" class="search-toggle" rel="nofollow">
										<i class="fa fa-search"></i>
									</a>
								</li>

							</ul>
						[{*}]
		[{* Admin Button *}]
		[{*if $oView->isDemoShop()}]
							<a href="[{$oViewConf->getBaseDir()}]admin/" class="btn btn-sm btn-danger navbar-btn navbar-right visible-lg">
								[{oxmultilang ident="DD_DEMO_ADMIN_TOOL"}]
									<i class="fa fa-external-link" style="font-size: 80%;"></i>
							</a>
						[{/if*}]
		[{*}]</div>[{*}]
	[{/if}]
[{/block}]
