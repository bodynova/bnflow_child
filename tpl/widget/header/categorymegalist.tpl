[{block name="dd_widget_header_categorylist"}]
    [{if $oxcmp_categories}]
        [{assign var="homeSelected" value="false"}]
        [{if $oViewConf->getTopActionClassName() == 'start'}]
            [{assign var="homeSelected" value="true"}]
        [{/if}]
        [{assign var="oxcmp_categories" value=$oxcmp_categories}]
        [{assign var="blFullwidth" value=$oViewConf->getViewThemeParam('blFullwidthLayout')}]

<div class="cd-dropdown-wrapper">
[{* Welten Button *}]
    <a class="cd-dropdown-trigger" href="#0">Massage & Praxisbedarf</a>

    <nav class="cd-dropdown">
[{* Bodynova Logo im Moving Head *}]
        <h2>Bodynova GmbH</h2>
[{* Colse X im Moving Head *}]
        <a href="#0" class="cd-close">Close</a>
[{* Dropdown Inhalt *}]
        <ul class="cd-dropdown-content">
            [{block name="dd_widget_header_categorylist_navbar"}]
                [{block name="dd_widget_header_categorylist_navbar_list"}]
                    [{*
                        Home Button
                        <li [{if $homeSelected == 'true'}]class="active"[{/if}]>
                            <a href="[{$oViewConf->getHomeLink()}]">[{oxmultilang ident="HOME"}]</a>
                        </li>
                    *}]

[{* Bereicht Für Suchfeld *}]

[{* /Suche *}]
                    [{* 1. Ebene oxrootid Top-Navigation *}]
                    [{foreach from=$oxcmp_categories item="ocat" key="catkey" name="root"}]
                        [{*$ocat->oxcategories__welt->value|var_dump*}]

                        [{if $ocat->getIsVisible()}]
                            [{* CMS - Kategorien Schleife *}]
[{* ??? *}]
                            [{foreach from=$ocat->getContentCats() item="oTopCont" name="MoreTopCms"}]
                                <li>
                                    <a href="[{$oTopCont->getLink()}]">[{$oTopCont->oxcontents__oxtitle->value}]</a>
                                </li>
                            [{/foreach}]

[{* Dropdown 1. Ebene weitere Dropdowns 4 Kateorien rechts: *}]
[{* Clothing > *}]
                            [{* Top - Navigation Buttons *}]
                            <li class="[{if $ocat->getSubCats()}]has-children[{/if}]">
                                <a href="[{$ocat->getLink()}]">
                                    [{if $oxcmp_user->oxuser__oxrights->value == "malladmin" && $oViewConf->getViewThemeParam('showNavSort') == 1}]
                                        [{$ocat->oxcategories__oxsort->value}]
                                    [{/if}]
                                    [{$ocat->oxcategories__oxtitle->value}]
	                                [{*if $ocat->getSubCats()}]
		                                <i class="fa fa-angle-down"></i>
	                                [{/if*}]
                                </a>

[{* Wenn Unterkategorien vorhanden sind Hover Ausklappmenue Rechts *}]
                                [{if $ocat->getSubCats()}]
                                    <ul class="cd-secondary-dropdown is-hidden fade-out">
[{* sichtbarer Back Link col-xs col-sm *}]
[{* < Menu *}]
                                        <li class="go-back">
                                            <a href="#0">Menu</a>
                                        </li>
[{* All Clothing *}]
                                        <li class="see-all">
                                            <a href="[{$ocat->getLink()}]">All [{$ocat->oxcategories__oxtitle->value}]</a>
                                        </li>

                                        [{assign var="subcatsCount" value=$ocat->getSubCats()|@count}]
                                        [{assign var="counter" value=1}]


                                            [{* 2. Ebene Unterkategorien Schleife *}]
                                            [{foreach from=$ocat->getSubCats() item="osubcat" key="subcatkey" name="SubCat"}]
	                                            <li class="[{if $osubcat->getSubCats()}]has-children[{/if}]">
		                                            <a href="[{$osubcat->getLink()}]">
			                                            [{$osubcat->oxcategories__oxtitle->value}]
		                                            </a>
	                                                [{if $osubcat->getIsVisible()}]
		                                                AAA
	                                                    [{* CMS - Kategorien Schleife *}]
	                                                    [{foreach from=$osubcat->getContentCats() item=ocont name=MoreCms}]
	                                                        <a href="[{$ocont->getLink()}]">
		                                                        [{$ocont->oxcontents__oxtitle->value}]
	                                                        </a>
	                                                    [{/foreach}]

	                                                    <ul class="is-hidden">
	[{* sichtbarer Back Link col-xs col-sm *}]
	[{* < Clothing *}]
	                                                        <li class="go-back">
	                                                            <a href="#0">
		                                                            [{$ocont->oxcategories__oxtitle->value}]
	                                                            </a>
	                                                        </li>
	[{* All Accessories *}]
	                                                        <li class="see-all">
	                                                            <a href="[{$osubcat->getLink()}]">
		                                                            All [{$osubcat->oxcategories__oxtitle->value}]
	                                                            </a>
	                                                        </li>

	                                                        [{assign var="subsubcatsCount" value=$osubcat->getSubCats()|@count}]
	                                                        <li class="has-children">

	                                                           [{* Wenn Unterkategorien vorhanden sind *}]

	                                                           [{if $subsubcatsCount > 0}]
	                                                                [{foreach from=$osubcat->getSubCats() item="oosubcat" key="subcatkey" name="SubCat"}]
	                                                                        [{* CMS - Kategorien Schleife *}]
	    [{* Ohne Drop Down right ??? *}]
	        [{* Beanies > *}]
	                                                                        [{if $oosubcat->getIsVisible()}]
	                                                                            <ul class="is-hidden">
	                                                                                <li class="go-back">
	                                                                                    <a href="#0">
		                                                                                    [{$oosubcat->oxcategories__oxtitle->value}]
	                                                                                    </a>
	                                                                                </li>
				[{* All Beanies *}]???
	                                                                                <li class="see-all">
	                                                                                    <a href="[{$subocont->getLink()}]">
		                                                                                    All [{$subocont->oxcontents__oxtitle->value}]
	                                                                                    </a>
	                                                                                </li>
					[{* Caps & Hats *}]
																					[{foreach from=$oosubcat->getContentCats() item=subocont name=MoreCms}]
																						<li>
																							<a href="[{$subocont->getLink()}]">
																								[{$subocont->oxcontents__oxtitle->value}]
																							</a>
																						</li>

																						[{*}]
																						<li>
	                                                                                        <a href="[{$oosubcat->getLink()}]">
	                                                                                            [{$oosubcat->oxcategories__oxtitle->value}]
	                                                                                        </a>
	                                                                                    </li>
	                                                                                    [{*}]
	                                                                                [{/foreach}]
	                                                                            </ul>
	                                                                        [{/if}]

	                                                                [{/foreach}]
	                                                            [{/if}]
															</li>
														</ul>
														[{* Counter bei jedem Schleifendurchlauf eines *}]
	                                                    [{math equation="x + y" x=$counter y=1 assign="counter"}]
													[{/if}]
	                                            </li>
											[{/foreach}]
										</li>
									</ul>
								[{/if}]
							</li>
						[{/if}]
					[{/foreach}]
				[{/block}]
			[{/block}]
		</ul>
	</nav>
</div>