[{if $oView->isEnabled() && $smarty.cookies.displayedCookiesNotification != '1'}]
    [{oxscript include="js/libs/jquery.cookie.min.js"}]
    [{oxscript add="$.cookie('testing', 'yes'); if(!$.cookie('testing')) $('#cookieNote').hide(); else{ $('#cookieNote').show(); $.cookie('testing', null, -1);}"}]
    [{oxscript include="js/widgets/oxcookienote.min.js"}]
    <div id="cookieNote">
        <div class="alert alert-info bn-coockie" style="margin: 0;">
            [{oxmultilang ident='COOKIE_NOTE'}]
            <div class="pull-right">
                <button type="button" class="btn btn-success" data-dismiss="alert">
                    Ja Einverstanden
                    [{*}]<span aria-hidden="true">&times;</span>[{*}]
                    <span class="sr-only">
                    [{oxmultilang ident='COOKIE_NOTE_CLOSE'}]
                    </span>
                </button>
                <a class="btn btn-danger" href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=clearcookies"}]"
                   title="[{oxmultilang ident='COOKIE_NOTE_DISAGREE'}]">
                    [{*oxmultilang ident='COOKIE_NOTE_DISAGREE'*}]
                    Nicht Einverstanden
                </a>
            </div>
            [{*}]<span class="cancelCookie">[{*}]
            [{*}]</span>[{*}]
        </div>
    </div>
    [{oxscript add="$('#cookieNote').oxCookieNote();"}]
[{/if}]
[{oxscript widget=$oView->getClassName()}]
