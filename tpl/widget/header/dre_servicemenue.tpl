[{* Profil *}]
<div class="btn-group user-profile">
    <button type="button" class="btn dropdown-toggle bodyGrau" data-toggle="dropdown" style="border: none">
        [{if $oxcmp_user->oxuser__oxpassword->value}]
            <i class="fal fa-user-check fa-2x"></i>
        [{else}]
            <i class="fal fa-user-times fa-2x"></i>
        [{/if}]
    </button>
    <ul class="dropdown-menu dropdown-menu-right left-auto" role="menu">
        [{* Begrüßung *}]
        [{*if $oxcmp_user->oxuser__oxpassword->value}]
            <li class="text-align-right">
                [{assign var="fullname" value=$oxcmp_user->oxuser__oxfname->value|cat:" "|cat:$oxcmp_user->oxuser__oxlname->value }]
                [{oxmultilang ident="GREETING"}]
                [{if $fullname}]
                    [{$fullname}]
                [{else}]
                    [{$oxcmp_user->oxuser__oxusername->value|oxtruncate:25:"...":true}]
                [{/if}]
            </li>
            <hr style="margin-top: 5px;margin-bottom: 5px">
        [{/if*}]
        <li class="">
            [{include file="widget/header/loginbox.tpl"}]
        </li>
        [{block name="widget_header_servicebox_items"}]
            [{if $oxcmp_user->oxuser__oxpassword->value}]
                <li class="text-align-right">
                    <a href="[{oxgetseourl ident=$oViewConf->getSslSelfLink()|cat:"cl=account"}]">[{oxmultilang ident="MY_ACCOUNT"}]</a>
                </li>
                [{if $oViewConf->getShowCompareList()}]
                    <li class="text-align-right">
                        <a href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=compare"}]">
                            [{if $oView->getCompareItemsCnt()}]<span
                                    class="badge">[{$oView->getCompareItemsCnt()}]</span>[{/if}][{oxmultilang ident="MY_PRODUCT_COMPARISON"}]
                        </a>
                    </li>
                [{/if}]
                <li class="text-align-right">
                    <a href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=account_noticelist"}]"><span>
                                [{if $oxcmp_user && $oxcmp_user->getNoticeListArtCnt()}] <span
                                        class="badge">[{$oxcmp_user->getNoticeListArtCnt()}]</span>[{/if}][{oxmultilang ident="MY_WISH_LIST"}]</span></a>
                </li>
                [{if $oViewConf->getShowWishlist()}]
                    <li class="text-align-right">
                        <a href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=account_wishlist"}]"><span>
                                    [{if $oxcmp_user && $oxcmp_user->getWishListArtCnt()}] <span
                                            class="badge">[{$oxcmp_user->getWishListArtCnt()}]</span>[{/if}][{oxmultilang ident="MY_GIFT_REGISTRY"}]</span></a>
                    </li>
                [{/if}]
                [{if $oViewConf->getShowListmania()}]
                    <li class="text-align-right">
                        <a href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=account_recommlist"}]"><span>
                                    [{if $oxcmp_user && $oxcmp_user->getRecommListsCount()}] <span
                                            class="badge">[{$oxcmp_user->getRecommListsCount()}]</span>[{/if}][{oxmultilang ident="MY_LISTMANIA"}]</span></a>
                    </li>
                [{/if}]
                [{if $oViewConf->isFunctionalityEnabled( "blEnableDownloads" )}]
                    <li class="text-align-right">
                        <a href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=account_downloads"}]"><span>[{oxmultilang ident="MY_DOWNLOADS"}]</span></a>
                    </li>
                [{/if}]
                [{* Logout *}]
                <li class="text-align-right" style="margin-top: 5px">
                    <a href="[{$oViewConf->getLogoutLink()}]" class="btn btn-danger" role="getLogoutLink">
                        <i class="fal fa-power-off"></i> [{oxmultilang ident="LOGOUT"}]
                    </a>
                </li>
            [{/if}]
        [{/block}]
    </ul>
</div>