[{*

    !!! Wird in der EE wie folgt extended: /var/www/oxideshop/vendor/oxid-esales/oxideshop-ee/Application
        views/flow/tpl/widget/header/menubasket.tpl
    !!!

*}]
<li id="basket-moving-head" class="hidden-xs hidden-sm">
	<a href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=basket"}]" rel="nofollow">
		header menubasket
		<i class="fal fa-shopping-cart fa-2x"></i>
        [{if isset($oxcmp_basket) && $oxcmp_basket->getItemsCount() > 0}]
			[{ $oxcmp_basket->getItemsCount() }]
        [{/if}]
	</a>
</li>
