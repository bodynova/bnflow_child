<div class="btn-group minibasket-menu">
    <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" data-href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=basket"}]" style="background-color: inherit;border: none">
        [{block name="dd_layout_page_header_icon_menu_minibasket_button"}]
            <i class="fal fa-shopping-cart fa-2x"></i>
            [{if $oxcmp_basket->getItemsCount() > 0}]
                [{$oxcmp_basket->getItemsCount()}]
            [{else}]
                [{*}]
                    <i class="fa fa-angle-down"></i>
                [{*}]
            [{/if}]
        [{/block}]
    </button>
    <ul class="dropdown-menu dropdown-menu" role="menu" style="padding: 10px; min-width: 285px">
        [{block name="dd_layout_page_header_icon_menu_minibasket_list"}]
            <li>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="minibasket-menu-box">
                            [{oxid_include_dynamic file="widget/minibasket/minibasket.tpl"}]
                        </div>
                    </div>
                </div>
            </li>
        [{/block}]
    </ul>
</div>