[{if $oxcmp_user}]
    [{assign var="noticeListCount" value=$oxcmp_user->getNoticeListArtCnt()}]
    [{assign var="wishListCount" value=$oxcmp_user->getWishListArtCnt()}]
    [{assign var="recommListCount" value=$oxcmp_user->getRecommListsCount()}]
[{else}]
    [{assign var="noticeListCount" value="0"}]
    [{assign var="wishListCount" value="0"}]
    [{assign var="recommListCount" value="0"}]
[{/if}]
[{math equation="a+b+c+d" a=$oView->getCompareItemsCnt() b=$noticeListCount c=$wishListCount d=$recommListCount assign="notificationsCounter"}]

<div class="btn-group user-profile service-menu [{if !$oxcmp_user}]showLogin[{/if}]">
	<button type="button" class="btn dropdown-toggle bodyGrau" data-toggle="dropdown" style="border: none"
	        data-href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=account"}]">
        [{if $oxcmp_user}]
			<i class="fal fa-user-check fa-2x"></i>
        [{else}]
			<i class="fal fa-user-times fa-2x"></i>
        [{/if}]
        [{if $notificationsCounter > 0}]
			<span class="badge">[{$notificationsCounter}]</span>
        [{/if}]
	</button>
	<ul class="dropdown-menu dropdown-menu-right left-auto" role="menu">
        [{block name="dd_layout_page_header_icon_menu_account_list"}]
			<li>
            [{* Begrüßung *}]
            [{if $oxcmp_user}]
				<li class="text-align-right">
                    [{assign var="fullname" value=$oxcmp_user->oxuser__oxfname->value|cat:" "|cat:$oxcmp_user->oxuser__oxlname->value }]
                    [{oxmultilang ident="GREETING"}]
                    [{if $fullname}]
                        [{$fullname}]
                    [{else}]
                        [{$oxcmp_user->oxuser__oxusername->value|oxtruncate:25:"...":true}]
                    [{/if}]
				</li>
				<hr style="margin-top: 5px;margin-bottom: 5px">
				<li>
					<div class="service-menu-box clearfix">
                        [{include file="widget/header/servicebox.tpl"}]
					</div>
				</li>
            [{/if}]
            [{if !$oxcmp_user}]
				<div class="col-xs-12">
					<div class="service-menu-box clearfix">
                        [{include file="widget/header/loginbox.tpl"}]
					</div>
				</div>
            [{/if}]
			</li>
            [{if $oxcmp_user}]
				<hr style="margin-top: 5px;margin-bottom: 5px">
				<li class="text-align-right" style="margin-top: 5px">
					<a href="[{$oViewConf->getLogoutLink()}]" class="btn btn-danger" role="getLogoutLink">
						<i class="fal fa-power-off"></i> [{oxmultilang ident="LOGOUT"}]
					</a>
				</li>
            [{/if}]

        [{/block}]
	</ul>
</div>