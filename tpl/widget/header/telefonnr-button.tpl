[{* Telefonnummer *}]
<div class="btn-group tel-button">
	<button type="button" class="btn dropdown-toggle" data-toggle="dropdown" style="background-color:white;border:none">
		<i class="fal fa-phone fa-2x"></i>
	</button>
	<ul class="dropdown-menu dropdown-menu-right left-auto" role="menu">
		<li>
			<address>
				<a href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=contact"}]"
				   title="[{oxmultilang ident="WIDGET_SERVICES_CONTACTS"}]">
					<strong>Mo-Fr: 9 - 17 Uhr</strong><br>
				</a>
				<abbr title="Telefonnummer">tel:</abbr>[{oxmultilang ident="TELEFONNUMMER"}]
			</address>
		</li>
	</ul>
</div>