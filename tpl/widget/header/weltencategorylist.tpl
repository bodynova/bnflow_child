[{oxscript add="
        function movshowsearch(e) {
                $('#mov-mini-search-box').css('display', 'block');
                $('#mov-mini-search-box').removeClass('hidden');
                $('#mov-head-lupe-button').hide();
                $('#mov-mini-search-box>form.form.search>:nth-child(4)>div.easy-autocomplete.eac-custom>input').focus();
        }
"}]

[{assign var="act" value=$oxcmp_categories->getClickCat()}]
[{assign var="actint" value=$act->oxcategories__welt->value|intval}]
[{if $oView->getClassName() == 'content'}]
    [{assign var="oContent" value=$oView->getContent()}]
    [{if $oContent}]
        [{assign var="ContWelt" value=$oContent->oxcontents__oxloadid->value|substr:4:1}]
    [{/if}]
[{/if}]

<nav id="mainnav" class="stellarnav" style="min-height: 36px">


	<ul style="z-index: 100000;background-color: #F0F0F0">

		<li id="my-moving-head-logo" class="moving-head-logo hidden-xs hidden-sm"
		    style="width: 100%; display: none;padding:0">
            [{assign var="slogoImg" value=$oViewConf->getViewThemeParam('sLogoFile')}]
            [{assign var="sLogoWidth" value=$oViewConf->getViewThemeParam('sLogoWidth')}]
            [{assign var="sLogoHeight" value=$oViewConf->getViewThemeParam('sLogoHeight')}]
			<a href="[{$oViewConf->getHomeLink()}]" title="[{$oxcmp_shop->oxshops__oxtitleprefix->value}]"
			   style="padding: 0" class="pull-left">
				<img src="[{$oViewConf->getImageUrl($slogoImg)}]"
				     alt="[{$oxcmp_shop->oxshops__oxtitleprefix->value}]"
				     style="[{if $sLogoWidth}]width:auto;max-width:[{$sLogoWidth}]px;[{/if}][{if $sLogoHeight}]height:auto;max-height:37px;[{/if}]">
			</a>
		</li>

		<li class="symbols hidden-lg hidden-md">


			<div class="btn-group-justified">

                [{* Länder Flaggen *}]
                [{if $oView->isLanguageLoaded()}]
					<div class="btn-group dropdown languages-menu">
						<button type="button" class="btn dropdown-toggle bodyButton" data-toggle="dropdown"
						        style="border:none" aria-haspopup="true" aria-expanded="false">
                            [{*assign var="sLangImg" value="lang/"|cat:$oViewConf->getActLanguageAbbr()|cat:".png"*}]
                            [{block name="dd_layout_page_header_icon_menu_languages_button"}]
								<span class="bn-flag-[{$oViewConf->getActLanguageAbbr()}]"></span>
                                [{*}]<img src="[{$oViewConf->getImageUrl($sLangImg)}]" alt=""/><i class="fa fa-angle-down"></i>[{*}]
                            [{/block}]
						</button>
						<ul class="dropdown-menu" role="menu"
						    style="min-width: 0;border:1px solid #E7E7E7;position: absolute;left:37px !important;top:45px;padding-left: 10px;padding-right: 10px;z-index: 100002">
                            [{block name="dd_layout_page_header_icon_menu_languages_list"}]
                                [{foreach from=$oxcmp_lang item=_lng}]
                                    [{assign var="sLangImg" value="lang/"|cat:$_lng->abbr|cat:".png"}]
                                    [{if $_lng->selected}]
                                        [{capture name="languageSelected"}]
											<a class="[{$_lng->abbr}]" title="[{$_lng->name}]"
											   href="[{$_lng->link|oxaddparams:$oView->getDynUrlParams()}]"
											   hreflang="[{$_lng->abbr}]" style="padding:0">
												<span class="bn-flag-[{$_lng->abbr}]"></span>
                                                [{*}]<span style="background-image:url('[{$oViewConf->getImageUrl($sLangImg)}]')" >[{$_lng->name}]</span>[{*}]
											</a>
                                        [{/capture}]
                                    [{/if}]
									<li[{if $_lng->selected}] class="active"[{/if}] style="padding-left: 0">
										<a class="text-align-left [{$_lng->abbr}]" title="[{$_lng->name}]"
										   href="[{$_lng->link|oxaddparams:$oView->getDynUrlParams()}]"
										   hreflang="[{$_lng->abbr}]" style="padding:0">
											<span class="bn-flag-[{$_lng->abbr}]"></span>
                                            [{*}]<img src="[{$oViewConf->getImageUrl($sLangImg)}]" alt=""/> [{$_lng->name}][{*}]
										</a>
									</li>
                                [{/foreach}]
                            [{/block}]
						</ul>
					</div>
                [{/if}]

                [{* Telefonnummer *}]
				<div class="btn-group tel-button">
					<button type="button" class="btn dropdown-toggle bodyButton" data-toggle="dropdown"
					        style="border:none" aria-haspopup="true" aria-expanded="false">
						<i class="fal fa-phone fa-2x"></i>
					</button>
					<ul class="dropdown-menu dropdown-menu-right text-align-right left-auto" role="menu"
					    style="min-width: 0;border:1px solid #E7E7E7;position: absolute;top:50px;z-index: 100001;">
						<li style="padding-left: 0;padding-right:0;border:none; max-width: 210px;width:210px">
							<address>
								<a href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=contact"}]"
								   title="[{oxmultilang ident="WIDGET_SERVICES_CONTACTS"}]">
									<strong>[{oxmultilang ident="OEFFNUNGSZEITEN"}]</strong><br>
								</a>
								<a href="tel:[{oxmultilang ident="TELEFONNUMMER"}]"
								   title="Telefonnummer">[{oxmultilang ident="TELEFONNUMMER"}]</a>
							</address>
						</li>
					</ul>
				</div>


                [{* User-Profil Dropup *}]
                <div class="btn-group dropDown">
                    <button type="button" class="btn dropdown-toggle bodyGrau bodyButton" data-toggle="dropdown" style="border:none"
                            aria-haspopup="true" aria-expanded="false">
                        [{if $oxcmp_user->oxuser__oxpassword->value}]
                            <i class="fal fa-user-check fa-2x"></i>
                        [{else}]
                            <i class="fal fa-user-times fa-2x"></i>
                        [{/if}]
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right text-align-right left-auto" role="menu"
                        style="min-width: 0;border:1px solid #E7E7E7;position: absolute;z-index: 100001;top:50px">
                        [{if $oxcmp_user->oxuser__oxpassword->value}]
                            <li class="text-align-right" style="padding-left: 5px;padding-right:5px;padding-top:5px">
                                [{assign var="fullname" value=$oxcmp_user->oxuser__oxfname->value|cat:" "|cat:$oxcmp_user->oxuser__oxlname->value }]
                                [{oxmultilang ident="GREETING"}]
                                [{if $fullname}]
                                    [{$fullname}]
                                [{else}]
                                    [{$oxcmp_user->oxuser__oxusername->value|oxtruncate:25:"...":true}]
                                [{/if}]
                            </li>
                        [{/if}]
                        <li style="padding-left: 5px;padding-right:5px;max-width: 280px;width:280px">
                            [{include file="widget/header/loginbox.tpl"}]
                        </li>
                        [{block name="widget_header_servicebox_items"}]
                            [{if $oxcmp_user->oxuser__oxpassword->value}]
                                <li class="text-align-right" style="padding-left: 5px;padding-right:5px;max-width: 380px">
                                    <a href="[{oxgetseourl ident=$oViewConf->getSslSelfLink()|cat:"cl=account"}]">[{oxmultilang ident="MY_ACCOUNT"}]</a>
                                </li>
                                [{if $oViewConf->getShowCompareList()}]
                                    <li class="text-align-right"
                                        style="padding-left: 5px;padding-right:5px;max-width: 380px">
                                        <a href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=compare"}]">[{oxmultilang ident="MY_PRODUCT_COMPARISON"}]</a> [{if $oView->getCompareItemsCnt()}]
                                            <span class="badge">[{$oView->getCompareItemsCnt()}]</span>[{/if}]
                                    </li>
                                [{/if}]
                                <li class="text-align-right" style="padding-left: 5px;padding-right:5px;max-width: 380px">
                                    <a href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=account_noticelist"}]">
                                        <span>
                                            [{if $oxcmp_user && $oxcmp_user->getNoticeListArtCnt()}]
                                                <span class="badge">[{$oxcmp_user->getNoticeListArtCnt()}]</span>
                                            [{/if}]
                                            [{oxmultilang ident="MY_WISH_LIST"}]
                                        </span>
                                    </a>
                                </li>
                                [{if $oViewConf->getShowWishlist()}]
                                    <li class="text-align-right" style="padding-left: 5px;padding-right:5px;max-width: 380px">
                                        <a href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=account_wishlist"}]">
                                            <span>
                                                [{if $oxcmp_user && $oxcmp_user->getWishListArtCnt()}]
                                                    <span class="badge">[{$oxcmp_user->getWishListArtCnt()}]</span>
                                                [{/if}]
                                                [{oxmultilang ident="MY_GIFT_REGISTRY"}]
                                            </span>
                                        </a>
                                    </li>
                                [{/if}]
                                [{if $oViewConf->getShowListmania()}]
                                    <li class="text-align-right" style="padding-left: 5px;padding-right:5px;max-width: 380px">
                                        <a href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=account_recommlist"}]">
                                            <span>
                                                [{if $oxcmp_user && $oxcmp_user->getRecommListsCount()}]
                                                    <span class="badge">[{$oxcmp_user->getRecommListsCount()}]</span>
                                                [{/if}]
                                                [{oxmultilang ident="MY_LISTMANIA"}]
                                            </span>
                                        </a>
                                    </li>
                                [{/if}]
                                [{if $oViewConf->isFunctionalityEnabled( "blEnableDownloads" )}]
                                    <li class="text-align-right" style="padding-left: 5px;padding-right:5px;max-width: 380px">
                                        <a href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=account_downloads"}]">
                                            <span>
                                                [{oxmultilang ident="MY_DOWNLOADS"}]
                                            </span>
                                        </a>
                                    </li>
                                [{/if}]
                                [{* Logout *}]
                                <li class="text-align-right" style="padding-left: 5px;padding-right:5px;max-width: 380px">
                                    <a href="[{$oViewConf->getLogoutLink()}]" class="btn btn-danger" role="getLogoutLink" style="color:white">
                                        <i class="fal fa-power-off"></i> [{oxmultilang ident="LOGOUT"}]
                                    </a>
                                </li>
                            [{/if}]
                        [{/block}]
                    </ul>
                </div>
			</div>
		</li>

		<hr class="hidden-lg hidden-md" style="margin: 0;border:1px solid #e7e7e7;"/>
        [{block name="layout_header_bottom"}]
            [{* 1 *}]
            [{* Bodhi-Shop *}]
			<li class="has-sub [{if $actint == 5}]opened[{/if}]">
                [{assign var="oCat" value=$oxcmp_categories->dre_loadWeltList(5)}]
                [{assign var="welt" value=$oxcmp_categories->getWelt(5)}]
                [{if $oxcmp_categories->dre_getWeltString(5) !== 0}]
					<a class="btn btn-welt [{if $actint == 5 || $ContWelt == 5}]active[{/if}]"
					   href="[{oxgetseourl ident=$oxcmp_categories->dre_getWeltString(5) type="oxcontent"}]">
                        [{oxifcontent ident=$oxcmp_categories->dre_getWeltString(5) object="oCont"}]
                        [{$oCont->oxcontents__oxtitle->value|html_entity_decode}]
                            [{*$oCont->oxcontents__oxcontent->value*}]
                            [{*$oCont|var_dump*}]
                        [{/oxifcontent}]
					</a>
                [{/if}]
				<ul>
                    [{*$oCat|var_dump*}]
                    [{oxid_include_widget cl="oxwCategoryTree" cnid=$oCat sWidgetType="header" nocookie=1 welt=$welt}]
				</ul>
			</li>
            [{* 2 *}]

            [{* Yoga & Pilates *}]
			<li class="has-sub [{if $actint == 3}]opened[{/if}]">
                [{assign var="oCat" value=$oxcmp_categories->dre_loadWeltList(3)}]
                [{assign var="welt" value=$oxcmp_categories->getWelt()}]
                [{if $oxcmp_categories->dre_getWeltString(3) !== 0}]
					<a class="btn btn-welt [{if $actint == 3|| $ContWelt == 3}]active[{/if}]"
					   href="[{oxgetseourl ident=$oxcmp_categories->dre_getWeltString(3) type="oxcontent"}]">
                        [{oxifcontent ident=$oxcmp_categories->dre_getWeltString(3) object="oCont"}]
                        [{$oCont->oxcontents__oxtitle->value|html_entity_decode}]
                            [{*$oCont->oxcontents__oxcontent->value*}]
                            [{*$oCont|var_dump*}]
                        [{/oxifcontent}]
					</a>
                [{/if}]
				<ul>
                    [{oxid_include_widget cl="oxwCategoryTree" cnid=$oCat sWidgetType="header" nocookie=1 welt=3}]
				</ul>
			</li>
            [{* 3 *}]
            [{* Yoga Kleidung *}]
			<li class="has-sub [{if $actint == 6}]opened[{/if}]">
                [{assign var="oCat" value=$oxcmp_categories->dre_loadWeltList(6)}]
                [{assign var="welt" value=$oxcmp_categories->getWelt(6)}]
                [{if $oxcmp_categories->dre_getWeltString(6) !== 0}]
					<a class="btn btn-welt [{if $actint == 6 || $ContWelt == 6}]active[{/if}]"
					   href="[{oxgetseourl ident=$oxcmp_categories->dre_getWeltString(6) type="oxcontent"}]">
                        [{oxifcontent ident=$oxcmp_categories->dre_getWeltString(6) object="oCont"}]
                        [{$oCont->oxcontents__oxtitle->value|html_entity_decode}]
                            [{*$oCont->oxcontents__oxcontent->value*}]
                            [{*$oCont|var_dump*}]
                        [{/oxifcontent}]
					</a>
                [{/if}]
				<ul>
                    [{oxid_include_widget cl="oxwCategoryTree" cnid=$oCat sWidgetType="header" nocookie=1 welt=6}]
				</ul>
			</li>
            [{* 4 *}]
            [{* Wellness & Gesundheit *}]
			<li class="drop-left has-sub [{if $actint == 2}]opened[{/if}]">
                [{assign var="oCat" value=$oxcmp_categories->dre_loadWeltList(2)}]
                [{assign var="welt" value=$oxcmp_categories->getWelt()}]
                [{if $oxcmp_categories->dre_getWeltString(2) !== 0}]
					<a class="btn btn-welt [{if $actint == 2 || $ContWelt == 2}]active[{/if}]"
					   href="[{oxgetseourl ident=$oxcmp_categories->dre_getWeltString(2) type="oxcontent"}]">
                        [{oxifcontent ident=$oxcmp_categories->dre_getWeltString(2) object="oCont"}]
                        [{$oCont->oxcontents__oxtitle->value|html_entity_decode}]
                            [{*$oCont->oxcontents__oxcontent->value*}]
                            [{*$oCont|var_dump*}]
                        [{/oxifcontent}]
					</a>
                [{/if}]
				<ul>
                    [{oxid_include_widget cl="oxwCategoryTree" cnid=$oCat left="true" sWidgetType="header" nocookie=1 welt=2}]
				</ul>
			</li>
            [{* 5 *}]
            [{* Massage & Praxisbedarf *}]
			<li class="drop-left has-sub [{if $actint == 1}]opened[{/if}]">
                [{assign var="oCat" value=$oxcmp_categories->dre_loadWeltList(1)}]
                [{assign var="welt" value=$oxcmp_categories->getWelt()}]
                [{*$oxcmp_categories->dre_getWeltString(1)*}]
                [{if $oxcmp_categories->dre_getWeltString(1) !== 0}]
					<a class="btn btn-welt [{if $actint == 1 || $ContWelt == 1}]active[{/if}]"
					   href="[{oxgetseourl ident=$oxcmp_categories->dre_getWeltString(1) type="oxcontent"}]">
                        [{oxifcontent ident=$oxcmp_categories->dre_getWeltString(1) object="oCont"}]
                        [{$oCont->oxcontents__oxtitle->value|html_entity_decode}]
                            [{*$oCont->oxcontents__oxcontent->value*}]
                            [{*$oCont|var_dump*}]
                        [{/oxifcontent}]
					</a>
                [{/if}]
				<ul>
                    [{oxid_include_widget cl="oxwCategoryTree" cnid=$oCat left="true" sWidgetType="header" nocookie=1 welt=1}]
				</ul>
			</li>
            [{*/block*}]
            [{* 6 *}]
            [{* MEDITATION
			<li class="has-sub">
				[{assign var="oCat" value=$oxcmp_categories->dre_loadWeltList(4)}]
				[{assign var="welt" value=$oxcmp_categories->getWelt(4)}]
				[{if $oxcmp_categories->dre_getWeltString(4) !== 0}]
					<a class="btn btn-welt [{if $actint == 4}]active[{/if}]" href="[{oxgetseourl ident=$oxcmp_categories->dre_getWeltString(4) type="oxcontent"}]">
						[{oxifcontent ident=$oxcmp_categories->dre_getWeltString(4) object="oCont"}]
							[{$oCont->oxcontents__oxtitle->value|html_entity_decode}]
							[{*$oCont->oxcontents__oxcontent->value*}]
            [{*$oCont|var_dump

		[{/oxifcontent}]
	</a>
[{/if}]
<ul>
	[{oxid_include_widget cl="oxwCategoryTree" cnid=$oCat sWidgetType="header" nocookie=1}]
</ul>
</li>
*}]
            [{* blog
			<li class="drop-left [{if $actint == 1}]opened[{/if}]">
				<a class="btn btn-welt [{if $actint == 1}]active[{/if}]"
					href="[{$oViewConf->getCurrentHomeDir()}]index.php?cl=dre_bloglist">
					Blog
				</a>
			</li>
			*}]
			<li id="lupe-moving-head" class="hidden-xs hidden-sm">
				<div class="btn btn-group mov-head-lupe">
					<button id="mov-head-lupe-button" type="button" class="btn" onclick="movshowsearch();">
						<i class="fal fa-search"></i>
					</button>
				</div>
				<div id="mov-mini-search-box" class="hidden pull-right">
                    [{* Suche *}]
                    [{include file="widget/header/search_top.tpl"}]
				</div>
			</li>
            [{*block name="categorylist_navbar_minibasket"}]
				[{include file="widget/header/menubasket.tpl"}]
			[{/block*}]
        [{/block}]
	</ul>

</nav>