[{assign var="_newitem" value=1}]
[{if $oxcmp_basket->getProductsCount() && $_newitem}]
    [{oxhasrights ident="TOBASKET"}]
        <div class="alert alert-success" id="newItemMsg">
            <div class="container-fluid">
                <div class="row">
                    [{block name="dd_widget_minibasket_new_item_msg"}]
                        <div class="col-md-4">
                            [{oxmultilang ident="NEW_BASKET_ITEM_MSG"}]
                        </div>

                        <div class="col-md-4">
                            [{*}]
                                "delivery costs" : [{$oxcmp_basket->getDeliveryCosts()}]<br/>
                                "$oxcmp_basket->getProductsCount()" : [{$oxcmp_basket->getProductsCount()}]<br/>
                                "$oViewConf->freeShippingTreshold()" : [{$oViewConf->freeShippingTreshold()}]<br/>
                                "$oViewConf->freeShippingOver()" : [{$oViewConf->freeShippingOver()}]<br/>
                                "$oxcmp_basket->getBruttoSum()" : [{$oxcmp_basket->getBruttoSum()}]<br/>
                                "$oxcmp_basket->getBruttoSum() > $oViewConf->freeShippingTreshold()" : [{$oxcmp_basket->getBruttoSum() > $oViewConf->freeShippingTreshold()}]<br/>
                                "$oxcmp_basket->getBruttoSum() < $oViewConf->freeShippingOver()" : [{$oxcmp_basket->getBruttoSum() < $oViewConf->freeShippingOver()}]<br/>
                                "$oViewConf->freeShippingOver()" : [{$oViewConf->freeShippingOver()}]<br/>
                                "$oxcmp_basket->getBruttoSum()" : [{$oxcmp_basket->getBruttoSum()}]<br/>
                                [{math equation="x-y" x=$oViewConf->freeShippingOver() y=$oxcmp_basket->getBruttoSum() assign="ordermore" format="%.2f"}]
                                [{assign var="Restkaufwert" value=$ordermore|number_format:2:",":' '|cat:$currency->sign}]
                                "Restkaufwert" : [{$Restkaufwert}]<br/>
                            [{*}]

                            [{if $oxcmp_user->oxuser__oxcountryid->value == 'a7c40f631fc920687.20179984'}]
                                [{if $oxcmp_basket->getProductsCount() && $oxcmp_basket->getDeliveryCosts() && $oxcmp_basket->getBruttoSum() > $oViewConf->freeShippingTreshold() && $oxcmp_basket->getBruttoSum() < $oViewConf->freeShippingOver()}]
                                    [{strip}]
                                        [{math equation="x-y" x=$oViewConf->freeShippingOver() y=$oxcmp_basket->getBruttoSum() assign="ordermore" format="%.2f"}]
                                        [{assign var="Restkaufwert" value=$ordermore|number_format:2:",":' '|cat:$currency->sign}]
                                        <div class="text-center" style="display: table-cell; vert-align: middle">
                                            <a href="[{oxgetseourl ident='oxdeliveryinfo' type='oxcontent'}]" class="btn btn-success" type="button" rel="nofollow">
                                                [{oxmultilang ident="ONLY_LEFT_FOR_FREE_SHIPPING"}] <span class="badge">[{$Restkaufwert}]</span>
                                            </a>
                                            [{*}]
                                                <a href="[{oxgetseourl ident='oxdeliveryinfo' type='oxcontent'}]" class="btn btn-success" type="button" rel="nofollow">
                                                    [{oxmultilang ident="LOOK_AT_OUR_SALES"}] <span class="badge">Angebote</span>
                                                </a>
                                            [{*}]
                                        </div>
                                    [{/strip}]
                                [{/if}]
                            [{/if}]
                        </div>

                        <div class="col-md-4">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <a href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=basket"}]" class="btn btn-default pull-right" style="margin-right:15px;">
                                <i class="fa fa-shopping-cart"></i> [{oxmultilang ident="DISPLAY_BASKET"}]
                            </a>
                        </div>
                    [{/block}]
                </div>
            </div>
        </div>
    [{/oxhasrights}]
[{/if}]