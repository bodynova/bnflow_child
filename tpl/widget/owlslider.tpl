[{block name="dd_widget_promoslider"}]
	[{assign var="oBanners" value=$oView->getBanners()}]
	[{assign var="currency" value=$oView->getActCurrency()}]
	[{if $oBanners|@count}]
		[{assign var="bannerAnzahl" value=$oBanners|count}]
		[{*oxscript add="$('#multi-item-example').carousel({
			interval : 2000,
			pause : null,
			wrap : true
		});"*}]
		<div class="owl-carousel owl-theme">

			[{assign var="col" value="left"}]
			[{assign var="iBannActive" value=1}]
			[{block name="dd_widget_promoslider_list"}]
				[{foreach from=$oBanners key="iPicNr" item="oBanner" name="promoslider"}]
						[{assign var="oArticle" value=$oBanner->getBannerArticle()}]
						[{assign var="sBannerPictureUrl" value=$oBanner->getBannerPictureUrl()}]
						[{assign var="sBannerLink" value=$oBanner->getBannerLink()}]

						[{if $sBannerPictureUrl}]
							[{assign var="sBannerLink" value=$oBanner->getBannerLink()}]

							[{* Link hinter Banner? *}]
							[{if $sBannerLink}]
								<a href="[{$sBannerLink}]" title="[{$oBanner->oxactions__oxtitle->value}]">
							[{/if}]

							[{* Banner Bild *}]
							<div class="owl-banner-box" style="background-image: url('[{$sBannerPictureUrl}]'); background-repeat:no-repeat;background-position:center;background-size: cover;" >

										[{$oBanner->oxactions__oxhtml->value|html_entity_decode}]

							</div>

							[{if $sBannerLink}]
								</a>
							[{/if}]

							[{* Banner Body *}]
							[{*if $oViewConf->getViewThemeParam('blSliderShowImageCaption') && $oArticle}]
								<div class="carousel-caption">
									[{* Link hinter Titel }]
									[{if $sBannerLink}]
									<a href="[{$sBannerLink}]"
									   title="[{$oBanner->oxactions__oxtitle->value}]">
										[{/if}]

										[{* Banner Titel }]
										<h4 class="card-title">
											[{$oArticle->oxarticles__oxtitle->value}]
										</h4>

										<p>
											[{if $oArticle->oxarticles__oxshortdesc->value|trim}]
												[{* Banner Short Description }]
												<span class="shortdesc">
												[{$oArticle->oxarticles__oxshortdesc->value|trim}]
											</span>
											[{/if}]
										</p>
										[{if $sBannerLink}]
									</a>
									[{/if}]
								</div>
							[{/if*}]
							[{*}]</div>[{*}]
							[{if $iBannActive > 0}]
								<!--/First slide-->
								[{*}]</div>[{*}]
								[{math equation="x - y" x=$iBannActive y=1 assign="iBannActive"}]
								[{*else*}]
								[{*}]</div>[{*}]
							[{/if}]
						[{/if}]
				[{/foreach}]
			[{/block}]
		</div>
	[{/if}]
[{/block}]