[{if $product->oxarticles__bnflagbestand->value == 2}]
	[{if $button == "yes"}]
		<button href="[{$_productLink}]" class="btn btn-default no-border">
	[{/if}]
	<span class="stockFlag notOnStock pull-left">
		<i class="fa fa-square" style="color:red"></i>
		[{if $Product->oxarticles__oxnostocktext->value}]
			<link itemprop="availability" href="http://schema.org/OutOfStock"/>
			[{$Product->oxarticles__oxnostocktext->value}]
		[{elseif $oViewConf->getStockOffDefaultMessage()}]
			<link itemprop="availability" href="http://schema.org/OutOfStock"/>
			[{oxmultilang ident="MESSAGE_NOT_ON_STOCK"}]
		[{/if}]
		[{if $product->getDeliveryDate()}]
			<link itemprop="availability" href="http://schema.org/PreOrder"/>
			[{* Oxid Bug Datumsformat: *}]
            [{*$product->getDeliveryDate()*}]
            [{*$product->oxarticles__oxdelivery->value|date_format:"%d.%m.%Y"*}]
            [{if $button == "yes"}]
                [{oxmultilang ident="AVAILABLE_ON" args=$product->oxarticles__oxdelivery->value|date_format:"%d.%m.%Y"}]
	        [{else}]
                [{oxmultilang ident="AVAILABLE_ON_NOT_BUYABLE" args=$product->oxarticles__oxdelivery->value|date_format:"%d.%m.%Y"}]
	        [{/if}]
		[{/if}]
	</span>
	[{if $button == "yes"}]
		</button>
	[{/if}]
[{elseif $product->oxarticles__bnflagbestand->value == 1}]
	[{if $button == "yes"}]
		<a href="[{$_productLink}]" class="btn btn-default no-border">
	[{/if}]
	<link itemprop="availability" href="http://schema.org/InStock"/>
	<span class="stockFlag lowStock">
        <i class="fa fa-square" style="color:yellow"></i>
            [{oxmultilang ident="LOW_STOCK"}]
	</span>
	[{if $button == "yes"}]
		</a>
	[{/if}]
[{elseif $product->oxarticles__bnflagbestand->value == 0}]
	[{if $button == "yes"}]
		<a href="[{$_productLink}]" class="btn btn-default no-border">
	[{/if}]
	<link itemprop="availability " href="http://schema.org/InStock"/>
	<i class="fa fa-square" style="color:forestgreen"></i>
	[{if $product->oxarticles__oxstocktext->value}]
		[{$product->oxarticles__oxstocktext->value}]
	[{elseif $oViewConf->getStockOnDefaultMessage()}]
		[{oxmultilang ident="READY_FOR_SHIPPING"}]
	[{/if}]
	[{if $button == "yes"}]
		</a>
	[{/if}]
[{/if}]