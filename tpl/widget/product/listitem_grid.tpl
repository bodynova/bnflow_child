[{block name="widget_product_listitem_grid"}]

    [{assign var="product"         value=$oView->getProduct()}]
    [{assign var="blDisableToCart" value=$oView->getDisableToCart()}]
    [{assign var="iIndex"          value=$oView->getIndex()}]
    [{assign var="showMainLink"    value=$oView->getShowMainLink()}]
    [{assign var="currency" value=$oView->getActCurrency()}]
    [{*if $showMainLink}]
        [{assign var='_productLink' value=$product->getMainLink()}]
    [{else}]
        [{assign var='_productLink' value=$product->getLink()}]
    [{/if*}]
    [{* Hier der Zwang immer die SeoUrl zu verwenden, man landet aus dem Suchergebnis in der Kategorie, zwangsläufig... *}]
    [{assign var='_productLink' value=$product->getBaseSeoLink($oViewConf->getActLanguageId(),true)}]
    [{assign var="aVariantSelections" value=$product->getVariantSelections(null,null,1)}]
    [{assign var="blShowToBasket" value=true}] [{* tobasket or more info ? *}]
    [{if $blDisableToCart || $product->isNotBuyable() || ($aVariantSelections&&$aVariantSelections.selections) || $product->hasMdVariants() || ($oViewConf->showSelectListsInList() && $product->getSelections(1)) || $product->getVariants()}]
        [{assign var="blShowToBasket" value=false}]
    [{/if}]

    <form name="tobasket[{$testid}]" [{if $blShowToBasket}]action="[{$oViewConf->getSelfActionLink()}]" method="post"[{else}]action="[{$_productLink}]" method="get"[{/if}]>
        <div class="hidden">
            [{$oViewConf->getNavFormParams()}]
            [{$oViewConf->getHiddenSid()}]
            <input type="hidden" name="pgNr" value="[{$oView->getActPage()}]">
            [{if $recommid}]
                <input type="hidden" name="recommid" value="[{$recommid}]">
            [{/if}]
            [{if $blShowToBasket}]
	            [{oxhasrights ident="TOBASKET"}]
	                <input type="hidden" name="cl" value="[{$oViewConf->getTopActiveClassName()}]">
	                [{if $owishid}]
	                    <input type="hidden" name="owishid" value="[{$owishid}]">
	                [{/if}]
	                [{if $toBasketFunction}]
	                    <input type="hidden" name="fnc" value="[{$toBasketFunction}]">
	                [{else}]
	                    <input type="hidden" name="fnc" value="tobasket">
	                [{/if}]
	                <input type="hidden" name="aid" value="[{$product->oxarticles__oxid->value}]">
	                [{if $altproduct}]
	                    <input type="hidden" name="anid" value="[{$altproduct}]">
	                [{else}]
	                    <input type="hidden" name="anid" value="[{$product->oxarticles__oxnid->value}]">
	                [{/if}]
	                <input type="hidden" name="am" value="1">
	            [{/oxhasrights}]
            [{else}]
                <input type="hidden" name="cl" value="details">
                <input type="hidden" name="anid" value="[{$product->oxarticles__oxnid->value}]">
            [{/if}]
        </div>
	    [{oxscript add="
                $(document).ready(function(){


					$('.productData').mouseover(function(e){
						e.preventDefault();

						$(this).find('.text-shortdesc > .short').addClass('hide');
						$(this).find('.text-shortdesc > .long').removeClass('hide');

					}).mouseout(function(e){

						$(this).find('.text-shortdesc > .short').removeClass('hide');
						$(this).find('.text-shortdesc > .long').addClass('hide');
					});

                });
            "}]

        <div class="item_wrapper" style="height: 490px">
	        [{*}]
	        <div class="product_miniDisplay hidden-md hidden-lg">
		        [{include file="widget/product/productbox_mini.tpl"}]
	        </div>
	        [{*}]
            <div class="product_short" style="min-height:490px; max-height: max-content">
                [{include file="widget/product/productbox_short.tpl"}]
            </div>
	        [{*}]
            <div class="product_full hidden-xs hidden-sm hide" style="min-height: 450px">
                [{include file="widget/product/productbox_full.tpl"}]
            </div>
	        [{*}]
        </div>
    </form>
[{/block}]
