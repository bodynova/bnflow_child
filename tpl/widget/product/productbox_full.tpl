<div style="border: 1px solid #E7E7E7; position: absolute;width: 110%;z-index: 2; margin: -17px; background-color: white; padding-bottom: 100px">
	[{block name="widget_product_listitem_infogrid_gridpicture"}]

		[{if $product->oxarticles__flagangebotfahne->value == 1}]
			<div class="ribbon" style="left: 0">
				<a href="[{$_productLink}]"
				   title="[{$product->oxarticles__oxtitle->value}] [{$product->oxarticles__oxvarselect->value}]"><span>[{$product->oxarticles__angebotfahnetext->value}]</span></a>
			</div>
		[{/if}]

		<div class="picture text-center pictureBox">
			<a href="[{$_productLink}]"
			   title="[{$product->oxarticles__oxtitle->value}] [{$product->oxarticles__oxvarselect->value}]"
			   class="img-wrap">
				<div class="js-fillcolor">
					<img src="[{$product->getThumbnailUrl()}]"
					     alt="[{$product->oxarticles__oxtitle->value}] [{$product->oxarticles__oxvarselect->value}]"
					     class="img-responsive [{*}]chamelion[{*}]" loading="lazy">
					[{oxhasrights ident="SHOWARTICLEPRICE"}]
						[{assign var="oUnitPrice" value=$product->getUnitPrice()}]
						[{if $oUnitPrice}]
							<div class="unit-price-bild">
	                            <span id="productPricePerUnit_[{$testid}]" class="pricePerUnit">
	                                [{$product->oxarticles__oxunitquantity->value}] [{$product->getUnitName()}] | [{oxprice price=$oUnitPrice currency=$currency}]/[{$product->getUnitName()}]
	                            </span>
							</div>
						[{/if}]
						[{*elseif $product->oxarticles__oxweight->value}]
							<div class="unit-price-bild">
	                            <span id="productPricePerUnit_[{$testid}]" class="pricePerUnit">
	                                <span title="weight">
		                                [{oxmultilang ident="WEIGHT"}]
	                                </span>
	                                <span class="value">
		                                [{$product->oxarticles__oxweight->value}] [{oxmultilang ident="KG"}]
	                                </span>
	                            </span>
							</div>
						[{/if*}]
					[{/oxhasrights}]
				</div>
				[{include file="widget/product/variantfarbliste.tpl"}]
			</a>
		</div>
	[{/block}]

	<div class="listDetails">
		[{block name="widget_product_listitem_infogrid_titlebox"}]
			<div class="title" style="margin-left:auto;margin-right: auto; max-width: 90%">
				<a id="[{$testid}]" href="[{$_productLink}]" class="title text-left"
				   title="[{$product->oxarticles__oxtitle->value}] [{$product->oxarticles__oxvarselect->value}]">
	                <span class="text-left">
	                    [{$product->oxarticles__oxtitle->value}] [{$product->oxarticles__oxvarselect->value}]
	                </span>
				</a>
			</div>
		[{/block}]

		<div class="descblock" style="margin-right: auto;margin-left: auto;max-width: 90%;height:auto">
			<div class="text-shortdesc">
				<div class="full">
					<span class="text-left">[{$product->oxarticles__oxshortdesc->value}]</span>
				</div>
				<div class="content text-left" style="height:15px">
					[{block name="widget_product_listitem_grid_price"}]
						[{oxhasrights ident="SHOWARTICLEPRICE"}]
							[{assign var="oUnitPrice" value=$product->getUnitPrice()}]
							[{assign var="tprice"     value=$product->getTPrice()}]
							[{assign var="price"      value=$product->getPrice()}]
							[{if $tprice && $tprice->getBruttoPrice() > $price->getBruttoPrice()}]
								<span class="oldPrice text-muted">
	                                <del>[{$product->getFTPrice()}] [{$currency->sign}]</del>
	                            </span>
							[{/if}]
						[{/oxhasrights}]
					[{/block}]
				</div>
			</div>
		</div>


		<div class="price" style="margin-right: auto;margin-left: auto;max-width: 90%; margin-bottom:0">
			[{block name="widget_product_listitem_grid_price_value"}]
				[{if $product->getFPrice()}]
					<span class="lead text-nowrap">
                        [{if $product->isRangePrice()}]
                            [{oxmultilang ident="PRICE_FROM"}]
                            [{if !$product->isParentNotBuyable()}]
                                [{$product->getFMinPrice()}]
                            [{else}]
                                [{$product->getFVarMinPrice()}]
                            [{/if}]
                        [{else}]
                            [{if !$product->isParentNotBuyable()}]
                                [{$product->getFPrice()}]
                            [{else}]
                                [{$product->getFVarMinPrice()}]
                            [{/if}]
                        [{/if}]
						[{$currency->sign}]
						[{if $oView->isVatIncluded()}]
							[{if !($product->hasMdVariants() || ($oViewConf->showSelectListsInList() && $product->getSelections(1)) || $product->getVariants())}]*[{/if}]
						[{/if}]
                    </span>
				[{/if}]
			[{/block}]

			<div class="pull-right">
				[{if $product->getFPrice()}]
					[{if $blShowToBasket}]
						[{oxhasrights ident="TOBASKET"}]
							<div class="btn-group">
								<button type="submit" class="btn btn-default hasTooltip no-border"
								        data-placement="bottom"
								        title="[{oxmultilang ident="TO_CART"}]">
									<i class="fal fa-shopping-cart"></i>
								</button>
								[{if $product->oxarticles__bnflagbestand->value == 2}]
									<button href="[{$_productLink}]" class="btn btn-default no-border">
										<i class="fa fa-square" style="color:red"></i>
										[{*}]
										<span class="stockFlag notOnStock pull-right">
													<i class="fa fa-square" style="color:red"></i>
													[{*if $Product->oxarticles__oxnostocktext->value}]
														<link itemprop="availability" href="http://schema.org/OutOfStock"/>
														[{$Product->oxarticles__oxnostocktext->value}]
													[{elseif $oViewConf->getStockOffDefaultMessage()}]
														<link itemprop="availability" href="http://schema.org/OutOfStock"/>
														[{oxmultilang ident="MESSAGE_NOT_ON_STOCK"}]
													[{/if*}]
										[{*if $Product->getDeliveryDate()}]
											<link itemprop="availability" href="http://schema.org/PreOrder"/>
											[{oxmultilang ident="AVAILABLE_ON"}] [{$oDetailsProduct->getDeliveryDate()}]
										[{/if}]
                                            </span>
									*}]
									</button>
								[{elseif $product->oxarticles__bnflagbestand->value == 1}]
									<a href="[{$_productLink}]" class="btn btn-default no-border">

										<link itemprop="availability" href="http://schema.org/InStock"/>
										<span class="stockFlag lowStock">
	                                    <i class="fa fa-square" style="color:yellow"></i>
	                                                    [{*oxmultilang ident="LOW_STOCK"*}]
	                                </span>
									</a>
								[{elseif $product->oxarticles__bnflagbestand->value == 0}]
									<a href="[{$_productLink}]" class="btn btn-default no-border">
										<link itemprop="availability" href="http://schema.org/InStock"/>
										<i class="fa fa-square" style="color:forestgreen"></i>
										[{*if $Product->oxarticles__oxstocktext->value}]
											[{$Product->oxarticles__oxstocktext->value}]
										[{elseif $oViewConf->getStockOnDefaultMessage()}]
											[{oxmultilang ident="READY_FOR_SHIPPING"}]
										[{/if*}]
									</a>
								[{/if}]
							</div>
						[{/oxhasrights}]
					[{else}]
						<button class="btn btn-xs btn-default hasTooltip pull-right no-border"
						        href="[{$_productLink}]">[{*oxmultilang ident="MORE_INFO"*}]
							<i class="fal fa-info" style="font-size: 20px"></i>
						</button>
					[{/if}]
					[{*if $blShowToBasket}]
						[{oxhasrights ident="TOBASKET"}]
							<button type="submit" class="btn btn-default hasTooltip" data-placement="bottom"
									title="[{oxmultilang ident="TO_CART"}]">
								<i class="fa fa-shopping-cart"></i>
							</button>
						[{/oxhasrights}]
						<a class="btn btn-primary"
						   href="[{$_productLink}]">[{oxmultilang ident="MORE_INFO"}]</a>
					[{else}]
						<a class="btn btn-primary"
						   href="[{$_productLink}]">[{oxmultilang ident="MORE_INFO"}]</a>
					[{/if*}]
					[{*if !$Product == null && $Product->isBuyable()*}]

					[{*/if*}]
					[{*$product->oxarticles__bnflagbestand->value|var_dump*}]
				[{/if}]
			</div>
		</div>
		[{*}]
			<div class="content" >
				[{if $oUnitPrice}]
					<span id="productPricePerUnit_[{$testid}]" class="pricePerUnit text-right">
						[{$product->oxarticles__oxunitquantity->value}] [{$product->getUnitName()}] | [{oxprice price=$oUnitPrice currency=$currency}]/[{$product->getUnitName()}]
					</span>
				[{elseif $product->oxarticles__oxweight->value }]
					<span id="productPricePerUnit_[{$testid}]" class="pricePerUnit text-right">
						<span title="weight">[{oxmultilang ident="WEIGHT"}]</span>
						<span class="value">[{$product->oxarticles__oxweight->value}] [{oxmultilang ident="KG"}]</span>
					</span>
				[{/if}]
			</div>
		[{*}]
		[{*block name="widget_product_listitem_grid_tobasket"}]
			<div class="actions text-center">
				<div class="btn-group">
					[{if $blShowToBasket}]
						[{oxhasrights ident="TOBASKET"}]
							<button type="submit" class="btn btn-default hasTooltip" data-placement="bottom" title="[{oxmultilang ident="TO_CART"}]">
								<i class="fal fa-shopping-cart"></i>
							</button>
						[{/oxhasrights}]
						<a class="btn btn-primary" href="[{$_productLink}]" >[{oxmultilang ident="MORE_INFO"}]</a>
					[{else}]
						<a class="btn btn-primary" href="[{$_productLink}]" >[{oxmultilang ident="MORE_INFO"}]</a>
					[{/if}]
				</div>
			</div>
		[{/block*}]
	</div>
</div>
