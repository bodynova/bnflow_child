[{assign var="oSelections" value=$oSelectionList->getSelections()}]
[{assign var="showTable" value=false}]
[{assign var="seoparent" value=$oDetailsProduct}]
[{if $seoparent}]
    [{if $seoparent->getParentArticle()}]
        [{assign var="seoparent" value=$seoparent->getParentArticle()}]
    [{/if}]
    [{assign var="simpleVariants" value=$seoparent->getSimpleVariants()}]
[{/if}]

[{assign var="oActiveSelection" value=$oSelectionList->getActiveSelection()}]

[{if $oSelections}]
    [{*if $showTable && ($oSelectionList->getLabel() eq 'Farbe' || $oSelectionList->getLabel() eq 'Color' || $oSelectionList->getLabel() eq 'Couleur')}]
        [{* Select List Tabelle TEST }]
		<div class="container-fluid">
			<div class="row">
				<div class="panel panel-default">
					<div class="panel-heading">
						<b>[{oxmultilang ident="CHOOSE_VARIANT"}] [{$oSelectionList->getLabel()}][{oxmultilang ident="COLON"}]</b>
					</div>
					<div class="panel-body">
                        [{foreach from=$simpleVariants item="seovariant" }]
							<div class="col-xs-4 col-md-3 col-lg-2">
								<a href="[{$seovariant->getLink()}]">
									<img src="[{$seovariant->getIconUrl()}]" class="img-thumbnail"
									     alt="[{$seovariant->oxarticles__oxtitle->value}] [{$seovariant->oxarticles__oxvarselect->value}]"
									     width="75px"/>
									<p style="width: 75px">
                                        [{if $oActiveSelection && ($oActiveSelection->getName()|trim eq $seovariant->oxarticles__oxvarselect->value|trim)}]
											<strong>[{$seovariant->oxarticles__oxvarselect->value|oxtruncate:20:"..."}]</strong>
                                        [{else}]
                                            [{$seovariant->oxarticles__oxvarselect->value|oxtruncate:20:"..."}]
                                        [{/if}]
									</p>
								</a>
							</div>
                        [{/foreach}]
					</div>
				</div>
			</div>
		</div>
    [{else*}]
		<div class="selectbox dropDown">
            [{if !$blHideLabel}]
				<p class="variant-label">
					<strong>[{$oSelectionList->getLabel()}][{oxmultilang ident="COLON"}]</strong>
				</p>
            [{/if}]
			<div class="dropdown-wrapper">
				<button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                    [{if $oActiveSelection}]
						<span class="pull-left">[{$oActiveSelection->getName()}]</span>
                    [{elseif !$blHideDefault}]
						<span class="pull-left">
	                    [{if $sFieldName == "sel"}]
                            [{oxmultilang ident="PLEASE_CHOOSE"}]
                        [{else}]
                            [{*$oSelectionList->getLabel()*}] [{oxmultilang ident="CHOOSE_VARIANT"}]
                        [{/if}]
	                </span>
                    [{/if}]
					<i class="fa fa-angle-down pull-right"></i>
				</button>
                [{if $editable !== false}]
					<input type="hidden" name="[{$sFieldName|default:"varselid"}][[{$iKey}]]"
					       value="[{if $oActiveSelection}][{$oActiveSelection->getValue()}][{/if}]">
					<ul class="dropdown-menu [{$sJsAction}][{if $sFieldName != "sel"}] vardrop[{/if}]" role="menu">
                        [{if $oActiveSelection && !$blHideDefault}]
							<li>
								<a href="#" rel="">
                                    [{if $sFieldName == "sel"}]
                                        [{oxmultilang ident="PLEASE_CHOOSE"}]
                                    [{else}]
                                        [{oxmultilang ident="CHOOSE_VARIANT"}]
                                    [{/if}]
								</a>
							</li>
                        [{/if}]
                        [{*[{foreach from=$oSelections item=oSelection}]
								<li class="[{if $oSelection->isDisabled()}]disabled js-disabled[{/if}]">

									 ToDo: Farbboxen anzeigen in dem Varianten Dropdown Menue *}]
                        [{*if $oSelectionList->getLabel() == 'Farbe' OR $oSelectionList->getLabel() == 'Color' OR $oSelectionList->getLabel() == 'Couleur'}]
										[{if $oDetailsProduct != null AND !empty($oDetailsProduct->getVariantHex())}]
											[{foreach from=$oDetailsProduct->getVariantHex() item="Variant"}]
												[{if $Variant->oxarticles__oxvarselect->value|strpos:$oSelection->getName() !== false}]
													[{if $oSelection->getName() == $Variant->oxarticles__oxvarselect->value}]
														<div style="overflow: auto">
															<div class="pull-left"
																 style="background-color: #[{$Variant->oxarticles__farbhex->value}];height:20px;width:20px;margin-left:15px;"></div>
															<a href="[{$oSelection->getLink()}]"
															   data-selection-id="[{$oSelection->getValue()}]"
															   class="[{if $oSelection->isActive()}]active[{/if}] pull-left"
															   style="margin-left: 15px">[{$oSelection->getName()}]</a>
														</div>
													[{*/if}]
												[{/if}]
											[{/foreach}]
										[{else}]
											<a href="[{$oSelection->getLink()}]"
											   data-selection-id="[{$oSelection->getValue()}]"
											   class="[{if $oSelection->isActive()}]active[{/if}]">[{$oSelection->getName()}]</a>
										[{/if}]
									[{else}]
										<a href="[{$oSelection->getLink()}]"
										   data-selection-id="[{$oSelection->getValue()}]"
										   class="[{if $oSelection->isActive()}]active[{/if}]">[{$oSelection->getName()}]</a>
									[{*/if
								</li>
							[{/foreach}]*}]
                        [{*foreach from=$seoparent->getSimpleVariants() item="seovariant" }]
								<li class="[{*if $seovariant->isDisabled()}]disabled js-disabled[{/if}]">
									<a href="[{$seovariant->getLink()}]" data-selection-id="[{$seovariant->getValue()}]"
									   class="[{if $seovariant->isActive()}]active[{/if}]">[{$seovariant->getName()}]
									</a>
								</li>
							[{/foreach*}]
                        [{*$oSelections|var_dump*}]

                        [{foreach from=$oSelections item=oSelection}]
							<li class="[{if $oSelection->isDisabled()}]disabled js-disabled[{/if}]">
								<a href="[{$oSelection->getLink()}]" data-selection-id="[{$oSelection->getValue()}]"
								   class="[{if $oSelection->isActive()}]active[{/if}]">[{$oSelection->getName()}]</a>
							</li>
                        [{/foreach}]
					</ul>
                [{/if}]
			</div>
		</div>
    [{*/if*}]
[{/if}]
