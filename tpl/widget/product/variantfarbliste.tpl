<div id="VariantHex" style="[{if $oView->getClassName() == "oxwarticledetails"}]width:50%;[{/if}]/*display: flex;flex-direction: column;flex-wrap: nowrap;justify-content: flex-start;align-items: stretch;align-content: stretch;*/z-index: 100;">
	[{if $product->oxarticles__oxvarcount->value > 1}]
		<div style="display: flex;padding-left:15px;padding-right:15px;justify-content: space-between;">
			[{foreach from=$product->getVariantHex() item="variante"}]
				<div style="background-color:#[{$variante->oxarticles__farbhex->value}];width:30px;height: 30px;max-width: 30px"></div>
			[{/foreach}]
		</div>
	[{else}]
		<div class="pull-left" style="height:30px"></div>
	[{/if}]
</div>