[{block name="dd_widget_promoslider"}]
    [{assign var="oBanners" value=$oView->getBanners()}]
    [{assign var="currency" value=$oView->getActCurrency()}]
    [{if $oBanners|@count}]
        [{assign var="bannerAnzahl" value=$oBanners|count}]
        [{*oxscript add="$('#multi-item-example').carousel({
            interval : 2000,
            pause : null,
            wrap : true
        });"*}]


                <!--Carousel Wrapper-->
                [{*}]<div id="multi-item-example" class="carousel slide multi-item-carousel xs-items-1 sm-items-1 md-items-3 lg-items-3">[{*}]
                <div class="stage-wrapper stage-slider clearfix carousel slide" data-ride="carousel">

                    <!-- Indicators -->
                    [{*}]
                    <ol class="carousel-indicators">
                        <li data-target="#multi-item-example" data-slide-to="0" class="active"></li>
                        <li data-target="#multi-item-example" data-slide-to="1"></li>
                        <li data-target="#multi-item-example" data-slide-to="2"></li>
                    </ol>
                    [{*}]
                <div class="slide-outer-wrap">
                    <!--Slides-->
                    <div class="slide-inner-wrap carousel-inner" style="width: 500%;left:-100%">
                        [{*}]<div class="carousel-inner" role="listbox">[{*}]
                        [{assign var="col" value="left"}]
                        [{assign var="iBannActive" value=1}]
                        [{block name="dd_widget_promoslider_list"}]
                            [{foreach from=$oBanners key="iPicNr" item="oBanner" name="promoslider"}]
                                <div class="slide-element item [{if $iBannActive > 0}]active[{/if}]" style="float: left; width: 20%" role="listbox">
                                    [{assign var="oArticle" value=$oBanner->getBannerArticle()}]
                                    [{assign var="sBannerPictureUrl" value=$oBanner->getBannerPictureUrl()}]
                                    [{assign var="sBannerLink" value=$oBanner->getBannerLink()}]

                                    [{if $sBannerPictureUrl}]

                                        [{*if $iBannActive > 0}]
                                            <div class="item active">
                                        [{else}]
                                            <div class="item">
                                        [{/if*}]


                                        [{* Links *}]
                                        [{*}]<div class="">[{*}]

                                            [{assign var="sBannerLink" value=$oBanner->getBannerLink()}]

                                            [{* Link hinter Banner? *}]
                                            [{if $sBannerLink}]
                                            <a href="[{$sBannerLink}]" title="[{$oBanner->oxactions__oxtitle->value}]">
                                                [{/if}]

                                                [{* Banner Bild *}]
                                                <div class="simple-stage main-stage set-background" style="background-image: url('[{$sBannerPictureUrl}]');">
                                                </div>
                                                [{*}]
											<img src="[{$sBannerPictureUrl}]" alt="[{$oBanner->oxactions__oxtitle->value}]" title="[{$oBanner->oxactions__oxtitle->value}]" class="img-responsive">
												[{*}]
                                                [{if $sBannerLink}]
                                            </a>
                                            [{/if}]

                                            [{* Banner Body *}]
                                            [{*if $oViewConf->getViewThemeParam('blSliderShowImageCaption') && $oArticle}]
                                                <div class="carousel-caption">
                                                    [{* Link hinter Titel }]
                                                    [{if $sBannerLink}]
                                                    <a href="[{$sBannerLink}]"
                                                       title="[{$oBanner->oxactions__oxtitle->value}]">
                                                        [{/if}]

                                                        [{* Banner Titel }]
                                                        <h4 class="card-title">
                                                            [{$oArticle->oxarticles__oxtitle->value}]
                                                        </h4>

                                                        <p>
                                                            [{if $oArticle->oxarticles__oxshortdesc->value|trim}]
                                                                [{* Banner Short Description }]
                                                                <span class="shortdesc">
                                                                [{$oArticle->oxarticles__oxshortdesc->value|trim}]
                                                            </span>
                                                            [{/if}]
                                                        </p>
                                                        [{if $sBannerLink}]
                                                    </a>
                                                    [{/if}]
                                                </div>
                                            [{/if*}]
                                        [{*}]</div>[{*}]
                                        [{if $iBannActive > 0}]
                                            <!--/First slide-->
                                            [{*}]</div>[{*}]
                                            [{math equation="x - y" x=$iBannActive y=1 assign="iBannActive"}]
                                        [{*else*}]
                                            [{*}]</div>[{*}]
                                        [{/if}]
                                    [{/if}]
                                </div>

                            [{/foreach}]
                        [{/block}]
                    </div>
                </div>


                    [{* Controls *}]
                    [{*}]
                    <a class="left carousel-control" href="#sample1" data-slide="prev">
                        <i class="glyphicon glyphicon-chevron-left"></i>
                    </a>
                    <a class="right carousel-control" href="#sample1" data-slide="next">
                        <i class="glyphicon glyphicon-chevron-right"></i>
                    </a>
                    [{*}]

                    <!-- Controls -->
                    <a class="left carousel-control" href="#multi-item-example" role="button" data-slide="prev">
                        <i class="fa fa-chevron-left fa-4x"></i>
                        [{*}]
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        [{*}]
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#multi-item-example" role="button" data-slide="next">
                        <i class="fa fa-chevron-right fa-4x"></i>
                        [{*}]
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        [{*}]
                        <span class="sr-only">Next</span>
                    </a>

                </div>
    [{/if}]
[{/block}]