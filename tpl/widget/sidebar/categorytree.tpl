[{block name="dd_widget_sidebar_categorytree"}]
    [{if $oxcmp_categories}]

	    [{if $categories != null}]
            [{assign var="categories" value=$oxcmp_categories->getClickRoot()}]
		[{else}]
		    [{assign var="categories" value=$oxcmp_categories->getArray()}]
		[{/if}]

	    [{assign var="oxcmp_categories" value=$oxcmp_categories}]

        [{assign var="act" value=$oxcmp_categories->getClickCat()}]
	    [{assign var="actint" value=$act->oxcategories__welt->value|intval}]

	    [{assign var="weltstring" value='welt'|cat:$act->oxcategories__welt->value}]
	    [{*$weltstring*}]

[{* WeltenÜberschrift *}]
	    <div class="page-header h4" style="padding-left: 10px;padding-top: 2px;padding-bottom: 10px;">
		    <div class="pull-right visible-xs visible-sm">
			    <i class="fa fa-caret-down toggleTree"></i>
		    </div>
		    [{oxifcontent ident=$weltstring object="oCont"}]
		        [{$oCont->oxcontents__oxtitle->value}]
		    [{/oxifcontent}]
		</div>

	    [{assign var="oCat" value=$oxcmp_categories->dre_loadWeltList($actint)}]
	    [{assign var="welt" value=$oxcmp_categories->getWelt()}]
	    [{if $oViewConf->getTopActionClassName() == 'start'}]
		    [{assign var="homeSelected" value="true"}]
	    [{/if}]
	    [{assign var='actCatId' value=$oViewConf->getActCatId()}]


	    <ul id="sidebartree" style="list-style: none;padding-left: 0;white-space:normal;">
		    [{foreach from=$oxcmp_categories item="ocat" key="catkey" name="root"}]
			    [{if $ocat->oxcategories__welt->value == $welt}]
				    [{assign var='active' value='true'}]
				[{else}]
				    [{assign var='active' value='false'}]
				[{/if}]
			    [{if $ocat->getIsVisible()}]
				    [{* CMS - Kategorien Schleife *}]
				    [{*foreach from=$ocat->getContentCats() item="oTopCont" name="MoreTopCms"}]
					    <li>
						    <a href="[{$oTopCont->getLink()}]">
							    [{$oTopCont->oxcontents__oxtitle->value}]
						    </a>
					    </li>
				    [{/foreach*}]
[{* Erste Ebene *}]
				    <li class="[{if $ocat->getSubCats()}]has-sub[{/if}]">
					    <div class="btn-group" style="width: 100%;">
						    [{* Titel *}]
						    <a style="padding:1px 7px; white-space:normal;max-width:120px;text-align: left;"
						       class="btn  [{if $oxcmp_categories->dre_getActive($ocat->oxcategories__oxid->value) == true}]bold[{/if}][{if $oViewConf->getActCatId() == $ocat->oxcategories__oxid->value}] active[{/if}]"
						       href="[{$ocat->getLink()}]">
							    [{$ocat->oxcategories__oxtitle->value|html_entity_decode}]
						    </a>
			                [{if $ocat->getSubCats()}]
							    [{* +/- Button *}]
							    <a style="padding:1px 6px;cursor: pointer;font-size:14px;" class="btn pull-right no-hover"
							       onclick="$(this).parent().parent().children('ul').toggle();if($(this).children('i').hasClass('fa-plus')){$(this).children('i').removeClass('fa-plus').addClass('fa-minus');}else{$(this).children('i').removeClass('fa-minus').addClass('fa-plus')};">
								    [{if $oxcmp_categories->dre_getActive($ocat->oxcategories__oxid->value) == true}]
									    <i class="fa fa-minus"></i>[{else}]
									    <i class="fa fa-plus"></i>
								    [{/if}]
							    </a>
				            [{/if}]
					    </div>
			        [{if $ocat->getSubCats()}]
[{* Subcategorien *}]
					    <ul style="[{if $oxcmp_categories->dre_getActive($ocat->oxcategories__oxid->value) == true}]display:block;[{else}]display: none;[{/if}]list-style: none;padding-left: 15px">
						    [{foreach from=$ocat->getSubCats() item="osubcat" key="subcatkey" name="SubCat"}]
							    [{if $osubcat->getIsVisible()}]
								    [{* CMS - Kategorien Schleife *}]
								    [{* foreach from=$osubcat->getContentCats() item=ocont name=MoreCms}]
									    <li>
										    <a href="[{$ocont->getLink()}]">
											    [{$ocont->oxcontents__oxtitle->value}]
										    </a>
									    </li>
								    [{/foreach *}]
							    [{/if}]
[{* zweite Ebene *}]
								[{if $osubcat->getIsVisible()}]
								    <li class="[{if $osubcat->getSubCats()}]has-sub[{/if}]">
									    <div class="btn-group" style="width: 100%;">
										    [{* Titel *}]
										    <a style="padding:1px 7px;white-space:normal;max-width:110px;text-align: left;"
										       class="btn [{if $oxcmp_categories->dre_getActive($osubcat->oxcategories__oxid->value) == true}]bold[{/if}][{if $oViewConf->getActCatId() == $osubcat->oxcategories__oxid->value}] active[{/if}]"
										       href="[{$osubcat->getLink()}]">
											    [{$osubcat->oxcategories__oxtitle->value|html_entity_decode}]
										    </a>
										    [{if $osubcat->getSubCats()}]
											    [{* +/- Button *}]
											    <a style="padding:1px 6px;cursor: pointer;font-size:14px;"
											       class="pull-right no-hover"
											       onclick="$(this).parent().parent().children('ul').toggle();if($(this).children('i').hasClass('fa-plus')){$(this).children('i').removeClass('fa-plus').addClass('fa-minus');}else{$(this).children('i').removeClass('fa-minus').addClass('fa-plus')}">
												    [{if $oxcmp_categories->dre_getActive($osubcat->oxcategories__oxid->value) == true}]
													    <i class="fa fa-minus"></i>
												    [{else}]
													    <i class="fa fa-plus"></i>
												    [{/if}]
											    </a>
										    [{/if}]
										</div>
									    [{if $osubcat->getSubCats()}]
[{* Subcategorien *}]
										    <ul style="[{if $oxcmp_categories->dre_getActive($osubcat->oxcategories__oxid->value) == true}]display:block;[{else}]display: none;[{/if}]list-style: none;padding-left: 15px">
											    [{foreach from=$osubcat->getSubCats() item="oosubcat" key="subcatkey" name="SubCat"}]
												    [{if $oosubcat->getIsVisible()}]
													    [{* CMS - Kategorien Schleife *}]
													    [{* foreach from=$oosubcat->getContentCats() item=subocont name=MoreCms}]
														    <li>
															    <a href="[{$subocont->getLink()}]">[{$subocont->oxcontents__oxtitle->value}]</a>
														    </li>
													    [{/foreach *}]
												    [{/if}]
[{* dritte Ebene *}]
												    [{if $oosubcat->getIsVisible()}]
													    <li class="[{if $oosubcat->getSubCats()}]has-sub[{/if}]">
														    <div class="btn-group" style="width: 100%;">
															    [{* Titel *}]
														        <a style="padding:1px 7px;white-space:normal;max-width:100px;text-align: left;"
														           class="btn [{if $oxcmp_categories->dre_getActive($oosubcat->oxcategories__oxid->value) == true}]bold[{/if}][{if $oViewConf->getActCatId() == $oosubcat->oxcategories__oxid->value}] active[{/if}]"
														           href="[{$oosubcat->getLink()}]">
															        [{$oosubcat->oxcategories__oxtitle->value|html_entity_decode}]
														        </a>

															    [{if $oosubcat->getSubCats()}]
																    [{* +/- Button *}]
																    <a style="padding:0;cursor: pointer;font-size:14px;line-height:26px;"
																       class="pull-right no-hover"
																       onclick="$(this).parent().parent().children('ul').toggle();if($(this).children('i').hasClass('fa-plus')){$(this).children('i').removeClass('fa-plus').addClass('fa-minus');}else{$(this).children('i').removeClass('fa-minus').addClass('fa-plus')}">
																	    [{if $oxcmp_categories->dre_getActive($oosubcat->oxcategories__oxid->value) == true}]
																		    <i class="fa fa-minus"></i>
																	    [{else}]
																		    <i class="fa fa-plus"></i>
																	    [{/if}]
																    </a>
															    [{/if}]
														    </div>
															[{if $oosubcat->getSubCats()}]
[{* Subcategorien *}]
															    <ul style="[{if $oxcmp_categories->dre_getActive($oosubcat->oxcategories__oxid->value) == true}]display:block;[{else}]display: none;[{/if}]list-style: none;padding-left: 15px">
																    [{foreach from=$oosubcat->getSubCats() item="moosubcat" key="subcatkey" name="SubCat"}]
																	    [{if $moosubcat->getIsVisible()}]
																		    [{* CMS - Kategorien Schleife *}]
																		    [{* foreach from=$moosubcat->getContentCats() item=subocont name=MoreCms}]
																			    <li>
																				    <div style="border-left:1px solid">
																				        <a class="btn" href="[{$subocont->getLink()}]">
																					        [{$subocont->oxcontents__oxtitle->value}]
																				        </a>
																				    </div>
																			    </li>
																		    [{/foreach *}]
																	    [{/if}]
																		[{if $moosubcat->getIsVisible()}]
																		    <li>
																			    <div class="btn-group" style="width: 100%;">
																			        <a style="padding:1px 7px;white-space:normal;max-width:190px;text-align: left;"
																			           class="btn [{if $oxcmp_categories->dre_getActive($moosubcat->oxcategories__oxid->value) == true}]bold[{/if}][{if $oViewConf->getActCatId() == $moosubcat->oxcategories__oxid->value}] active[{/if}]"
																			           href="[{$moosubcat->getLink()}]">
																				        [{$moosubcat->oxcategories__oxtitle->value|html_entity_decode}]
																			        </a>
																			    </div>
																		    </li>
																	    [{/if}]
																    [{/foreach}]
															    </ul>
														    [{/if}]
													    </li>
													    [{*}]<hr class="hr-welt"/>[{*}]
												    [{/if}]
											    [{/foreach}]
										    </ul>
									    [{/if}]
								    </li>
								[{/if}]
						    [{/foreach}]
					    </ul>
				    [{/if}]
				    </li>
			    [{/if}]
		    [{/foreach}]
	    </ul>




	    [{*assign var="oCat" value=$oxcmp_categories->dre_loadWeltList($actint)*}]

	    [{*
				<div class="categoryBox hidden-xs hidden-sm">
					<ul class="nav nav-pills nav-stacked cat-tree">
						<li>[{$act->oxcategories__oxtitle->value}]

							<ul>
								[{foreach from=$act->getSubCats() item="subCat"}]
									<li>
										[{$subCat->oxcategories__oxtitle->value}]
									</li>
								[{/foreach}]
							</ul>
						</li>

					</ul>
				</div>
				if $categories}]
					[{assign var="deepLevel" value=$oView->getDeepLevel()}]
					<div class="categoryBox hidden-xs hidden-sm">
						[{block name="dd_widget_sidebar_categorytree_inner"}]
							<ol class="nav nav-pills nav-stacked cat-tree">
								[{block name="dd_widget_sidebar_categorytree_list"}]
									[{defun name="tree" categories=$categories}]
										[{assign var="deepLevel" value=$deepLevel+1}]
										[{assign var="oContentCat" value=$oView->getContentCategory()}]
										[{foreach from=$categories item="_cat"}]
											[{if $_cat->getIsVisible()}]
												[{* CMS category *}]
                                        [{*if $_cat->getContentCats() && $deepLevel > 1}]
                                            [{foreach from=$_cat->getContentCats() item="_oCont"}]
                                                <li class="[{if $oContentCat && $oContentCat->getId()==$_oCont->getId()}] active [{else}] end [{/if}]" >
                                                    <a href="[{$_oCont->getLink()}]" title="[{$_oCont->oxcontents__oxtitle->value}]"><i></i>[{$_oCont->oxcontents__oxtitle->value}]</a>
                                                </li>
                                            [{/foreach}]
                                        [{/if}]

                                        [{* subcategories }]
                                        [{assign var="oSubCats" value=$_cat->getSubCats()}]
                                        <li class="[{if !$oContentCat && $act && $act->getId()==$_cat->getId()}]active[{elseif $_cat->expanded}]exp[{/if}][{if !$_cat->hasVisibleSubCats}] end[{/if}]">
                                            <a href="[{$_cat->getLink()}]" title="[{$_cat->oxcategories__oxtitle->value}]">
                                                <i class="fa fa-caret-[{if $_cat->expanded && $oSubCats}]down[{else}]right[{/if}]"></i> [{$_cat->oxcategories__oxtitle->value}] [{if $oView->showCategoryArticlesCount() && ($_cat->getNrOfArticles() > 0)}] ([{$_cat->getNrOfArticles()}])[{/if}]
                                            </a>
                                            [{* $_cat->getSubCats()|var_dump }]
                                            [{*if $oSubCats && $_cat->expanded }]
                                            [{if $oSubCats && $act && $act->getId()==$_cat->getId()}]
                                                 <ul class="nav nav-pills nav-stacked">[{fun name="tree" categories=$oSubCats}]</ul>
                                            [{/if}]
                                        </li>
                                    [{/if}]
                                [{/foreach}]
                            [{/defun}]
                        [{/block}]
                    </ol>
                [{/block}]
            </div>
        [{/if*}]
    [{/if}]
[{/block}]


[{*}]
<div class="categoryBox">
    <ul class="tree" id="tree">
        [{assign var="level1counter" value="1"}]
        [{defun name="tree" categories=$categories}]
        [{assign var="deepLevel" value=$deepLevel+1}]
        [{assign var="oContentCat" value=$oView->getContentCategory() }]
        [{foreach from=$categories item=_cat}]
            [{if $deepLevel == 1}]
			  [{assign var="level1counter" value=$level1counter+1}]
			  [{if $categories|@count ==$level1counter}]
				[{assign var="lastelement" value="1"}]
			  [{/if}]
			[{/if}]
            [{if $_cat->getIsVisible() }]
                [{* CMS category }]
                [{if $_cat->getContentCats() && $deepLevel > 1 }]
                    [{foreach from=$_cat->getContentCats() item=_oCont}]
                        <li class="[{if $oContentCat && $oContentCat->getId()==$_oCont->getId() }] active [{else}] end [{/if}]">
                            <a href="[{$_oCont->getLink()}]">[{ $_oCont->oxcontents__oxtitle->value }]</a>
                        </li>
                    [{/foreach}]
                [{/if }]
                [{* subcategories }]
                [{if $_cat->oxcategories__oxtitle->value}]
                    <li class="[{if $level1counter}]first [{/if}][{if !$oContentCat && $act && $act->getId()==$_cat->getId() }]active[{elseif $_cat->expanded}]exp[{/if}][{if !$_cat->hasVisibleSubCats}] end[{/if}]">
                    [{assign var="indent" value=$deepLevel*8}]
                    <a href="[{$_cat->getLink()}]" [{if $_cat->expanded && $deepLevel == 1}]class="mainnav"[{/if}]>
                    [{$_cat->oxcategories__oxtitle->value}] [{ if $oView->showCategoryArticlesCount() && ($_cat->getNrOfArticles() > 0) }] ([{$_cat->getNrOfArticles()}])[{/if}]
                </a>
                [{if $_cat->getSubCats() && $_cat->expanded}]
                    <ul>[{fun name="tree" categories=$_cat->getSubCats() }]</ul>
                [{/if}]
                </li>
            [{/if}]
            [{assign var="level1counter" value="0"}]
        [{/if}]
        [{/foreach}]
        [{/defun}]
    </ul>
</div>
[{*}]
