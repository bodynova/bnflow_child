<div class="stage-wrapper stage-slider clearfix " data-require-module="stageslider, tooltips"
     data-autoslide="{&quot;autoslide&quot;: {&quot;enable&quot;: true, &quot;pauseOnHover&quot;: true, &quot;delay&quot;: 5000}}">
	<div class="slide-outer-wrap js_stage-slider">
		<div class="slide-inner-wrap js_slide-inner-wrap" style="width: 900%; left: -700%;">
			<div class="slide-element js_slide-element" style="float: left; width: 11.1111%;">
				<a href="/marken/is-not-enough/?itm_medium=startseitenbanner&amp;itm_source=globetrotter&amp;itm_campaign=isnotenough_neue_marke">
					<div class="simple-stage main-stage set-background "
					     data-background="background-image:url('/media/i/IsNotEnough_1100x450px_3-compressor-19708-0.jpg');">
        <span class="stage-content center-content bottom-content active-content">
            <span class="content-image-wrapper">
                <picture>
                  <img data-src="/media/i/IsNotEnough_Safe-Space_430x250-19709-0.png"
                       class="content-image js-unveil-list" alt="" title="">
                </picture>
              </span>
            <span class="btn" style="color:#ffffff;
                    border-color:#ffffff;
                    background-color: #5b5658;">
                Multifunktionshosen mit Top Preis-Leistungsverhältnis</span>
            </span>
					</div>
				</a>
			</div>
			<div class="slide-element js_slide-element" style="float: left; width: 11.1111%;">
				<a href="/newsletter/duffles/?itm_medium=startseitenbanner&amp;itm_source=globetrotter">
					<div class="simple-stage main-stage set-background "
					     style="background-image:url('/media/i/Duffles-1100x450-compressor%281%29-19822-0.jpg');">
        <span class="stage-content center-content bottom-content active-content">
            <canvas class="stage-content-image" width="650" height="48"></canvas>
            <span class="btn" style="color:#ffffff;
                    border-color:#ffffff;
                    background-color: rgba(14,15,15,0.3);">
                Extra viel Platz für deine Reiseausrüstung</span>
            </span>
					</div>
				</a>
			</div>
			<div class="slide-element js_slide-element" style="float: left; width: 11.1111%;">
				<a href="/aktionen/adventskalender/?itm_medium=startseitenbanner&amp;itm_source=globetrotter">
					<div class="simple-stage main-stage set-background "
					     style="background-image:url('/media/i/Adventskalender-compressor-9088-0.jpg');">
        <span class="stage-content center-content bottom-content active-content">
            <canvas class="stage-content-image" width="650" height="48"></canvas>
            <span class="btn" style="color:#ffffff;
                    border-color:#ffffff;
                    background-color: rgba(14,15,15,0.3);">
                Klaus hat hinter jedem Türchen ein besonderes Outdoor-Schnäppchen versteckt</span>
            </span>
					</div>
				</a>
			</div>
			<div class="slide-element js_slide-element" style="float: left; width: 11.1111%;">
				<a href="/aktionen/geschenke/?itm_medium=startseitenbanner&amp;itm_source=globetrotter&amp;itm_campaign=KLaus_Geschenkeseite_101218">
					<div class="simple-stage main-stage set-background "
					     style="background-image:url('/media/i/Kochen-mit-Klaus-und-Primus-compressor-19641-0.jpg');">
        <span class="stage-disrupter active-content" style="padding-top: 45.5px; padding-bottom: 45.5px;">
                <span class="paragraph-small">Geschenketipps<br>vom<br>Fachmann </span></span>
						<span class="stage-content center-content bottom-content active-content">
            <span class="btn" style="color:#ffffff;
                    border-color:#ffffff;
                    background-color: rgba(14,15,15,0.3);">
                Nicht nur für Outdoor-Köche: Empfehlungen und Geschenkideen von Klaus</span>
            </span>
					</div>
				</a>
			</div>
			<div class="slide-element js_slide-element" style="float: left; width: 11.1111%;">
				<a href="/newsletter/klaus-merinowaesche-zum-vorteilspreis/?itm_medium=startseitenbanner&amp;itm_source=globetrotter&amp;itm_campaign=waeschesets_weihnachten_031218">
					<div class="simple-stage main-stage set-background "
					     data-background="background-image:url('/media/i/Wa%CC%88sche-Aktion-compressor-19639-0.jpg');"
					     style="background-image:url('/media/i/Wa%CC%88sche-Aktion-compressor-19639-0.jpg');">
        <span class="stage-content center-content bottom-content active-content">
            <span class="content-image-wrapper">
                <picture>
                  <img data-src="/media/i/KW53_Wa%CC%88sche_Aktion_Safe-Space_430x250-compressor-19667-0.gif"
                       class="content-image js-unveil-list" alt="" title=""
                       src="/media/i/KW53_Wa%CC%88sche_Aktion_Safe-Space_430x250-compressor-19667-0.gif"
                       style="opacity: 1;">
                </picture>
              </span>
            <span class="btn" style="color:#ffffff;
                    border-color:#ffffff;
                    background-color: rgba(14,15,15,0.3);">
                Hält dich warm und müffelt nicht - Merinowäsche zum Vorteilspreis</span>
            </span>
					</div>
				</a>
			</div>
			<div class="slide-element js_slide-element" style="float: left; width: 11.1111%;">
				<a href="/aktionen/geschenke/geschenkefinder/?itm_medium=startseitenbanner&amp;itm_source=globetrotter&amp;itm_campaign=Geschenberater_banner_041218">
					<div class="simple-stage main-stage set-background "
					     data-background="background-image:url('/media/i/Banner-Geschenkeberater-1100x450-19663-0.jpg');"
					     style="background-image:url('/media/i/Banner-Geschenkeberater-1100x450-19663-0.jpg');">
        <span class="stage-content center-content bottom-content active-content">
            <span class="btn" style="color:#ffffff;
                    border-color:#ffffff;
                    background-color: rgba(14,15,15,0.3);">
                Geschenkeberater: Mit drei Clicks zum passenden Geschenk</span>
            </span>
					</div>
				</a>
			</div>
			<div class="slide-element js_slide-element" style="float: left; width: 11.1111%;">
				<a href="/marken/is-not-enough/?itm_medium=startseitenbanner&amp;itm_source=globetrotter&amp;itm_campaign=isnotenough_neue_marke">
					<div class="simple-stage main-stage set-background "
					     data-background="background-image:url('/media/i/IsNotEnough_1100x450px_3-compressor-19708-0.jpg');"
					     style="background-image:url('/media/i/IsNotEnough_1100x450px_3-compressor-19708-0.jpg');">
        <span class="stage-content center-content bottom-content active-content">
            <span class="content-image-wrapper">
                <picture>
                  <img data-src="/media/i/IsNotEnough_Safe-Space_430x250-19709-0.png"
                       class="content-image js-unveil-list" alt="" title=""
                       src="/media/i/IsNotEnough_Safe-Space_430x250-19709-0.png" style="opacity: 1;">
                </picture>
              </span>
            <span class="btn" style="color:#ffffff;
                    border-color:#ffffff;
                    background-color: #5b5658;">
                Multifunktionshosen mit Top Preis-Leistungsverhältnis</span>
            </span>
					</div>
				</a>
			</div>
			<div class="slide-element js_slide-element current-slide" style="float: left; width: 11.1111%;">
				<a href="/newsletter/duffles/?itm_medium=startseitenbanner&amp;itm_source=globetrotter">
					<div class="simple-stage main-stage set-background "
					     style="background-image:url('/media/i/Duffles-1100x450-compressor%281%29-19822-0.jpg');">
        <span class="stage-content center-content bottom-content active-content">
            <canvas class="stage-content-image" width="650" height="48"></canvas>
            <span class="btn" style="color:#ffffff;
                    border-color:#ffffff;
                    background-color: rgba(14,15,15,0.3);">
                Extra viel Platz für deine Reiseausrüstung</span>
            </span>
					</div>
				</a>
			</div>
			<div class="slide-element js_slide-element" style="float: left; width: 11.1111%;">
				<a href="/aktionen/adventskalender/?itm_medium=startseitenbanner&amp;itm_source=globetrotter">
					<div class="simple-stage main-stage set-background "
					     style="background-image:url('/media/i/Adventskalender-compressor-9088-0.jpg');">
        <span class="stage-content center-content bottom-content active-content">
            <canvas class="stage-content-image" width="650" height="48"></canvas>
            <span class="btn" style="color:#ffffff;
                    border-color:#ffffff;
                    background-color: rgba(14,15,15,0.3);">
                Klaus hat hinter jedem Türchen ein besonderes Outdoor-Schnäppchen versteckt</span>
            </span>
					</div>
				</a>
			</div>
		</div>
	</div>
	<div class="js_indicator-wrap slide-indicator-wrap">
		<div class="slide-indicator-item" style="width: 16.6667%;"></div>
		<div class="slide-indicator-item" style="width: 16.6667%;"></div>
		<div class="slide-indicator-item" style="width: 16.6667%;"></div>
		<div class="slide-indicator-item" style="width: 16.6667%;"></div>
		<div class="slide-indicator-item" style="width: 16.6667%;"></div>
		<div class="slide-indicator-item slide-indicator-current" style="width: 16.6667%;"></div>
	</div>
	<div class="slide-control-wrap">
		<span class="slide-control slide-control-prev js_prev"></span>
		<span class="slide-control slide-control-next js_next"></span>
	</div>
</div>