<svg style="display:none">
	<symbol id="bn-cart" viewBox="0 0 30 30">
		<title>Shopping Cart</title>
		<desc>Warenkorb</desc>
		<style type="text/css">
			.st6 {
				fill: none;
				stroke: #000000;
				stroke-width: 2;
				stroke-miterlimit: 10;
			}
		</style>
		<path class="st6" d="M5.9,7.5L5.9,7.5h19.9l-3.3,9.8H6.1 M6,22c-0.7,0-1.3,0.6-1.3,1.3s0.6,1.3,1.3,1.3s1.3-0.6,1.3-1.3S6.8,22,6,22 z M21.2,22c-0.7,0-1.3,0.6-1.3,1.3s0.6,1.3,1.3,1.3s1.3-0.6,1.3-1.3S21.9,22,21.2,22z M3.3,4.8l2.3,2.7v14.1h16.9"/>
	</symbol>
	<symbol id="bn-search" viewBox="0 0 30 30">
		<title>
			Body Lupe
		</title>
		<desc>
			Body Lupe
		</desc>
		<style type="text/css">
			.st6 {
				fill: none;
				stroke: #000000;
				stroke-width: 2;
				stroke-miterlimit: 10;
			}

			.st6:hover{
				stroke: #60a3b4;
			}
			.st7:hover{
				stroke: #60a3b4;
				fill:   #60a3b4;
			}

			.st7 {
				fill: #1D1D1B;
			}

			</style>
			<g>
				<g>
					<line class="st6" x1="19.7" y1="19.7" x2="26.3" y2="26.2"></line>
					<line class="st7" x1="19.7" y1="19.7" x2="26.3" y2="26.2"></line>
				</g>
				<g>
					<path d="M13.1,5.3c2.1,0,4,0.8,5.5,2.3c3,3,3,7.9,0,11c-1.5,1.5-3.4,2.3-5.5,2.3s-4-0.8-5.5-2.3c-3-3-3-7.9,0-11
			C9.1,6.1,11,5.3,13.1,5.3 M13.1,3.3c-2.5,0-5,1-6.9,2.9c-3.8,3.8-3.8,10,0,13.8c1.9,1.9,4.4,2.9,6.9,2.9c2.5,0,5-1,6.9-2.9
			c3.8-3.8,3.8-10,0-13.8C18.1,4.3,15.6,3.3,13.1,3.3L13.1,3.3z"></path>
				</g>
			</g>
	</symbol>
</svg>

[{*

<svg xmlns='http://www.w3.org/2000/svg' width='100%' height='100%' viewBox='0 0 30 30'>

	<style>

		.sprite {
			display: none
		}

		.sprite:target {
			display: inline;
		}
	</style>

	<g id='bn-cart' class="sprite">
		<path class="st6" d="M5.9,7.5L5.9,7.5h19.9l-3.3,9.8H6.1 M6,22c-0.7,0-1.3,0.6-1.3,1.3s0.6,1.3,1.3,1.3s1.3-0.6,1.3-1.3S6.8,22,6,22 z M21.2,22c-0.7,0-1.3,0.6-1.3,1.3s0.6,1.3,1.3,1.3s1.3-0.6,1.3-1.3S21.9,22,21.2,22z M3.3,4.8l2.3,2.7v14.1h16.9"/>
	</g>

</svg>


<div class="hidden">
	<!--
	<svg viewBox="0 0 400 240">
		<g id="darktable" style="font-size:12px;font-family:Helvetica, Arial, sans-serif;fill:#ccc">
			…
		</g>
	</svg>
	-->

	<svg id="bn-cart" viewBox="0 0 30 30">
		<style type="text/css">
			.st6 {
				fill: none;
				stroke: #000000;
				stroke-width: 2;
				stroke-miterlimit: 10;
			}
		</style>
		<path class="st6" d="M5.9,7.5L5.9,7.5h19.9l-3.3,9.8H6.1 M6,22c-0.7,0-1.3,0.6-1.3,1.3s0.6,1.3,1.3,1.3s1.3-0.6,1.3-1.3S6.8,22,6,22 z M21.2,22c-0.7,0-1.3,0.6-1.3,1.3s0.6,1.3,1.3,1.3s1.3-0.6,1.3-1.3S21.9,22,21.2,22z M3.3,4.8l2.3,2.7v14.1h16.9"/>
</svg>
</div>
 *}]