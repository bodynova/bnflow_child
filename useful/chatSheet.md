Smarty (all tags are enclosed in 
```[{...}]```
)

```[{* this is a comment *}]``` __comments syntax__

| Syntax | Description |
|: --- : | : --- : |
| oxgetseourl ident=$oViewConf->getSelfLink() | cat:“cl=info&tpl=foobar.tpl“|generates a SEO link|
| oxmultilang ident=“MULTILANGIDENT“ | gathers translations|
| oxstyle include=$oViewConf->getModuleUrl(“custom, “out/custom.css”) | include css from module|
| oxscript include=”js/widgets/oxinputvalidator.js” priority=10 | include js file with priority to load|
| oxscript add=”$(‘form’).validator();” | add javascript to template Parameters|
| $oViewConf->getActArticleId()	[articleId] | (Returns active article id)
| $oViewConf->getActCatId()	[categoryId] | (Returns active category id)
| $oViewConf->getActLanguageId()	[langId] | (Returns session language id)
| $oViewConf->getActManufacturerId()	[manufacturerID] | (Returns active manufacturer id)
| $oViewConf->getActSearchParam()	[searchParam] | (Returns active search parameter)
| $oViewConf->getActSearchTag()	[searchTag] | (Returns active search tag parameter)
| $oViewConf->getActTplName()	[templateName] | (Returns active template name)
| $oViewConf->getActTplName()	| start, details, list (Returns name of active view class)
| $oViewConf->getActiveShopId()	| oxbaseshop  EE only: 1, 2, 3,.. (Returns currently open shop id)
| $oViewConf->getActiveShopId()	| |
| <input name=”stoken” value=”6F12EEC4″ type=”hidden”>| | 
| <input name=”force_sid” value=”[sessionId]” type=”hidden”> | | 
| <input name=”lang” value=”[langId]” type=”hidden”> | (Returns forms hidden session parameters) | 
| $oViewConf->getNavFormParams() | |
| <input name=”ldtype” value=”infogrid” type=”hidden”> |(Returns navigation forms parameters) |
| $oViewConf->getNavUrlParams()	&amp;ldtype=infogrid | (Returns navigation url parameters) |
| $oViewConf->isBuyableParent() true/false | (Returns “blVariantParentBuyable” parent article config state)|
| $oViewConf->isSsl()	true/false (Returns ssl mode (on/off))

| Url + Directories| |
| :---: | :---: |
| $oViewConf->getAdminDir()	/this/is/shop/root/admin/ (config.inc.php) (Returns admin path)
$oViewConf->getBaseDir()	http://www.domain.com/ (Returns shops base directory path)
$oViewConf->getCurrentHomeDir()	http(s)://www.domain.com/ (Returns shops home path)
$oViewConf->getHelpPageLink()	http://www.domain.com/Help-Main/?force_sid=[sessionId] (Returns shop help link)
$oViewConf->getHomeLink()	http://www.domain.com/index.php?force_sid=[sessionId]&amp; (Returns shops home link)
$oViewConf->getImageUrl()	http(s)://www.domain.com/out/basic/img/ (Returns image url)
$oViewConf->getNoSslImageDir()	http://www.domain.com/out/basic/img/ (Returns non ssl image url)
$oViewConf->getResourceUrl()	http(s)://www.domain.com/out/basic/src/ (Returns shops resource url)
$oViewConf->getTemplateDir()	/this/is/shop/root/out/basic/tpl/ (Returns shops current [related to language] templates path)
$oViewConf->getUrlTemplateDir()	http(s)://www.domain.com/out/basic/tpl/ (Returns shops current templates url)
$oViewConf->getPictureDir()	http(s)://www.domain.com/out/pictures/ (Returns dynamic (language related) image url, planned deprecation)
$oViewConf->getSelfActionLink()	http(s)://www.domain.com/index.php?force_sid=[sessionId]&amp; (Returns shops action link)
$oViewConf->getSelfLink()	http://www.domain.com/index.php?force_sid=[sessionId]&amp; (Returns shops self link)
$oViewConf->getSslSelfLink()	http(s)://www.domain.com/index.php?force_sid=[sessionId]&amp; (Returns shops self ssl link)
oxRegistry::getUtils()->redirect($sUrl, $blAddRedirectParam, $iHeaderCode)	null|exit (Redirect user to the specified URL)
Checkout process

$oViewConf->getBasketLink()	http://www.domain.com/index.php?force_sid=[sessionId]&amp;cl=basket
(Returns shops basket link)
$oViewConf->getExeOrderLink()	http(s)://www.domain.com/index.php?force_sid=[sessionId]&amp;cl=order&amp;fnc=execute
(Returns shops order execution link)
$oViewConf->getOrderConfirmLink()	http(s)://www.domain.com/index.php?force_sid=[sessionId]&amp;cl=order
(Returns shops order confirmation link)
$oViewConf->getOrderLink()	http(s)://www.domain.com/index.php?force_sid=[sessionId]&amp;cl=user
(Returns shops order link)
$oViewConf->getPaymentLink()	http(s)://www.domain.com/index.php?force_sid=[sessionId]&amp;cl=payment
(Returns shops payment link)
Debug

dumpVar($mVar)	PHP in OXID eShop framework (formatted)
var_dump($mVar)	in PHP (detailed)
debug_backtrace($oVar)	in PHP (especially detailed)
$mVar|@var_dump	in Smarty
$mVar|debug_print_var	in Smarty
$aVar|@debug_print_var	in Smarty for Arrays
[{debug}]	in Smarty opens a new page with a list of available variables
mySQL

mysql –u[user] –p[password] –h[host] -P[port] -D[database]	Log in
mysqldump -u[user] -p[password] -h[host] -P[port] [database]> [filename].sql	DB dump
mysql -u[user] -p[password] -h[host] -P[port] [database] < [filename].sql	Feed dump to DB
tee [path]/[filename].txt	writes output into a file
source [path]/[filename].sql (Profihost: /home/…)	runs SQL file
DELETE FROM oxconfig WHERE OXVARNAME IN (
“aModulePaths”,
“aModules”,
“aDisabledModules”,
“aLegacyModules”,
“aModuleTemplates”,
“aModuleFiles”,
“aModuleVersions”,
“aModuleEvents”,
“aModulePaths”
);	SQL query to disable all modules
SHOW FULL PROCESSLIST;	displays running SQL queries
Views (values can be different in different view classes)

$oView->getShowPromotionList()	true|false (should promotions list be shown?)
$oView->isEnabledPrivateSales()	true|false (Checks if private sales is on)
$oView->getInvoiceAddress()	array (Template variable getter. Returns user address)
$oView->getDeliveryAddress()	array (Template variable getter. Returns user delivery address)
$oView->getMenueList()	array (Template variable getter. Returns header menu list)
$oView->getNavigationParams()	array of cnid, mnid, listtype, ldtype, searchparam,…
(Returns array of params => values which are used in hidden forms and as additional url params)
$oView->getCatTreePath()	string (Template variable getter. Returns category path)
$oView->getComponents()	array of {oxcmp_user}, {oxcmp_shop}, {oxcmp_utils}, {oxcmp_basket},… (Get array of component objects)
$oView->getPromoFinishedList()	{oxActionList} (loads oxActionList::loadFinishedByCount(2)) (return last finished promotion list)
$oView->getPromoCurrentList()	{oxActionList} (loads oxActionList::loadCurrent()) (return current promotion list)
$oView->getPromoFutureList()	{oxActionList} (loads oxActionList::loadFutureByCount(2)) (return future promotion list)
$oView->getActiveCategory()	string (If current reset ID is not set – forms and returns view ID according to category and user group)
$oView->getActCurrency()	{stdClass} (Get active currency)
$oView->getProduct()	{oxArticle} (Returns current view product object (if it is loaded))
$oView->getViewProduct()	alias $oView->getProduct()
$oView->getBargainArticleList()	{oxArticleList} (loads oxArticleList::loadAktionArticles(‘OXBARGAIN’)) (Template variable getter. Returns bargain article list. Parameter oxUBase::$_blBargainAction must be set to true.)
$oView->getTop5ArticleList()	{oxArticleList} (loads oxArticleList::loadtop5Articles()) (Returns Top 5 article list. Parameter oxUBase::$_blTop5Action must be set to true.)
$oView->getActVendor()	{oxVendor} (Template variable getter. Returns vendor id)
$oView->getActManufacturer()	{oxManufacturer} (Returns active Manufacturer set by categories component;)
$oView->getCategoryTree()	{oxCategoryList} (Returns category tree (if it is loaded))
$oView->getManufacturerTree()	{oxManufacturerList} (Returns Manufacturer tree (if it is loaded))
$oView->getManufacturerlist()	{aList} (Template variable getter. Returns Manufacturer list for search)
$oView->getRootVendor()	{oxVendor} (Template variable getter. Returns root vendor)
$oView->getRootManufacturer()	{oxManufacturer} (Template variable getter. Returns root Manufacturer)
$oView->getActiveLangAbbr()	de, en, fr (Returns active lang suffix)
$oView->getActiveUsername()	your@username.com (Template variable getter. Returns logged in user name)
$oView->getAdditionalParams()	cl=search&searchparam=[searchParam] (Template variable getter. Returns additional params for url)
$oView->getManufacturerId()	[manufactuerId] (Template variable getter. Returns Manufacturer id)
$oView->getVendorId()	[vendorId] (Template variable getter. Returns vendor id)
Legend

http	permanent Non-SSL
http(s)	alternate URL, if applicable
sid=[…]	return value / example / partial return value
{…}	a possible object, contains more attributes
true|false	type of data
