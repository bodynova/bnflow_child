<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 19.12.17
 * Time: 11:28
 */

/**
 * Beginn um das oxid Framework einzubinden
*/
require_once dirname(__DIR__).'/bootstrap.php';

/**
 * erzeugt eine UUID und gibt diese aus
 */
echo \OxidEsales\Eshop\Core\UtilsObject::class::getInstance()->generateUID();

/**
 * smarty variablen an jQuery übergeben:
 */
/*
[{capture name="myJS"}]
	$(document).ready(function () {
		var test = '[{oxmultilang ident="ACCESSORIES"}]';
		console.log('test');
		console.log(test);
	});
[{/capture}]
[{oxscript add=$smarty.capture.myJS}]
*/

/**
 * aktuelle Sprache ermitteln smarty:
 */
/*
[{assign var="lang" value=$oViewConf->getActLanguageAbbr()}]
*/

/**
 * im Template auf ein Modul zugreifen:
 *
[{$oViewConf->getModuleUrl('dre_adminsearch','out/src/css/easy-autocomplete.css') }]
*/

